'use strict';

const moreEmployerClearSession = [
  'employer-name',
  'job-title',
  'job-setting',
  'other-job-setting'
  ];
const maxEmployerCount = 10;

module.exports = {
  moreEmployerClearSession,
  maxEmployerCount
};
const Controller = require('hof-govfrontend-v3').controller;
const SubmitClaimModel = require('./../models/submit-claim');
const ClaimUtility = require('./../utility/claim-utility');


module.exports = class SubmitClaimController extends Controller {

    async process(req, res, next) {
        try {
            const claim = ClaimUtility.updateClaimFromSession(req.sessionModel.toJSON());
            let submitClaim = new SubmitClaimModel();
            const referenceNumber = await submitClaim.submitClaim(claim, next);
            req.sessionModel.set('claim-reference-number', referenceNumber);
            req.sessionModel.set('claim', claim);
            req.sessionModel.set('health-claim-submit-flag', true);
            next();

        } catch (error) {
            console.error('Error occured while submitting Claim, Please try again later!! ', error);
        }
    }
};
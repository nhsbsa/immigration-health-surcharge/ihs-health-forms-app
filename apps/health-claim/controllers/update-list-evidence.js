const Controller = require('hof-govfrontend-v3').controller;
const FileUploadModel = require('../models/file-upload');
const fileUploadModel = new FileUploadModel();
module.exports = class DependantDeleteController extends Controller {

    get(req, res, next) {
        fileUploadModel.fileListAPI(req.sessionModel.get('claim'),  req.sessionID).then(response => {
            req.sessionModel.set('uploadedFileEvidence', []);
            let uploadFileList = (response['data']['filesCount'] == 0) ? [] : response['data']['files'];
            let newUploadList = [];

            uploadFileList.forEach((currentFile, index) => {
                if(!currentFile['filename'].includes("extra-info-")){    
                newUploadList.push({
                    'srNo': index + 1,
                    'fileName': currentFile['filename'],
                    'fileId': currentFile['fileId']
                });
                }
            });
            // TODO: MAKE IT COME FROM INDEX
            req.sessionModel.set('uploadedFileEvidence', newUploadList);
            if (req.url.indexOf('/edit') != -1) {
                return res.redirect(`${req.baseUrl}/check-your-answers`);
            } else {
                req.session['hof-wizard-health-claim'].steps.includes('/attachments') ?
        '' : req.session['hof-wizard-health-claim'].steps.push('/attachments');
                return res.redirect(`${req.baseUrl}/extra-information`);
            }

        }).catch(error => {
            return next(new this.ValidationError('Invalid request.'));
        }).finally(() => {

        });
    }

};
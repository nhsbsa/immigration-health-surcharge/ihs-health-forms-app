const DependantDetailsController = require('../../../../apps/health-claim/controllers/dependant-details');
const HOFController = require('hof-govfrontend-v3').controller;
const mockData = require('../../../_mocks_/health-claim/controllers/dependant-details');
const { chai, reqres, sinon, expect, assert } = require('../../../setup');
const _ = require('lodash');

describe('DependantDetailsController', () => {
  let req;
  let res;
  const next = sinon.stub();  
  const callback = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('DependantDetailsController instance', () => {
    let dependantDetailsController;
    beforeEach(() => {
      // an instance of the DependantDetailsController
      dependantDetailsController = new DependantDetailsController({});
    });

    it('extends the hof-form-controller', () => {
      dependantDetailsController.should.be.instanceof(HOFController);
    });

    it('has getValues, modifyDepedantDetails methods', () => {
      dependantDetailsController.should.have.property('getValues').that.is.a('function');
      dependantDetailsController.should.have.property('saveDependant').that.is.a('function');      
      dependantDetailsController.should.have.property('modifyDepedantDetails').that.is.a('function');      
    });

    describe('getValues', () => {
      beforeEach(() => {
        dependantDetailsController.saveDependant = sinon.stub();
      });
      it('calls getValues ', () => {
        dependantDetailsController.getValues(req, res, next);
        dependantDetailsController.saveDependant.should.have.been.called;
      });
    });
    
    describe('saveDependant', () => {
      beforeEach(() => {
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();
        req.sessionModel.unset = sinon.stub();        
        res.redirect = sinon.stub();
        req.session = sinon.stub();
        sessionModel = sinon.stub();
        dependantDetailsController.modifyDepedantDetails = sinon.stub(); 
      });

      it('calls saveDependant when dependant id matches', () => {
        sessionModel['dependant-id'] =  'abc123';     
        sessionModel['all-dependants'] = mockData.allDependants;  
        dependantDetailsController.saveDependant(req, sessionModel);    
        dependantDetailsController.modifyDepedantDetails.should.have.been.called;  
      });

      it('calls saveDependant  when dependant id does not matches', () => {   
        sessionModel = mockData.sessionModel;    
        sessionModel['dependant-id'] =  'abc123';         
        sessionModel['all-dependants'] = mockData.allDependants;    
        dependantDetailsController.saveDependant(req, sessionModel);
        expect(sessionModel['all-dependants'][0]['dependant-id']).to.deep.equal(sessionModel['dependant-id']);
      });
    });

    describe('modifyDepedantDetails', () => {
      beforeEach(() => {
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();
        req.sessionModel.unset = sinon.stub();  
        req.session = sinon.stub();
        sessionModel = sinon.stub();       
      });

      it('calls modifyDepedantDetails if edit-dependantId matches', () => {
        sessionModel['edit-dependantId'] =  'abc123'; 
        sessionModel['all-dependants'] = mockData.allDependants; 
       dependantDetailsController.modifyDepedantDetails(req);
      });

      it('calls modifyDepedantDetails if edit-dependantId does not matches', () => {
        sessionModel['edit-dependantId'] =  'wqeqe'; 
        sessionModel['all-dependants'] = mockData.allDependants; 
         dependantDetailsController.modifyDepedantDetails(req);
      });   
    });
  });

});
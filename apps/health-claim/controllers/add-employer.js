const Controller = require('hof-govfrontend-v3').controller;
var fs = require('fs');
const { reject } = require('lodash');
const { moreEmployerClearSession } = require('../constants/employer');
const EmployerModel = require('../models/employer');
const Employer = require('../models/get-employer');

module.exports = class AddEmployerController extends Controller {

  locals(req, res, callback) {
    const locals = super.locals(req, res, callback);
    const deletedEmployer = req.sessionModel.get('deletedEmployer');
    locals.deletedEmployer = deletedEmployer ? deletedEmployer : false;
    req.sessionModel.unset('deletedEmployer');
    return locals;
  }

  async process(req, res, next) {
    const valid = this.checkValidation(req, res, next);
    if (valid) {
      try {
        let checkResult = await this.employerCheck(req, res, next);
        const employerId = req.sessionModel.get('edit-employerId') ? req.sessionModel.get('edit-employerId') : req.sessionModel.get('employer-id');

        if (!employerId) {
          this.createNewEmployee(req, checkResult);
        } else {
          let empData = this.modifyEmployerDetails(req, employerId, checkResult);
          // req.sessionModel.unset('edit-employerId');
          if (checkResult.status === 'next' && empData['job-title'] == '') {
            checkResult.status = `${req.baseUrl}/job-title`;
          }
          if (req.url.indexOf('/edit') != -1) {           
            return res.redirect(`${req.baseUrl}/check-employment/edit`);
          }
        }
        if (checkResult.status === `${req.baseUrl}/check-employment`) {
          req.session['hof-wizard-health-claim'].steps.includes('/add-employer') ?
            '' : req.session['hof-wizard-health-claim'].steps.push('/add-employer');
          req.session['hof-wizard-health-claim'].steps.includes('/job-title') ?
            '' : req.session['hof-wizard-health-claim'].steps.push('/job-title');
          req.session['hof-wizard-health-claim'].steps.includes('/job-setting') ?
            '' : req.session['hof-wizard-health-claim'].steps.push('/job-setting');
          return res.redirect(`${checkResult.status}`);
        } else {
          let index = req.session['hof-wizard-health-claim'].steps.indexOf('/job-title');
          if (index > -1) {
            req.session['hof-wizard-health-claim'].steps.splice(index, 1); // 2nd parameter means remove one item only
          }
          index = req.session['hof-wizard-health-claim'].steps.indexOf('/job-setting');
          if (index > -1) {
            req.session['hof-wizard-health-claim'].steps.splice(index, 1); // 2nd parameter means remove one item only
          }
          return next();
        }
      } catch (error) {
        console.error('Error calling getEmployer', error);
        return next();       
      }
    }
  }

  flashError(req, res, next, validationType) {
    const err = new this.ValidationError('employer-name', {
      type: validationType
    }, req, res);
    const errorObject = {};
    errorObject['employer-name'] = err;
    return next(errorObject);
  }

  checkValidation(req, res, next) {
    const empname = req.form.values['employer-name'];
    if (empname.length == 0)
      return this.flashError(req, res, next, 'minlength');

    if (empname.length < 3 || empname.length > 255)
      return this.flashError(req, res, next, 'maxlength');

    const nonWhiteSpace = /(.*[a-zA-Z0-9]){3}/i;
    if (!nonWhiteSpace.test(empname))
      return this.flashError(req, res, next, 'maxlength');

    const pattern = /^[a-zA-Z0-9'_/&:.\[\],()@-\s]+$/i;
    const otherSpecial = /[*+]/gi;
    if (!pattern.test(empname) || otherSpecial.test(empname)) {
      return this.flashError(req, res, next, 'employer-name', 'regex');
    }
    return true;
  }

  createNewEmployee(req, employerData) {
    let uniqueId = 'id' + (new Date()).getTime();
    req.sessionModel.set('employer-id', uniqueId);
    let empData = {
      'employer-id': uniqueId,
      'employer-name': req.form.values['employer-name'],
      'organisation-id': (employerData.organisationData.length == 0) ? '' : employerData.organisationData[0]['OrgId'],
      'job-role-id': '',
      'job-title': '',
      'job-setting': ''
    };
    let allEmployers = req.sessionModel.get('all-employers') ? req.sessionModel.get('all-employers') : [];
    allEmployers.push(new EmployerModel(empData));
    req.sessionModel.set('all-employers', allEmployers);
  }

  modifyEmployerDetails(req, employerId, checkResult) {
    let allEmployers = req.sessionModel.get('all-employers');
    const employerIndex = allEmployers.findIndex(employer => employer['employer-id'] === employerId);
    allEmployers[employerIndex]['employer-name'] = req.form.values['employer-name'];
    allEmployers[employerIndex]['organisation-id'] = (checkResult.organisationData.length == 0) ? '' : checkResult.organisationData[0]['OrgId'];
    if (checkResult.status !== 'next') {
      allEmployers[employerIndex]['job-title'] = allEmployers[employerIndex]['job-setting'] = allEmployers[employerIndex]['job-role-id'] = '';
    }
    const empmodel = new EmployerModel(allEmployers[employerIndex]);
    allEmployers[employerIndex] = empmodel.editEmployer(empmodel);
    req.sessionModel.set('all-employers', allEmployers);

    return allEmployers[employerIndex];
  }

  async employerCheck(req, res, next, employerId = false) {
    return new Promise(async (resolve, reject) => {
      try {
        if (req.form.values['employer-name'] < 3)
          return false;
        const employer = new Employer();
        let employeData = [];
        if (employeData.Organisations.length !== 1) {
          return resolve({ status: 'next', organisationData: [] });
        }

        var roleId = employeData.Organisations['0']['PrimaryRoleId'];
        let proleids = await fs.promises.readFile(`${__dirname}/primaryroleids.json`)
        const primaryroleids = JSON.parse(proleids);
        if (!primaryroleids.hasOwnProperty(roleId) || primaryroleids[roleId] !== 'FALSE') {
          return resolve({ status: 'next', organisationData: employeData.Organisations });
        }

        return resolve({ status: `${req.baseUrl}/check-employment`, organisationData: employeData.Organisations });
      } catch (error) {
        console.error('Error calling getEmployer', error);
        return resolve({ status: 'next', organisationData: [] });
      }
    });
  }
};
const Controller = require('hof-govfrontend-v3').controller;
const { moreDependantClearSession, maxDependantCount } = require('./../constants/dependant');

module.exports = class DependantAddController extends Controller {
  render(req, res, callback) {    
    const isedit =  req.sessionModel.get('check-your-answers')? '/edit' : ''; 
    var  backlink =  'phone-number';
    res.locals.backLink = "/health/claim/"+ backlink+isedit;
    res.locals.options.locals.backLink = "/health/claim/"+ backlink+isedit;    
    const locals = super.render(req, res, callback);
    return locals;
  }

  locals(req, res, callback) {
    const locals = super.locals(req, res, callback);
    const deletedDependant = req.sessionModel.get('deletedDependant');
    locals.backLink=locals.backLink+'/edit';
    locals.deletedDependant = deletedDependant ? deletedDependant : false;
    if(deletedDependant){
      req.form.values['dependant-question']='';
    }else{
      req.form.values['dependant-question']=req.sessionModel.get('formValue-dependant-question');  
    }
    req.sessionModel.unset('deletedDependant');
    return locals;
  }

  process(req, res, next) {
    if( !req.form.values['dependant-question']){   
      req.sessionModel.unset('formValue-dependant-question');  
      const err = new this.ValidationError('dependant-question', {
        type: 'required'
      }, req, res);
      const errorObject = {};
      errorObject['dependant-question'] = err;
      
      return next(errorObject);
    }else{
      let allDependants = req.sessionModel.get('all-dependants');
      req.sessionModel.set('formValue-dependant-question',  req.form.values['dependant-question']);
      if(allDependants && allDependants.length > 0){
        req.sessionModel.unset('dependant-id');
        const nextNav = '/check-dependants';
        return res.redirect(`${req.baseUrl}${nextNav}`);
      }else{
        const moreDependants = req.form.values['dependant-question'];
        if(moreDependants === 'yes'){      
          req.sessionModel.unset(moreDependantClearSession);
          const uniqueId = 'id' + (new Date()).getTime() ;
          req.sessionModel.set('dependant-id', uniqueId);
        }else if(req.sessionModel.get('check-your-answers')){
          req.sessionModel.unset('check-your-answers');  
          const nextNav = '/check-your-answers';
          return res.redirect(`${req.baseUrl}${nextNav}`);
        }
      }      
      next();
    }
  }  
};
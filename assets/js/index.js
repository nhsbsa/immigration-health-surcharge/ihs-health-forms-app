/* eslint-disable */
'use strict';
var $ = require('jquery');

var dialogPolyfill = require('dialog-polyfill')
window.jQuery = $;
window.$ = $;
window.dialogPolyfill = dialogPolyfill;

var GOVUK = require('govuk-frontend')
var printEmail = require('./print-email');

var path = window.location.pathname;
if (path.includes('add-employer')) {
  var getEmployer = require('./get-employer');
}
if (path.includes('job-title')) {
  var getTitle = require('./job-title');
}
if (path.includes('job-setting')) {
  if (document.getElementById('other-job-setting') !== undefined) {
    document.getElementById('other-job-setting').classList.add('govuk-input');
    document.getElementById('other-job-setting').classList.add('govuk-input--width-20');
    document.getElementById('other-job-setting').maxLength = 50;
    if (document.getElementsByClassName('govuk-error-summary').length != 0) {
      setTimeout(function () {
        if (document.getElementById('other-job-setting-panel').getAttribute('aria-hidden') == 'false') {
          document.querySelector('[data-module="govuk-error-summary"]').focus();
        }
      }, 100);
    }
  }
}

if (path.includes('upload-evidence')) {
  var multiFileUploadObj = require('./multi-file-upload');
}

if (path.includes('check-your-answers')) {
  $('#fileEvidence-change-link').attr('href', $('#fileEvidence-change-link').attr('href').replace('attachments', 'upload-evidence'));
  var checkYourAnswer = require('./check-your-answer');
}

if (path.includes('comments/edit')) {
  $('.govuk-back-link').attr('href', $('.govuk-back-link').attr('href').replace('attachments', 'upload-evidence'));
}

if (path.includes('extra-information')) {
  $('.govuk-back-link').attr('href', $('.govuk-back-link').attr('href').replace('attachments', 'upload-evidence'));
}

var skipToMain = require('./skip-to-main');


GOVUK.initAll();
window.GOVUK = GOVUK;
var cookie = require('./govuk-cookies');
var cookieSettings = require('hof-govfrontend-v3/frontend/themes/gov-uk/client-js/index');

var sessionDialog = require('./session-dialog');
GOVUK.sessionDialog.init();



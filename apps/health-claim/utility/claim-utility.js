const moment = require('moment');
const nodeBase64 = require('nodejs-base64-converter');
const config = require('./../../../config');

module.exports = class ClaimUtility {

  static bsaRefData(claim) {
    return {
      sessionId: claim.sessionId,
      cohort: claim.cohort,
      status: 'DRAFT'
    };
  }

  static fileData(file) {
    return {
      "fileName": file.name,
      "contentType": file.mimetype,
      "data": nodeBase64.encode(file.data)
    };
  }

  static generatePersonalDetails(data, personalDetails) {
    data.givenName = personalDetails['given-name'];
    data.familyName = personalDetails['family-name'];
    data.emailAddress = personalDetails['email'];
    data.ihsReference = personalDetails['immigration-health-surcharge-number'];
    data.nationalInsuranceNumber = personalDetails['national-insurance-number'];
    data.dateOfBirth = personalDetails['date-of-birth'];
    data.telephone = personalDetails['phone-number'];
  }

  static generateDependantClaimDetails(allDependants) {
    const dependantsClaimDetails = allDependants.map(dependant => {
      return {
        "givenName": dependant['dependant-given-name'],
        "familyName": dependant['dependant-family-name'],
        "ihsReference": dependant['dependant-immigration-health-surcharge-number'],
        "dateOfBirth": dependant['dependant-date-of-birth']
      };
    })
    return dependantsClaimDetails;
  }

  static generateEmployersClaimDetails(allEmployers) {
    const employersClaimDetails = allEmployers.map(employers => {
      return {
        "organisation": employers['employer-name'],
        "organisationId": employers['organisation-id'],
        "jobRoleTitle": employers['job-title'],
        "jobRoleCode": employers['job-role-id'],
        "setting": (employers['job-setting'] == 'other') ? employers['other-job-setting'] : employers['job-setting']
      };
    })
    return employersClaimDetails;
  }

  static updateClaimFromSession(model) {
    let payload = {
      apiVersion: "1.0",
      id: model['claim']["sessionId"],
      method: "NEW",
      source: "CLAIM_FORM",
      cohort: config.cohort,
      status: "NEW",
      sessionId: model['claim']["sessionId"]
    };
    const personalDetails = model['personalDetails'];
    this.generatePersonalDetails(payload, personalDetails);

    payload.claimStartDate = model['claim-start-date'];
    payload.claimEndDate = model['claim-calculated-end-date'];
    payload.declarationDate = moment().format('YYYY-MM-DD HH:mm:ss');
    payload.bsaReimbursementReference = model['claim']['bsaRef'];

    payload.channel = "ONLINE";
    payload.comments = model['extra-information'];
    payload.createdBy = "UNAUTHENTICATED";
    payload.claimReminder = (model['subscribe-for-reminder'] === 'yes') ? true : false;

    const allDependants = model['all-dependants'] ? model['all-dependants'] : [];
    payload.dependants = this.generateDependantClaimDetails(allDependants);

    const allEmployers = model['all-employers'] ? model['all-employers'] : [];
    payload.employments = this.generateEmployersClaimDetails(allEmployers);

    payload.contactMethodPreference = "EMAIL";

    return payload;
  }

}
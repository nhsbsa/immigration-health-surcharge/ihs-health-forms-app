
const sessionModel = {
  'dependant-given-name': 'Dependant',
  'dependant-family-name': 'One',
  'dependant-date-of-birth': '1998-12-02',
  'dependant-immigration-health-surcharge-number': 'IHS223456789', 
  'dependant-id': 'abc123',
  };

const allDependants = [{
  'dependant-name': 'Dependant One',
  ...sessionModel
}];

const displayDependants = [
  {
    'dependantIndex': 1,
    'dependantId': 'abc123',
    'dependantFields': [
      {
        "key": "dependant-name",
        'label': 'Translated Label',
        "url": "dependant-name",
        'value': 'Dependant One'
      },
      {
        "key": "dependant-date-of-birth",
        'label': 'Translated Label',
        "url": "dependant-date-of-birth",
        'value': 'Tranformed Value'
      },
      {
        "key": "dependant-immigration-health-surcharge-number",
        'label': 'Translated Label',
        "url": "dependant-immigration-health-surcharge-number",
        'value': 'IHS223456789'
      }
    ]
  }
];

const maxDependants = [
  {
    'dependant-id': 'abc001'
  },
  {
    'dependant-id': 'abc002'
  },
  {
    'dependant-id': 'abc003'
  },
  {
    'dependant-id': 'abc004'
  },
  {
    'dependant-id': 'abc005'
  },
  {
    'dependant-id': 'abc006'
  },
  {
    'dependant-id': 'abc007'
  },
  {
    'dependant-id': 'abc008'
  },
  {
    'dependant-id': 'abc009'
  },
  {
    'dependant-id': 'abc010'
  }
];

const addMoreDependantsError = { 'add-more-dependants': { type: 'maxDependants' } };

const dateInput = '2006-04-11';
const dateOutput = '11 April 2006';

const deleteDependant = 'Dependant One';

module.exports = {
  sessionModel,
  allDependants,
  displayDependants,
  deleteDependant,
  dateInput,
  dateOutput,
  maxDependants,
  addMoreDependantsError
}

const currentlyNotEligible = require('../../../../apps/health-claim/controllers/currently-not-eligible');
const HOFController = require('hof-govfrontend-v3').controller;
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash');

describe('currently Not Eligible', () => {
    let req;
    let res;
    const next = sinon.stub();

    beforeEach(() => {
        req = reqres.req();
        res = reqres.res();
    });
    describe('notEligibleController instance', () => {
        let notEligibleController;
        beforeEach(() => {
            notEligibleController = new currentlyNotEligible({});
            req.sessionModel.get = sinon.stub();
        });

        it('extends the hof-form-controller', () => {
            notEligibleController.should.be.instanceof(HOFController);
        });

        it('has process methods', () => {
            notEligibleController.should.have.property('render').that.is.a('function');
        });
        
        it('calls render with arguments', () => {            
            req.sessionModel.get.withArgs('claimants-start-date').returns('2000-09-09');
            req.sessionModel.get.withArgs('claimants-end-date').returns('2000-09-09');
            req.sessionModel.get.withArgs('calculated-earliest-date').returns('2000-09-09');
            notEligibleController.render(req, res, next);
        });

        it('calls render when there is no arguments', () => {
            notEligibleController.render(req, res, next);
        });
    });
});

'use strict';
const { startPageUrl, moreInfoData, onlinesurveys, tier2MedialLink } = require('../../configRoutes');

const continueButtonId = 'continue'
module.exports = {
  name: 'health-eligibility',
  baseUrl: '/health/eligibility',
  confirmStep: '/check-eligibility-answers',
  steps: {
    '/immigration-health-surcharge-paid': {
      fields: ['immigration-health-surcharge-paid'],
      next: '/applicant-visa-type',
      locals: {
        step: 'immigration-health-surcharge-paid',
        backLink: false,
        id: continueButtonId
      },
      forks: [{
        target: '/not-eligible-reimbursement-refund',
        condition: {
          field: 'immigration-health-surcharge-paid',
          value: 'no'
        }
      }]
    },
    '/applicant-visa-type': {
      fields: ['applicant-visa-type'],
      next: '/qualified-health-professional',
      locals: {
        step: 'applicant-visa-type',
        id: continueButtonId
      },
      forks: [{
        target: '/qualified-health-professional',
        condition: {
          field: 'applicant-visa-type',
          value: 'tier2'
        },
      },
      {
        target: '/not-eligible-to-apply',
        condition: {
          field: 'applicant-visa-type',
          value: 'tier5'
        },
      },
      {
        target: '/health-social-care-job',
        condition: {
          field: 'applicant-visa-type',
          value: 'other'
        },
      }
      ]
    },
    '/eligibility-check-your-answers': {
      template: 'eligibility-check-your-answers',
      behaviours: [require('hof-govfrontend-v3').components.summary]
    },
    '/not-eligible-reimbursement-refund': {
      template: 'not-eligible-reimbursement-refund',
      locals: {
        backLink: '',
        moreInfoData: moreInfoData,
        id: continueButtonId
      }
    },
    '/not-eligible-to-apply': {
      template: 'not-eligible-to-apply',
      locals: {
        backLink: '',
        moreInfoData: moreInfoData,
        onlinesurveys
      }
    },
    '/qualified-health-professional': {
      fields: ['qualified-health-professional'],
      next: '/health-social-care-job',
      locals: {
        startPageUrl: startPageUrl,
        step: 'qualified-health-professional',
        id: continueButtonId
      },
      forks: [{
        target: '/not-eligible-reimbursement',
        condition: {
          field: 'qualified-health-professional',
          value: 'yes'
        },
      }
      ]
    },
    '/health-social-care-job': {
      fields: ['health-social-care-job'],
      next: '/job-6-months',
      controller: require('./controllers/back-link'),
      locals: {
        startPageUrl: startPageUrl,
        step: 'health-social-care-job',
        id: continueButtonId
      },
      forks: [{
        target: '/not-eligible',
        condition: {
          field: 'health-social-care-job',
          value: 'no'
        }
      }]
    },
    '/not-eligible-reimbursement': {
      template: 'ihs-tier2-ukvi',
      locals: {
        backLink: '',
        tier2MedialLink: tier2MedialLink
      }
    },
    '/job-6-months': {
      fields: ['job-6-months'],
      next: '/job-average-16-hours',
      locals: {
        startPageUrl: startPageUrl,
        step: 'job-6-months',
        id: continueButtonId
      },
      forks: [{
        target: '/not-eligible',
        condition: {
          field: 'job-6-months',
          value: 'no'
        },
      }]
    },
    '/job-average-16-hours': {
      fields: ['job-average-16-hours'],
      next: '/check-eligibility-answers',
      locals: {
        startPageUrl: startPageUrl,
        step: 'job-average-16-hours',
        id: continueButtonId
      },
      forks: [{
        target: '/not-eligible',
        condition: {
          field: 'job-average-16-hours',
          value: 'no'
        },
      }]
    },
    '/check-eligibility-answers': {
      template: 'check-eligibility-answers',
      controller: require('./controllers/check-eligibility-answers'),
      locals: {
        backLink: '',
        startPageUrl
      },
      behaviours: [require('hof-govfrontend-v3').components.summary]
    },
    '/not-eligible': {
      template: 'not-eligible',
      locals: {
        backLink: '',
        startPageUrl: startPageUrl
      }
    }
  }
};

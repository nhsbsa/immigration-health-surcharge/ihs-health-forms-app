const Controller = require('hof-govfrontend-v3').controller;

module.exports = class checkEmployer extends Controller {

    process(req, res, next) {
        const ninoValue = req.form.values['national-insurance-number'] + '';
        if (ninoValue.replace(/\s+/g, '').length != 9 && ninoValue.replace(/\s+/g, '').length != '') {
            return next({
                'national-insurance-number': new this.ValidationError('national-insurance-number', {
                    type: 'regex'
                }, req, res)
            });
        }
        return next();
    }
}
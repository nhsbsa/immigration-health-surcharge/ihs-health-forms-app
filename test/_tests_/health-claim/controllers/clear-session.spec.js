const ClearDataSession = require('../../../../apps/health-claim/controllers/clear-session');
const HOFController = require('hof-govfrontend-v3').controller;
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('clearDataSession', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('ClearDataSession instance', () => {
    let clearDataSession;

    beforeEach(() => {
      // an instance of the ClearDataSession
      clearDataSession = new ClearDataSession({});
    });

    it('extends the hof-form-controller', () => {
      clearDataSession.should.be.instanceof(HOFController);
    });

    it('has a getValues method', () => {
      clearDataSession.should.have.property('getValues').that.is.a('function');
    });

    describe('getValues', () => {
      beforeEach(() => {
        _.set(req, 'session.destroy', sinon.stub());
        res.redirect = sinon.stub();
        req.baseUrl = '/health-eligibility';
      });

      it('calls destroy from session', () => {
        req.query = {};
        clearDataSession.getValues(req, res, next);
        req.session.destroy.should.have.been.called;
      });

      it('calls redirect to create new session for eligibility', () => {
        req.query = {};
        clearDataSession.getValues(req, res, next);
        res.redirect.should.have.been.calledWithExactly('/health-eligibility/start?sessionCleared=true');
      });

      it('calls redirect to create new session for claim', () => {
        req.query = {};
        req.baseUrl = '/health-claim';
        clearDataSession.getValues(req, res, next);
        res.redirect.should.have.been.calledWithExactly('/health-claim/start?sessionCleared=true');
      });

      it('calls redirect with student eligibility URL', () => {
        req.query = { sessionCleared: true };
        clearDataSession.getValues(req, res, next);
        res.redirect.should.have.been.calledWithExactly('/health-eligibility/paid-immigration-health-surcharge');
      });

      it('calls redirect with student claim URL', () => {
        req.query = { sessionCleared: true };
        req.baseUrl = '/health-claim';
        clearDataSession.getValues(req, res, next);
        res.redirect.should.have.been.calledWithExactly('/health-claim/name');
      });
    });
  });

});
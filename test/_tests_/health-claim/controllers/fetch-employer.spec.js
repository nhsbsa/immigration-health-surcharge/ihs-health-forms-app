const FetchEmployerController = require('../../../../apps/health-claim/controllers/fetch-employer');
const HOFController = require('hof-govfrontend-v3').controller;
const {  reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('FetchEmployerController', () => {
  let req,res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('FetchEmployerController instance', () => {
    let fetchEmployerController;
    const employerId = '1';
    beforeEach(() => {
      // an instance of the FetchEmployerController
      fetchEmployerController = new FetchEmployerController({});
    });
    describe('successHandler', () => {
      beforeEach(() => {
        req.sessionModel.get = sinon.stub();
      });
      it('calls successHandler and  edit-employerId is available', () => {
        req.sessionModel.get.returns( {'edit-employerId': employerId} );
        fetchEmployerController.successHandler(req, res, next);
      });

      it('calls successHandler and  edit-employerId is not available', () => {
        req.sessionModel.get.returns();
        fetchEmployerController.successHandler(req, res, next);
      });

      it('check if  check-your-answers session is set ', () => {
        req.sessionModel.get.returns({'check-your-answers' : true});
        fetchEmployerController.successHandler(req, res, next);
      });

      it('check if  check-employment session is set ', () => {
        req.sessionModel.get.returns([{'check-employment' : true},{'edit-employerId': employerId}]);       
        fetchEmployerController.successHandler(req, res, next);
      });

      it('check if check employement session is set false', () => {
        req.sessionModel.get.returns({'check-employment' : false});
        fetchEmployerController.successHandler(req, res, next);
      });

      it('calls successHandler with diffrent session  values ', () => {
        req.sessionModel.get.returns([{'check-your-answers' : true},{'edit-employerId': employerId},{'check-employment' : true}]);
        fetchEmployerController.successHandler(req, res, next);
      });

      it('calls successHandler with check-your-answers session value is true and check-employment is false', () => {
        req.sessionModel.get.withArgs('check-employment').returns(false);
        req.sessionModel.get.withArgs('check-your-answers').returns(true);
        req.sessionModel.get.withArgs('edit-employerId').returns(employerId);
        fetchEmployerController.successHandler(req, res, next);
      });

    });
  });

});
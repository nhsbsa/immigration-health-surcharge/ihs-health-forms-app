module.exports = class PersonalDetailsModel {
  constructor(model) {
    this['name'] = [model['given-name'], ' ', model['family-name']].join('');
    this['given-name'] = model['given-name'];
    this['family-name'] = model['family-name'];
    this['date-of-birth'] = model['date-of-birth'];
    this['immigration-health-surcharge-number'] = model['immigration-health-surcharge-number'].toUpperCase();
    this['national-insurance-number'] = model['national-insurance-number'].toUpperCase().replace(/\s/g, '');
    this['phone-number'] = model['phone-number'];
    this['email'] = model['email']; 
  }
}
const axios = require('axios').default;

module.exports = class getEmployerModel {
  getEmployer(next, param,primaryRoleId='') {  
    return new Promise(async (resolve, reject) => {
      try {
        const employeeNameArray = param.substring(0, 75).split('[');
        const employeeName = employeeNameArray[0].split(']');
        const roleId = primaryRoleId ? '&PrimaryRoleId='+encodeURIComponent(primaryRoleId) : '';
        console.log("Timeout-->");
        console.log(process.env.GET_EMP_TIMEOUT);
        const url = process.env.GET_EMP_URL + '?OrgRecordClass=RC1&_format=json&Status=Active&Name=' + encodeURIComponent(employeeName[0]) + roleId;
        const employerList = await axios.get(url , {
          timeout: parseInt(process.env.GET_EMP_TIMEOUT) });
        console.log("Fetching employer list ");
        resolve(employerList.data);
      } catch (error) {
        console.log('Error in fetching employer list' + error);
        reject(error);
      }
    });
  }

}
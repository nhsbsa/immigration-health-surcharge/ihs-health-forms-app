'use strict';
const Controller = require('hof-govfrontend-v3').controller;
const moment = require('moment');
const BsaRefModel = require('../models/bsa-reference');
const dateFormat = 'YYYY-MM-DD';

module.exports = class checkImmigrationHealthSurcharge extends Controller {
    async process(req, res, next) {
        try {

            let validationDate = req.sessionModel.get('validation-date');
            let currentDate = (typeof validationDate === 'undefined') ? moment(new Date()).format(dateFormat) : validationDate;
            let queryDate = moment(currentDate, dateFormat).subtract(6, 'months').format(dateFormat);
            const bsaRefModel = new BsaRefModel();
            let claimantsData = await bsaRefModel.getClaimantsSearch({
                "immigrationHealthSurchargeNumber": req.form.values['immigration-health-surcharge-number'],
                "dateOfBirth": req.sessionModel.get('date-of-birth'),
                "givenName": req.sessionModel.get('given-name'),
                "familyName": req.sessionModel.get('family-name'),
                "queryDate": queryDate
            });

            if (typeof claimantsData.data['claimants'] !== 'undefined' && claimantsData.data['claimants'].length != 0) {
                let previousEndDate = moment((claimantsData.data['claimants'][0]['endDate']).split('T')[0], dateFormat);
                let earliestCalData = await bsaRefModel.getEarliestDate(previousEndDate.format(dateFormat));
                let earliestCalDate = moment(earliestCalData.data['calculatedDate'], dateFormat);

                if (earliestCalDate.isSameOrAfter(moment(currentDate, dateFormat))) {
                    req.sessionModel.set('claimants-start-date', (claimantsData.data['claimants'][0]['startDate']).split('T')[0]);
                    req.sessionModel.set('claimants-end-date', (claimantsData.data['claimants'][0]['endDate']).split('T')[0]);
                    req.sessionModel.set('calculated-earliest-date', earliestCalDate.format(dateFormat));
                    return res.redirect(`${req.baseUrl}/currently-not-eligible`);
                } else {
                    return next();
                }
            } else {
                return next();
            }
        } catch (error) {
            return next();
        }
    }
}
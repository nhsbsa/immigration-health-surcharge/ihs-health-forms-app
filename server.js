'use strict';
const dotenv = require('dotenv')
const hof = require('hof-govfrontend-v3');
const express = require('express');
const config = require('./config');
const bodyParser = require('busboy-body-parser');
const { startPageUrl } = require('./configRoutes');
const { sessionTimeOut, sessionTimeOutWarning, finalSessionTimeOutWarning } = require('./apps/common/constants/session-timeout');

dotenv.config();

let appName = 'ihs-health-forms-app';

let settings = require('./hof.settings');

settings = Object.assign({}, settings, {
  routes: settings.routes.map(require),
  redis: config.redis,
  path: "/assets",
  govukAssetPath: "/govukAssetPathhealthpath/",
  ga4TagId: process.env.GA_4_TAG,
  serviceName: process.env.SERVICE_NAME,
  noCache: process.env.NO_CACHE,
  csp: {
    disabled: process.env.DISABLE_CSP === 'true',
    "connect-src": `'self' https://www.google-analytics.com https://region1.google-analytics.com https://directory.spineservices.nhs.uk ${process.env.MULTI_FILE_URL}multifile/evidence;`
  }
});

const app = hof(settings);



app.use((req, res, next) => {
  // Set HTML Language
  res.locals.htmlLang = 'en';
  res.locals.appName = appName;
  res.locals.cookieBannerHeaderTxt = 'Apply for your immigration health surcharge reimbursement';

  // session timeout configs
  res.locals.sessionTimeOut = sessionTimeOut;
  res.locals.sessionTimeOutWarning = sessionTimeOutWarning;
  res.locals.finalSessionTimeOutWarning = finalSessionTimeOutWarning;  
  // Set feedback and footer links
  res.locals.startPageRedirectUrl = startPageUrl;
  res.locals.helpBaseUrl = '/health/help/';
  res.locals.assetPath = '/health/public';
  res.locals.govukAssetPath = "/assets/";
  res.locals.feedbackUrl = process.env.FEEDBACK_URL;
  res.locals.footerSupportLinks = [
    { path: 'https://www.gov.uk/help', property: 'Help', id: 'help-link' },
    { path: '/health/help/cookies', property: 'Cookies', id: 'cookie-link' },
    { path: '/health/help/contact', property: 'Contact', id: 'contact-link' },
    { path: '/health/help/accessibility-statement', property: 'Accessibility statement', id: 'accessibility-link' },
    { path: '/health/help/terms-conditions', property: ' Terms and conditions', id: 'terms-and-conditions-link' },
    { path: '/health/help/privacy-notice', property: 'Privacy', id: 'privacy-link' }
  ];
  next();
});

app.use(bodyParser({ limit: config.upload.maxFileSize }));
app.use(express.static(__dirname + '/'));

app.use('/health/claim', (req, res, next) => {
  if (req.session && req.session['hof-wizard-health-claim']
    && req.session['hof-wizard-health-claim'].hasOwnProperty('health-claim-submit-flag')
    && req.url != '/done'
    && req.url != '/start?sessionCleared=true'
    && req.url != '/clear-session'
  ) {
    return res.redirect(`/health/session-timeout`);
  } else {
    next();
  }
});

app.use('/health/eligibility', (req, res, next) => {
  if (req.session && req.session['hof-wizard-health-claim']
    && req.session['hof-wizard-health-claim'].hasOwnProperty('health-claim-submit-flag')
  ) {
    return res.redirect(`/health/session-timeout`);
  } else {
    next();
  }
});

app.use('/health/clear-session', (req, res) => {
  req.session.destroy();
  res.redirect(`/health/eligibility/immigration-health-surcharge-paid`);
});
app.use('/health/session-timeout', (req, res) => {
  res.render('session-timeout', res.locals);
});
app.use('/health/test-server-error', (req, res) => {
  throw new Error('Test error');
});

module.exports = app;

const Controller = require('hof-govfrontend-v3').controller;
const moment = require('moment');

module.exports = class ClaimReferenceNumberController extends Controller {

    getValues(req, res, callback) {
        const field = this.options.locals.field;
        super.getValues(req, res, callback);
    }

    locals(req, res, callback) {
        const locals = super.locals(req, res, callback);
        locals.claimReferenceNumber = req.sessionModel.get('claim-reference-number');
        locals.claimStartDate = moment(res.locals.values['claim-start-date'], "YYYY-MM-DD").format('DD MMMM YYYY');
        locals.claimEndDate = moment(req.sessionModel.get('claim-calculated-end-date'), "YYYY-MM-DD").format('DD MMMM YYYY');
        return locals;
    }
};
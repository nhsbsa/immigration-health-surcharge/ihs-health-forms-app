module.exports = class DependantModel {
  constructor(model) {
    this['dependant-name'] = [model['dependant-given-name'], ' ', model['dependant-family-name']].join('');
    this['dependant-given-name'] = model['dependant-given-name'];
    this['dependant-family-name'] = model['dependant-family-name'];
    this['dependant-date-of-birth'] = model['dependant-date-of-birth'];
    this['dependant-immigration-health-surcharge-number'] = model['dependant-immigration-health-surcharge-number'].toUpperCase();
    this['dependant-id'] = model['dependant-id'];
  }

  editDependant(model){
    const editDep = model;
    editDep['dependant-name'] = [model['dependant-given-name'], ' ', model['dependant-family-name']].join('');
    editDep['dependant-immigration-health-surcharge-number'] = model['dependant-immigration-health-surcharge-number'].toUpperCase();    
      
   return editDep;
  }
}
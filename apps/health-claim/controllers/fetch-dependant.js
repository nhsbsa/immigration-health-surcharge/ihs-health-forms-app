const Controller = require('hof-govfrontend-v3').controller;
const PersonalDetailsModel = require('../models/personal-details');
const DependantModel = require('./../models/dependant');

module.exports = class DependantAddController extends Controller {

  process(req, res, next) {
    const dependantId = req.sessionModel.get('edit-dependantId');
    
    if(dependantId){
      let allDependants = req.sessionModel.get('all-dependants');
      const dependantIndex = allDependants.findIndex(dependant => dependant['dependant-id'] === dependantId);  
      const locals = super.locals(req, res,next);
      locals.fields.forEach((formfield, index) => {  
        allDependants[dependantIndex][formfield.key] =req.form.values[formfield.key] ;          
      });   
   
      const depmodel =  new DependantModel(allDependants[dependantIndex]);
      allDependants[dependantIndex] = depmodel.editDependant(depmodel);
      req.sessionModel.set('all-dependants', allDependants);
      req.sessionModel.unset('dependant-id');
    }   
    next();
  }

  successHandler(req, res, next) {
    const dependantId = req.sessionModel.get('edit-dependantId');
    if(dependantId){
      req.sessionModel.unset('edit-dependantId');
      if(req.sessionModel.get('check-dependants')){
        req.sessionModel.unset('check-dependants');
        const nextNav = '/check-dependants';
        req.form.options['confirmStep'] = nextNav;
        return res.redirect(`${req.baseUrl}${nextNav}`);
      }else{
        if(req.sessionModel.get('check-your-answers')){
          const nextNav = '/check-your-answers';
          req.sessionModel.unset('check-dependants');
          req.sessionModel.unset('check-your-answers');      
          return res.redirect(`${req.baseUrl}${nextNav}`); 
        }           
      }
    }
    return super.successHandler(req, res, next);    
  }
  
};
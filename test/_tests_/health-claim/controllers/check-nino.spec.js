const checkNino = require('../../../../apps/health-claim/controllers/check-nino');
const HOFController = require('hof-govfrontend-v3').controller;
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash');


describe('Check Nino', () => {
    let req;
    let res;
    const next = sinon.stub();

    beforeEach(() => {
        req = reqres.req();
        res = reqres.res();
    });
    describe('Check Nino Controller instance', () => {
        let checkNinoObj;
        const validationError = {
            type: 'regex'
          };

        beforeEach(() => {
            checkNinoObj = new checkNino({});
            checkNinoObj.ValidationError = sinon.stub();
            checkNinoObj.ValidationError.returns(validationError);
        });

        it('extends the hof-form-controller', () => {
            checkNinoObj.should.be.instanceof(HOFController);
        });

        it('has locals, process methods', () => {
            checkNinoObj.should.have.property('process').that.is.a('function');
        });
        
        it('calls process with arguments', () => {
            checkNinoObj.process(req, res, next);
          });

          it('calls process with arguments', () => {
            let ninoValue = 'IHS123123123';
           });
    });
});

'use strict';

const personalDetailsSection = [
  'name',
  'date-of-birth',
  'national-insurance-number',
  'immigration-health-surcharge-number',
  'phone-number',
  'email',  
];

const dependantSections = [
  'dependant-name',
  'dependant-date-of-birth',
  'dependant-immigration-health-surcharge-number'
];

module.exports = {
  dependantSections,
  personalDetailsSection
};

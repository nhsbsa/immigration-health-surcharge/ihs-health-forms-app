const Controller = require('hof-govfrontend-v3').controller;
const EmployerModel = require('../models/employer');

module.exports = class JobSettingController extends Controller {

  locals(req, res, callback) {
    let formSetting = {
      'job-setting': req.form.options.fields['job-setting']
    };
    req.form.options.fields = formSetting;
    const locals = super.locals(req, res, callback);
    return locals;
  }

  modifyEmployerDetails(req, employerId) {
    let allEmployers = req.sessionModel.get('all-employers');
    const employerIndex = allEmployers.findIndex(employer => employer['employer-id'] === employerId);
    allEmployers[employerIndex]['job-setting'] = req.form.values['job-setting'];
    allEmployers[employerIndex]['other-job-setting'] = (req.form.values['job-setting'] == 'other') ? req.form.values['other-job-setting'] : '';
    const empmodel = new EmployerModel(allEmployers[employerIndex]);
    allEmployers[employerIndex] = empmodel.editEmployer(empmodel);
    req.sessionModel.set('all-employers', allEmployers);
    return allEmployers;
  }

  process(req, res, next) {
    const nonWhiteSpace = /(.*[a-zA-Z0-9]){3}/gi;
    if (req.form.values['job-setting'] == 'other') {
      if (!req.form.values['other-job-setting']) {
        return this.flashError(req, res, next, 'required');
      } else if (!nonWhiteSpace.test(req.form.values['other-job-setting'])) {
        return this.flashError(req, res, next, 'minlength');
      }
    }
    const employerId = req.sessionModel.get('edit-employerId');
    if (employerId) {
      this.modifyEmployerDetails(req, employerId);
      if (req.sessionModel.get('check-employment')) {
        req.sessionModel.unset('check-employment');
      }
    } else {
      const employerId = req.sessionModel.get('employer-id');
      this.modifyEmployerDetails(req, req.sessionModel.get('employer-id'));
    }

    if(req.sessionModel.get('check-your-answers')){
      let index = req.session['hof-wizard-health-claim'].steps.indexOf('/check-employment');
      if (index > -1) {
        req.session['hof-wizard-health-claim'].steps.splice(index, 1); // 2nd parameter means remove one item only
      }
    }
    next();
  }

  flashError(req, res, next, validationType) {
    const err = new this.ValidationError('other-job-setting', {
      type: validationType
    }, req, res);
    const errorObject = {};
    errorObject['other-job-setting'] = err;
    return next(errorObject);
  }

  successHandler(req, res, next) {
    return super.successHandler(req, res, next);
  }
};
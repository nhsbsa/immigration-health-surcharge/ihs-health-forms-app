'use strict';
const Controller = require('hof-govfrontend-v3').controller;
module.exports = class setTestSettings extends Controller {
    getValues(req, res) {
        const validationDate = req.query.validationDate;
        if (validationDate && process.env.SET_TEST_SETTING_FLAG == 'true') {
            req.sessionModel.set('validation-date', validationDate);
        }
        return res.redirect(`${req.baseUrl}/name`);
    }
};
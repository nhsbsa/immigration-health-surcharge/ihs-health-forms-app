const Controller = require('hof-govfrontend-v3').controller;
const BsaRefModel = require('../models/bsa-reference');
const Claim = require('../models/claim');
const config = require('./../../../config');
const path = require('path');

module.exports = class BsaRefController extends Controller {
  async process(req, res, next) {
    try {
      let claim = req.sessionModel.get('claim') ? req.sessionModel.get('claim') : [];
      if (!claim.bsaRef || claim.bsaRef == '') {
        claim = await this.createNewDraftClaim(req.sessionID, next);
        req.sessionModel.set('claim', claim);
      }
      req.form.values['phone-number'] =  req.form.values['phone-number'].replace(/ /g, '').replace('(', '').replace(')', '');
      let validate = this.checkValidation(req.form.values['phone-number'], req, res);
      if (!validate.status) {
        return next(validate.errorObject);
      } else {
          return next();
      }
    } catch {
      const err = new this.ValidationError('phone-number', {
        type: 'error'
      }, req, res);
      const errorObject = {};
      errorObject['phone-number'] = err;
      return next(errorObject);
    }
  }

  flashError(req, res, validationType) {
    return {
      'phone-number': new this.ValidationError('phone-number', {
            type: validationType
        }, req, res)
    };
}

  checkValidation(formValue, req, res) {
    if (formValue.length === 0)
      return { status: true, errorObject: {} };

    if (!formValue.match(/^\d+$/))
      return { status: false, errorObject: this.flashError(req, res, 'regex') };
    
    if (formValue.match(/\d/g).length !=11)
      return { status: false, errorObject: this.flashError(req, res, 'maxlength') };
    
    return { status: true, errorObject: {} };
  }
  async createNewDraftClaim(sessionId, next) {
    const claimObj = new Claim();
    claimObj.sessionId = sessionId;
    claimObj.cohort = config.cohort;
    const bsaRefModel = new BsaRefModel();
    claimObj.bsaRef = await bsaRefModel.getBSARef(claimObj, next);
    return claimObj;
  }
};
const EditEmployerController = require('../../../../apps/health-claim/controllers/edit-employer');
const HOFController = require('hof-govfrontend-v3').controller;
const mockData = require('../../../_mocks_/health-claim/controllers/employer-details');
const {  reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('EditEmployerController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('EditEmployerController instance', () => {
    let editEmployerController;
    const employerId = '1';
    const pagename = 'abc';
    beforeEach(() => {
      // an instance of the EditEmployerController
      editEmployerController = new EditEmployerController({});
    });

    it('extends the hof-form-controller', () => {
      editEmployerController.should.be.instanceof(HOFController);
    });

    it('has get, editEmployer methods', () => {
      editEmployerController.should.have.property('get').that.is.a('function');
      editEmployerController.should.have.property('editEmployer').that.is.a('function');
    });

    describe('get', () => {
      beforeEach(() => {
        editEmployerController.editEmployer = sinon.stub();
      });

      it('calls next() skiping consdition if there is no employerId', () => {
        req.query = {};
        editEmployerController.get(req, res, next);
      });

      it('calls editEmployer when employerId is available', () => {
        req.query = { employerId };
        req.query = { pagename };
        editEmployerController.get(req, res, next);
        //editEmployerController.get.should.have.been.calledWithExactly(req, res, next);
      });
    });

    describe('editEmployer', () => {
      beforeEach(() => {
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();
        req.sessionModel.unset = sinon.stub();        
        res.redirect = sinon.stub();
        req.session = sinon.stub();
        req.session['hof-wizard-health-claim'] = sinon.stub();
        req.baseUrl = '/health-claim';
      });

      it('skips editEmployer if employer with Id not found', () => {
        req.sessionModel.get.returns([]);
        editEmployerController.editEmployer(req, res, employerId);
        res.redirect.should.have.been.called;
      });

      it('Calls redirect to add employer page, if all employers are deleted', () => {
        req.sessionModel.get.returns([{ 'employer-id': employerId }]);
        editEmployerController.editEmployer(req, res, employerId,'add-employer');
        res.redirect.should.have.been.calledWithExactly('/health-claim/add-employer?employerId=1');
      });

      it('Calls redirect to check employer page, if all dependants are not deleted', () => {
        req.sessionModel.get.returns([{ 'employer-id': employerId }, { 'employer-id': '1' }]);
        editEmployerController.editEmployer(req, res, employerId,'check-employment');
        res.redirect.should.have.been.calledWithExactly('/health-claim/check-employment?employerId=1');
      });

      it('Redirects based on the error and number of employers in the session ', () => {
        req.sessionModel.get.returns([{ 'employer-id': employerId }, { 'employer-id': '1' }]);
        _.set(req, 'hof-wizard-health-claim',   'errors'    );
        editEmployerController.editEmployer(req, res, employerId,'check-employment');
        res.redirect.should.have.been.calledWithExactly('/health-claim/check-employment?employerId=1');
      });
/*
      it('calls next() with validation error', () => {        
        const addMoreEmployersError = { 'add-more-employer': { type: 'maxEmployer' } };

         req.sessionModel.get.returns(addMoreEmployersError);
        _.set(req, 'hof-wizard-health-claim', {
          "add-more-employer": 'yes'
        });
        
        editEmployerController.editEmployer(req, res, employerId);
      //  res.redirect.should.have.been.calledWithExactly('/add-employer');
      });*/
    });
  });

});
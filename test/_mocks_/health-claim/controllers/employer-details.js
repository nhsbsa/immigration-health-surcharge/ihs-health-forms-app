
const sessionModel = {
  'employer-name':' NHA AIRDALE',
  'organisation-id':'1',
  'job-title':'abc',
  'job-role-id':'123',
  'job-setting':'pqr',
  'other-job-setting':'-',
  'employer-id':'1',
};

const allEmployer = [{
  'employer-name':' NHA AIRDALE',
  'organisation-id':'1',
  'job-title':'abc',
  'job-role-id':'123',
  'job-setting':'pqr',
  'other-job-setting':'-',
  'employer-id':'1',

  ...sessionModel
}];

const maxEmployers= [
  {
    'employer-id': 'abc001'
  },
  {
    'employer-id': 'abc002'
  },
  {
    'employer-id': 'abc003'
  },
  {
    'employer-id': 'abc004'
  },
  {
    'employer-id': 'abc005'
  },
  {
    'employer-id': 'abc006'
  },
  {
    'employer-id': 'abc007'
  },
  {
    'employer-id': 'abc008'
  },
  {
    'employer-id': 'abc009'
  },
  {
    'employer-id': 'abc010'
  }
];

const addMoreEmployerError = { 'add-more-employer': { type: 'maxEmployer' } };



const deleteEmployer = 'Employer One';

module.exports = {
  sessionModel,
  allEmployer,
  deleteEmployer,
  maxEmployers,
  addMoreEmployerError
}

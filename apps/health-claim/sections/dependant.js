'use strict';

module.exports = [
  'dependant-name',
  'dependant-date-of-birth',
  'dependant-immigration-health-surcharge-number'
];

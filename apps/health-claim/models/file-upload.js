'use strict';
const Model = require('hof-govfrontend-v3').model;
const axios = require('axios').default;

module.exports = class FileUploadModel extends Model {

  async fileListAPI(claim, sessionID) {
    try {
      const url = process.env.MULTI_FILE_URL + 'multifile/evidence';
      const headers = { "Content-type": "application/json", "sessionId": sessionID };
      let resp =  axios.get(url, { headers });
      return resp;
    } catch (error) {
      console.log('Error uploading file evidence for claim :  ' + error);
      return new Error(error);
    }
  }

  async fileuploadAPI(claim, file, next) {
    try {
      const url = process.env.BSA_URL + 'claims/' + claim.bsaRef + '/evidence';
      const headers = { "Content-type": "application/json", "x-api-key": process.env.IHS_API_KEY };
      console.log("Uploading evidence for "+ claim.bsaRef );
      return await axios.post(url, file, { headers });
    } catch (error) {
      console.log('Error uploading file evidence for claim : ' + claim.bsaRef + ' ' + error);
    }
  }

  async fileDeleteAPI(claim, fileId) {
    try {
      const url = process.env.BSA_URL + 'claims/' + claim.bsaRef + '/evidence/' + fileId;
      const headers = { "x-api-key": process.env.IHS_API_KEY };
      console.log("Deleting evidence for "+ claim.bsaRef );
      return await axios.delete(url, { headers });
    } catch (error) {
      console.log('Error deleting file evidence for claim : ' + claim.bsaRef + ' ' + error);
    }
  }
};
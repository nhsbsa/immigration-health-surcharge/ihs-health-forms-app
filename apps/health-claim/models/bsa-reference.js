'use strict';
const Model = require('hof-govfrontend-v3').model;
const axios = require('axios').default;
const ClaimUtility = require('../utility/claim-utility');

module.exports = class BsaRefService extends Model {
  async getBSARef(claim, next) {
    try {
      const headers = { "Content-type": "application/json", "x-api-key": process.env.IHS_API_KEY };
      return await axios.post(process.env.BSA_URL + "claims", ClaimUtility.bsaRefData(claim), { headers }).then(bsaRef => {
        console.log('New claim reference created : ' + bsaRef.data.bsaReference);
        return bsaRef.data.bsaReference;
      }).catch(error => {
        if ((error.response.status === 409) && (error.response.data.message === 'Request has been processed previously')) {
          console.log('Claim reference called again with 409 : ' + error.response.data.bsaReference);
          return error.response.data.bsaReference;
        }
        console.log('Error creating claim reference!' + error);
        next(new Error(error.body));
      });
    } catch (error) {
      console.log('Error creating claim reference!!' + error);
      next(new Error(error.body));
    }
  }

  async getEndDate(claimStartDate) {
    return new Promise(async (resolve, reject) => {
      try {
        const headers = { "Content-type": "application/json", "x-api-key": process.env.IHS_API_KEY };
        let endDate = await axios.get(`${process.env.BSA_URL}claims/periods/end-date?start_date=${claimStartDate}`, { headers });
        resolve(endDate);
      } catch (error) {
        reject(error);
      }
    })
  }

  async getClaimantsSearch(claimPayload) {
    return new Promise(async (resolve, reject) => {
      try {
        const headers = { "Content-type": "application/json", "x-api-key": process.env.IHS_API_KEY };
        let endDate = await axios.post(`${process.env.BSA_URL}claimants/search`, claimPayload, { headers });
        resolve(endDate);
      } catch (error) {
        reject(error);
      }
    })
  }

  async getEarliestDate(claimStartDate) {
    return new Promise(async (resolve, reject) => {
      try {
        const headers = { "Content-type": "application/json", "x-api-key": process.env.IHS_API_KEY };
        let endDate = await axios.get(`${process.env.BSA_URL}claims/periods/renewal-date?end_date=${claimStartDate}`, { headers });
        resolve(endDate);
      } catch (error) {
        reject(error);
      }
    })
  }
}
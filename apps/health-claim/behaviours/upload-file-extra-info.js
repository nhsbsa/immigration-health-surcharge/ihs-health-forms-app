'use strict';
const path = require('path');
const ClaimUtility = require('../utility/claim-utility');
const FileUploadModel = require('../models/file-upload');
const fileUploadModel = new FileUploadModel();
const HOF_WIZARD_HEALTH_CLAIM = 'hof-wizard-health-claim';

module.exports = superclass => class UploadFile extends superclass {
  async process(req, res, next) {
    try {
      const locals = super.locals(req, res, next);
      const currentStep = 'extra-information';
      const file = req.files['extra-info-upload-evidence'];    
    
      if (file == null) {
        const commentcheck = this.checkExtraInformationValidation(req, res, next,currentStep) ;
        if (!commentcheck.status){
          req.sessionModel.set('TempExtraInfo',req.form.values['extra-information']);
          throw commentcheck;        
        }else{
        req.sessionModel.set('extra-information',req.form.values['extra-information']);
        req.session[HOF_WIZARD_HEALTH_CLAIM].steps.includes('/extra-evidence-uploaded') ?
        '' : req.session[HOF_WIZARD_HEALTH_CLAIM].steps.push('/extra-evidence-uploaded');
        return res.redirect(`${req.baseUrl}/check-your-answers`);
       }
      }else{
        //save extra-information properly
        req.sessionModel.set('TempExtraInfo',req.form.values['extra-information']);
        const validationObj = this.checkValidation(req, res, next, currentStep);
        if (!validationObj.status){
          throw validationObj;        
        }else{   
          const commentcheck = this.checkExtraInformationValidation(req, res, next,currentStep);
          if (!commentcheck.status ) {
            throw commentcheck;
          }
          req.sessionModel.set('extra-information',req.form.values['extra-information']);
          const fileCountFlag = this.checkFileCount(req, res, next, currentStep);
        
          if (!fileCountFlag) {
            return res.redirect(`${req.baseUrl}/extra-evidence-uploaded`);
          }
          req.files['extra-info-upload-evidence'].name = req.files['extra-info-upload-evidence'].name.replace(/ /g, "_");
      
          let fileUploadObj = await this.fileupload(req, currentStep, next);
          if ((!fileUploadObj) || (!fileUploadObj.data.status) || fileUploadObj.data.status != '200') {
            console.log('Error occured while uploading File, Please try again later! ');
            throw   { status: false, validationType: 'api-error', currentStep: currentStep, message: req.files['extra-info-upload-evidence'].name +" was not uploaded. Try again"};
          } else {          
            this.incrementEvidenceCount(req, locals, currentStep, fileUploadObj['data']['fileid']);
            let index = req.session[HOF_WIZARD_HEALTH_CLAIM].steps.indexOf('/extra-evidence-uploaded');
            if (index > -1) {
              req.session[HOF_WIZARD_HEALTH_CLAIM].steps.splice(index, 1); // 2nd parameter means remove one item only
            }
            return next();
          }        
        }
      }
    } catch (error) {    
        const pattern = /^[a-zA-Z0-9\n\s,£.\-']*$/;

        if (!pattern.test(req.form.values['extra-information'])) {
          req.sessionModel.set('extraInfoErrorMsg', "Enter extra information using only letters, numbers, spaces, hyphens, apostrophes, commas, full stops and pound signs");
        }  
        if (req.form.values['extra-information'].length > 2000) {
          req.sessionModel.set('extraInfoErrorMsg', "Enter extra information using no more than 2000 characters");
        }
        if(req.files['extra-info-upload-evidence'] != null){
          if(!error.message){
            req.sessionModel.set('evidenceUploadErrorMsg', `${req.files['extra-info-upload-evidence'].name} was not uploaded. Try again`);
          }else{
            req.sessionModel.set('evidenceUploadErrorMsg', error.message);
          }        
        }
        return res.redirect(`${req.baseUrl}/extra-information`);
    }
  }

  incrementEvidenceCount(req, locals, currentStep, fileId) {
    currentStep = 'extra-info-upload-evidence';
    const uploadedFileCountExtraInfo = req.sessionModel.get('uploadedFileCountExtraInfo');
    req.sessionModel.set('uploadedFileCountExtraInfo', uploadedFileCountExtraInfo ? uploadedFileCountExtraInfo + 1 : 1);
    let uploadedFileList = []
    if (req.sessionModel.get(locals.fileNameKey) == undefined) {
      req.sessionModel.set(locals.fileNameKey, [{ 'srNo': 1, 'fileName': req.files[currentStep].name, 'fileId': fileId }]);
    } else {
      uploadedFileList = req.sessionModel.get(locals.fileNameKey);
      uploadedFileList.push({ 'srNo': req.sessionModel.get('uploadedFileCountExtraInfo'), 'fileName': req.files[currentStep].name, 'fileId': fileId });
      req.sessionModel.set(locals.fileNameKey, uploadedFileList);
    }
    req.sessionModel.set(currentStep, req.files[currentStep].name);
  }

  checkFileCount(req, res, next, currentStep) {
    if (req.sessionModel.get('uploadedFileCountExtraInfo') >= process.env.COMMENT_MAX_FILE_COUNT) {
      req.sessionModel.set('extraInfoMaxFileUpload', true);
      return false;
    }
    return true;
  }

  flashError(req, res, next, validationType) {
    const err = new this.ValidationError('extra-information', {
      type: validationType
    }, req, res);
    const errorObject = {};
    errorObject['extra-information'] = err;
    return next(errorObject);
  }

  checkExtraInformationValidation(req, res, next,currentStep) {
    const pattern = /^[a-zA-Z0-9\n\s,£.\-']*$/;
    if (!pattern.test(req.form.values['extra-information'])) {
      return { status: false, validationType: 'regex', currentStep: currentStep};    
    }  
    if (req.form.values['extra-information'].length > 2000) {
      return { status: false , validationType: 'maxlength', currentStep: currentStep};
    } 
    return { status: true};
  }

  checkValidation(req, res, next, currentStep='extra-information') {   
    currentStep='extra-information';
    const file = req.files['extra-info-upload-evidence'];
    const pattern = /^[a-z0-9\-_\s]+$/i;
    const extension = path.extname(file.name);
    let filename = path.basename(file.name, extension);

    if (filename.length > Number(process.env.MAX_FILENAME_LENGTH )) {
      return { status: false, validationType: 'filelength', currentStep: currentStep, message: `${filename}${extension} must be 130 characters or less` };
    }

    if (!pattern.test(filename)) {
      return { status: false, validationType: 'regex', currentStep: currentStep, message: `${filename}${extension} must use only letters, numbers, spaces, hyphens and underscore` };
    }  

    if (!this.fetchExt(file.name)) {
      return { status: false, validationType: 'extname', currentStep: currentStep, message: `${filename}${extension} must be a jpg, bmp, png or pdf` };
    }
  
    if (file && file.truncated) {
      return { status: false, validationType: 'filesize', currentStep: currentStep, message: `${filename}${extension} must be less than 2MB in size` };
    } else if (file.size > process.env.FILE_SIZE_LIMIT) {
      return { status: false, validationType: 'filesize', currentStep: currentStep, message: `${filename}${extension} must be less than 2MB in size` };
    }
    
    return { status: true, validationType: 'filesize', currentStep: currentStep };
  }
  render(req, res, next) {
    let evidenceDeletedInfo = req.sessionModel.get('evidenceDeletedInfo') ? req.sessionModel.get('evidenceDeletedInfo') : false;
    res.locals.evidenceDeletedInfo = evidenceDeletedInfo ? `File named ${evidenceDeletedInfo[0]['fileName']} has been deleted` : false;
    if (req.query.deleteAllWantedFile == 'true' && evidenceDeletedInfo == false) {
      res.locals.evidenceDeletedInfo = `All unsuccessful files have been deleted`;
    }

    if(req.sessionModel.get('TempExtraInfo')){
      req.form.values['extra-information'] = req.sessionModel.get('TempExtraInfo');
      req.sessionModel.unset('TempExtraInfo');    
    }
    if(req.sessionModel.get('check-your-answers')){
      res.locals.backLink = 'attachments/edit'; 
    }
    res.locals.errorLogMsg = req.sessionModel.get('evidenceUploadErrorMsg') ? req.sessionModel.get('evidenceUploadErrorMsg') : false;
    res.locals.extraInfoErrorMsg = req.sessionModel.get('extraInfoErrorMsg') ? req.sessionModel.get('extraInfoErrorMsg') : false;
    
    if(res.locals.errorLogMsg != false || res.locals.extraInfoErrorMsg != false){
      res.locals.isError = true;   
    }else{
      res.locals.isError = false;
    }
    req.sessionModel.unset('evidenceDeletedInfo');
    req.sessionModel.unset('evidenceUploadErrorMsg');
    req.sessionModel.unset('extraInfoErrorMsg');   
    return super.render(req, res, next);
  }
  async fileupload(req, currentStep, next) {
    const claim = req.sessionModel.get('claim');
    let file = ClaimUtility.fileData(req.files['extra-info-upload-evidence']);
    file.fileName =  "extra-info-"+file.fileName ;
    return fileUploadModel.fileuploadAPI(claim, file, next);
  }

  fetchExt(value) {
    return value && [
      '.jpeg',
      '.jpg',
      '.bmp',
      '.png',
      '.pdf'
    ].includes(path.extname(value).toLowerCase());
  }
};
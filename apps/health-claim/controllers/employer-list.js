const Controller = require('hof-govfrontend-v3').controller;

module.exports = class EmployerListController extends Controller {

  process(req, res, next) {
    req.sessionModel.set('subscribe-for-reminder',  req.form.values['subscribe-for-reminder']);
    if(req.sessionModel.get('check-your-answers')){
      const nextNav = '/check-your-answers';
      return res.redirect(`${req.baseUrl}${nextNav}`);
    }
    const allEmployers = req.sessionModel.get('all-employers');
    if((allEmployers && allEmployers.length > 0 ) && ( req.sessionModel.get('check-employment')) && 
      !(allEmployers.length == 1 && allEmployers[0]['job-title'].length == 0)
    ){
        const nextNav = '/check-employment';
        return res.redirect(`${req.baseUrl}${nextNav}`);
    }else{
       next();
    }    
  }
  
  locals(req, res, callback) {
    const locals = super.locals(req, res, callback);
    if(req.sessionModel.get('subscribe-for-reminder') != ''){
      req.form.values['subscribe-for-reminder']=req.sessionModel.get('subscribe-for-reminder');  
    }
    return locals;
  }
};
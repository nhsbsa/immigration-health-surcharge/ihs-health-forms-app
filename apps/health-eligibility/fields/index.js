'use strict';
const hints = require('../translations/src/en/hints.json');

module.exports = {
  'immigration-health-surcharge-paid': {
    mixin: 'radio-group',
    legend: {
      className: 'visuallyhidden'
    },
    includeInSummary: false,
    options: ['yes', 'no'],
    validate: 'required'
  },
  'applicant-visa-type': {
    mixin: 'radio-group',
    legend: {
      className: 'visuallyhidden'
    },
    hintHTML: hints['applicant-visa-type'].hint,
    includeInSummary: false,
    options: ['tier2', 'tier5', 'other'],
    validate: 'required'
  },
  'qualified-health-professional': {
    mixin: 'radio-group',
    legend: {
      className: 'visuallyhidden'
    },
    hintHTML: hints['qualified-health-professional'].hint,
    includeInSummary: false,
    options: ['yes', 'no'],
    validate: 'required'
  },
  'health-social-care-job': {
    mixin: 'radio-group',
    legend: {
      className: 'visuallyhidden'
    },
    hintHTML: hints['health-social-care-job'].hint,
    includeInSummary: true,
    options: ['yes', 'no'],
    validate: 'required'
  },
  'job-6-months': {
    mixin: 'radio-group',
    legend: {
      className: 'visuallyhidden'
    },
    hintHTML: hints['job-6-months'].hint,
    includeInSummary: false,
    options: ['yes', 'no'],
    validate: 'required'
  },
  'job-average-16-hours': {
    mixin: 'radio-group',
    legend: {
      className: 'visuallyhidden'
    },
    hintHTML: hints['job-average-16-hours'].hint,
    includeInSummary: false,
    options: ['yes', 'no'],
    validate: 'required'
  }
};

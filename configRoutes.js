'use strict';
const dotenv = require('dotenv')
dotenv.config();
module.exports = {
  informationAboutEHIC:process.env.INFO_EHIC,
  startPageUrl:process.env.START_PAGE_URL,
  viewAndProveImmigrationStatus:process.env.IMMIGRATION_STATUS,
  completionPageFeedbackURL: process.env.COMPLETION_FEEDBACK_URL,
  onlinesurveys: process.env.ONLINE_SURVEY,
  moreInfoData: process.env.MORE_INFO,
  tier2MedialLink: process.env.TIER2_MEDIA_LINK,
  contactUKLink:process.env.CONTACT_UK_LINK
};
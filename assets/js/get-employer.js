var autocomplete = require('./autocomplete-new');

document.getElementById("primary-role-id").value = '';
autocomplete({
  input: document.getElementById("employer-name"),
  fetch: function (text, update) {
    text = text.toLowerCase().substring(0, 75);
    fetch('https://directory.spineservices.nhs.uk/ORD/2-0-0/organisations?OrgRecordClass=RC1&_format=json&Status=Active&Limit=100&Name=' + encodeURIComponent(text) )
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        var suggestions = [];
        data.Organisations.forEach(filterFunction);
        function filterFunction(value, index, array) {
           if (value.Name.toLowerCase().indexOf(text.toLowerCase()) !== -1) {
            suggestions.push({ "label": value.Name , value: value.PrimaryRoleId });
          }
        }
        const key = 'label';
        const arrayUniqueByKey = [...new Map(suggestions.map(item => [item[key], item])).values()];
        update(Array.from(new Set(arrayUniqueByKey)));
      })
      .catch(function (err) {
        console.log('Error calling ODS', err);
      });
  },
  minLength: 3,
  onSelect: function (item) {
    document.getElementById("employer-name").value = item.label;
    document.getElementById("primary-role-id").value = item.value;
  }
});
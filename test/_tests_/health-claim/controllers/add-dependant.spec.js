const DependantAddController = require('../../../../apps/health-claim/controllers/add-dependant');
const HOFController = require('hof-govfrontend-v3').controller;
const {  reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('DependantAddController', () => {
  let req;
  let res;
  const next = sinon.stub();
  const callback = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('DependantAddController instance', () => {
    let dependantAddController;

    beforeEach(() => {
      // an instance of the DependantAddController
      dependantAddController = new DependantAddController({});
    });

    it('extends the hof-form-controller', () => {
      dependantAddController.should.be.instanceof(HOFController);
    });

    it('has locals, process methods', () => {
      dependantAddController.should.have.property('locals').that.is.a('function');
      dependantAddController.should.have.property('process').that.is.a('function');
      dependantAddController.should.have.property('render').that.is.a('function');
    });

    describe('render', () => {
      beforeEach(() => {         
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();  
      });
    
      it('calls render with arguments', () => {            
        req.sessionModel.get.withArgs('check-your-answers').returns('/edit');
      //  dependantAddController.render(req, res, callback);      
      });  
    });

    describe('locals', () => {
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'locals').returns({
          fields: []
        });
        req.sessionModel.get = sinon.stub();
        req.sessionModel.unset = sinon.stub();
      });
      afterEach(() => {
        HOFController.prototype.locals.restore();
      });

      it('calls super.locals', () => {
        dependantAddController.locals(req, res, next);
        HOFController.prototype.locals.should.have.been.calledOnce
          .and.calledWithExactly(req, res, next);
      });

      it('calls gets deletedDependant from the sessionModel', () => {
        dependantAddController.locals(req, res, next);
        req.sessionModel.get.should.have.been.calledWithExactly('deletedDependant');
      });

      it('calls unsets deletedDependant from the sessionModel', () => {
        dependantAddController.locals(req, res, next);
        req.sessionModel.unset.should.have.been.calledWithExactly('deletedDependant');
      });

      it('sets locals value for deletedDependant to true', () => {
        req.sessionModel.get.returns(true);
        dependantAddController.locals(req, res, next).deletedDependant.should.be.true;
      });
    });

    describe('process', () => {
      let getTimeValue = '7865432645';
      const validationError = {
        type: 'maxDependants'
      };
      const err = [{'dependant-question' : [ {
        type: 'required'
      }]}];

      beforeEach(() => {
        sinon.stub(Date.prototype, 'getTime').returns(getTimeValue);
        req.sessionModel.set = sinon.stub();
        req.sessionModel.get = sinon.stub();        
      });
      afterEach(() => {
        Date.prototype.getTime.restore();
      });

      it('calls process when dependant-question error is not set', () => {    
        _.set(dependantAddController, 'req.form.values', {})    
        dependantAddController.process(req, res, next);  
      });

      it('calls process when dependant-question error is set', () => {    
        req.form.values['dependant-question'] = 'yes';  
        dependantAddController.process(req, res, next);  
      });
      it('calls process when user comes back from check-your-answers', () => {        
       req.sessionModel.get.returns([{'check-your-answers' : true}]);        
         req.form.values['dependant-question'] = 'no';  
          dependantAddController.process(req, res, next);  
      });

      it('calls process when user comes back from check-your-answers', () => {    
       req.sessionModel.get.returns([{'check-your-answers' : true},{'dependant-question' : true}]);       
       req.form.values['dependant-question'] = 'yes';  
        dependantAddController.process(req, res, next);  
      });

      it('calls process when user comes back from check-your-answers', () => {    
        req.sessionModel.get.returns([{'check-your-answers' : true},{'dependant-question' : false}]);       
        req.form.values['dependant-question'] = 'yes';  
         dependantAddController.process(req, res, next);  
       });

       it('calls process when user comes back from check-your-answers', () => {    
        req.sessionModel.get.returns([{'check-your-answers' : false},{'dependant-question' : true}]);       
        req.form.values['dependant-question'] = 'yes';  
         dependantAddController.process(req, res, next);  
       });

       it('calls process when user comes back from check-your-answers', () => {    
        req.sessionModel.get.withArgs('check-your-answers').returns(true);
        req.sessionModel.get.withArgs('dependant-question').returns('no');     
        req.form.values['dependant-question'] = 'no';  
         dependantAddController.process(req, res, next);  
       });       

      it('calls set dependant-id with undefined', () => {
        dependantAddController.ValidationError = sinon.stub();
        dependantAddController.ValidationError.returns(validationError);
        dependantAddController.process(req, res, next);
      });
    });
  });

});
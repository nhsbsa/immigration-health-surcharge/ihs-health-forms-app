const axios = require('axios').default;

module.exports = class SubmitClaimApiReferanceModel {
  async submitClaim(claim, next) {
    const headers = { "Content-type": "application/json", "x-api-key": process.env.IHS_API_KEY };
    try {
      const url = process.env.BSA_URL + 'claims/' + claim.bsaReimbursementReference;
      const referenceNumber = await axios.post(url, claim, { headers });
      console.log("Submitting claim for "+claim.bsaReimbursementReference);
      return referenceNumber.data.bsaReference;
    } catch (error) {
      console.log('Error in submitting the claim ' + error);
      next(new Error(error.body));
    }
  }
}
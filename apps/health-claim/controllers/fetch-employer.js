const Controller = require('hof-govfrontend-v3').controller;


module.exports = class DependantAddController extends Controller {

  successHandler(req, res, next) {
    const employerId = req.sessionModel.get('edit-employerId');
    if(employerId){
      if(req.sessionModel.get('check-employment')){
        req.sessionModel.unset('check-employment');
        const nextNav = '/check-employment';
        req.form.options['confirmStep'] = nextNav;
        return res.redirect(`${req.baseUrl}${nextNav}`);
      }else{
        if(req.sessionModel.get('check-your-answers')){
          const nextNav = '/check-employment';
          req.sessionModel.unset('check-employment');
          return res.redirect(`${req.baseUrl}${nextNav}`); 
        }           
      }
    }
    return super.successHandler(req, res, next);    
  }
  
};
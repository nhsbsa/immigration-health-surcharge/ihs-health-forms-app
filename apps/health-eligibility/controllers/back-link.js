const Controller = require('hof-govfrontend-v3').controller;

module.exports = class getbackLink extends Controller {  
  render(req, res, callback) {    
    const backlink =  ( req.sessionModel.attributes['applicant-visa-type'] === 'other') ? 'applicant-visa-type' : 'qualified-health-professional';
    const isedit =  (res.locals.action.includes("/edit"))? '/edit' : ''; 

    res.locals.backLink = "/health/eligibility/"+ backlink+isedit;
    res.locals.options.locals.backLink = "/health/eligibility/"+ backlink+isedit;    
    const locals = super.render(req, res, callback);
    return locals;
  }
};
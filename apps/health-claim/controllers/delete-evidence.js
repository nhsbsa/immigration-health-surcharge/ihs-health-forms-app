const Controller = require('hof-govfrontend-v3').controller;
const { ValidationError } = require('hof-govfrontend-v3/controller');
const FileUploadModel = require('../models/file-upload');
const fileUploadModel = new FileUploadModel();
module.exports = class EvidenceDeleteController extends Controller {
    async get(req, res, next) {
        try {
            
            if (req.sessionModel.get('health-claim-submit-flag'))
                throw new ValidationError('Invalid request.');

            const fileId = req.query.fileId;
            var fileList;
            if (!fileId)
                throw new ValidationError('Invalid file id.');

            let fileUploadRequestObj = await fileUploadModel.fileDeleteAPI(req.sessionModel.get('claim'), fileId);
            let uploadedFileList = req.sessionModel.get('uploadedFileEvidence');

            if (uploadedFileList.filter((fileItem) => { return fileItem['fileId'] === fileId }).length == 0 || fileUploadRequestObj == undefined
                || (fileUploadRequestObj.data.status != 200)) {
                let fileObj = uploadedFileList.filter((fileItem) => { return fileItem['fileId'] === fileId })
                req.sessionModel.set('evidenceDeleteErrorMessage', fileObj[0].fileName + ' was not deleted. Try again');
                req.sessionModel.set('uploadedFileEvidence', uploadedFileList);
                fileList = uploadedFileList;
            } else {
                req.sessionModel.set('evidenceDeletedInfo', uploadedFileList.filter((fileItem) => { return fileItem['fileId'] === fileId }));
                let filteredUploadedFileList = uploadedFileList.filter((fileItem) => { return fileItem['fileId'] != fileId });
                filteredUploadedFileList.map((fileItem, index) => { return fileItem['srNo'] = index + 1 });
                req.sessionModel.set('uploadedFileEvidence', filteredUploadedFileList);
                req.sessionModel.set('uploadedFileEvidenceCount', filteredUploadedFileList.length);
                fileList = filteredUploadedFileList;
            }


            if (fileList && fileList.length == 0) {
                return res.redirect(`${req.baseUrl}/upload-evidence`);
            } else {
                return res.redirect(`${req.baseUrl}/attachments`);
            }

        } catch (exception) {
            if (exception.customeException) {
                next();
            } else {
                next(exception);
            }
        }
    }
};
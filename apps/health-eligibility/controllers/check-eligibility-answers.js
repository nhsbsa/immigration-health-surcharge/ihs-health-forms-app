const Controller = require('hof-govfrontend-v3').controller;

module.exports = class ihsCheckYourAnswersController extends Controller {

    render(req, res, callback) {
        let changeVar = res.locals.rows[1]['fields'][0];
        changeVar['label'] = changeVar['value'] === 'Skilled Worker visa or Tier 2 (General) visa' ? 'Hold a Skilled Worker visa or Tier 2 (General) visa' :'Hold Other visa';
        changeVar['value'] = 'Yes';
        const locals = super.render(req, res, callback);
        return locals;
    }
}
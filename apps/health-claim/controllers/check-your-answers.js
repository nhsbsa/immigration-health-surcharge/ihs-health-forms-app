const Controller = require('hof-govfrontend-v3').controller;
const { dependantSections, personalDetailsSection } = require('./../sections/personalDetails');
const PersonalDetailsModel = require('../models/personal-details');
const employerSections = require('../sections/employer');
const moment = require('moment');
const SubmitClaimModel = require('./../models/submit-claim');
const ClaimUtility = require('./../utility/claim-utility');
const { ValidationError } = require('hof-govfrontend-v3/controller');

const radioinputs = [
  'date-of-birth',
  'applicant-file-upload',
  'dependant-date-of-birth'
];
const inputques = [
  'immigration-health-surcharge-number',
  'national-insurance-number'
];

module.exports = class SummaryDetailsController extends Controller {
  process(req, res, next) {   
    try{
    let newUploadList = req.sessionModel.get('uploadedFileEvidence') ? req.sessionModel.get('uploadedFileEvidence') : [];
    if(newUploadList.length && req.sessionModel.get('all-employers').length > 0 &&
    req.sessionModel.get('all-employers')[0]['job-title'].length > 0 ){
        next();
      }else{     
        if(req.sessionModel.get('all-employers').length == 0 )
          req.sessionModel.set('no-employer', true);

        if(req.sessionModel.get('all-employers')[0]  && req.sessionModel.get('all-employers')[0]['job-title'].length == 0 )
          req.sessionModel.set('no-jobtitle', true);
        
        if(newUploadList.length == 0)
          req.sessionModel.set('no-files', true);
          next({
            'check-your-answers': new this.ValidationError('check-your-answers', {
                type: 'no-files'
            }, req, res)
        });
      }
    }catch (exception) {
      next(exception);
    }    
  }


  getValues(req, res, callback) {
    req.sessionModel.set('check-your-answers', true);
    req.sessionModel.unset('edit-dependantId');
    const field = this.options.locals.field;
    super.getValues(req, res, callback);
    const sessionModel = req.sessionModel.toJSON();
    this.savePersonalDetails(req, sessionModel);
  }

  locals(req, res, callback) {
    const locals = super.locals(req, res, callback);
    const allDependants = req.sessionModel.get('all-dependants');
    const NoFilesError = req.sessionModel.get('no-files') ? true : false;
    const NoEmployerError = req.sessionModel.get('no-employer') ? true : false;
    const NoJobTitleError = req.sessionModel.get('no-jobtitle') ? true : false;
    
    const personalDetails = req.sessionModel.get('personalDetails');
    const allEmployers = req.sessionModel.get('all-employers');
    const uploadFileEvidenceList = req.sessionModel.get('uploadedFileEvidence');
    const EvidanceListExtraInformation = req.sessionModel.get('uploadedFileExtraInfo');
    locals.employersDataList = this.genrateEmployerDataList(req, allEmployers);
    locals.dependantsDataList = allDependants ? this.genrateDependantDataList(req, allDependants) : [];
    locals.personalDetailsDataList = this.generatePersonalDataList(req, personalDetails);
    locals.commentList = req.sessionModel.get('extra-information');
    locals.uploadFileEvidenceList = uploadFileEvidenceList ? uploadFileEvidenceList : [];
    locals.EvidanceListExtraInformation = EvidanceListExtraInformation ? EvidanceListExtraInformation : [];
    locals.changeLinkAdditionalFiles = (EvidanceListExtraInformation && EvidanceListExtraInformation.length > 0) ? 'extra-evidence-uploaded/edit#' : 'extra-information/edit#' ;
    
    locals.backLink = (EvidanceListExtraInformation && EvidanceListExtraInformation.length > 0) ? 'extra-evidence-uploaded/edit#' : 'extra-information/edit#' ;
    if(NoFilesError){
      locals.NoFilesError = NoFilesError ;
      locals.TitleError = true ;
    }else{
      locals.FilesExists = true ;
    }

    if(NoEmployerError){
      locals.NoEmployerError = NoEmployerError ;
      locals.TitleError = true ;
    }

    if(NoJobTitleError){
      locals.NoJobTitleError = NoJobTitleError ;
      locals.TitleError = true ;
    }
    if((req.sessionModel.get('all-employers').length == 1 ) && 
       (req.sessionModel.get('all-employers')[0]['job-title'].length == 0 )  ){
      locals.NoJobTitle = true ;
    }

    if(req.sessionModel.get('all-employers').length == 0){
      locals.NoEmployer = true ;
    }

    if(req.sessionModel['attributes'].hasOwnProperty('errorValues') && req.sessionModel['attributes']['errorValues'].hasOwnProperty('extra-information')){
      delete(req.sessionModel['attributes']['errorValues']['extra-information']);
    }
    req.sessionModel.unset('no-files');
    req.sessionModel.unset('no-employer');
    req.sessionModel.unset('no-jobtitle');
    req.sessionModel.unset('TempExtraInfo');
    locals.claimStartDate = moment(req.sessionModel.get('claim-start-date'), 'YYYY-MM-DD').format('DD MMMM YYYY');
    locals.subscribeForReminder = req.sessionModel.get('subscribe-for-reminder')[0].toUpperCase() + req.sessionModel.get('subscribe-for-reminder').slice(1).toLowerCase();
    return locals;
  }

  savePersonalDetails(req, sessionModel) {
    const personalDetails = new PersonalDetailsModel(sessionModel);
    req.sessionModel.set('personalDetails', personalDetails);
  }

  generatePersonalDataList(req, personalDetails) {
    return personalDetailsSection.map(key => {
      const value = personalDetails[key];
      const url = (key == 'applicant-file-upload' || key == 'upload-dependant-european-health-insurance-card-ehic') ? false : key;
      return {
        label: inputques.includes(key) ? req.translate(`fields.${key}.last-label`): req.translate(`fields.${key}.label`),
        value: radioinputs.includes(key) ? this.getDisplayValue(req, key, value) : value,
        url: url,
        key: key
      };
    }).filter(item => item);
  }

  genrateDependantDataList(req, allDependants) {
    const dependantDataList = [];
    allDependants.forEach((dependant, index) => {
      const dependantData = { dependantIndex: index + 1, dependantId: dependant['dependant-id'] };

      dependantData.dependantFields = dependantSections.map(key => {
        const value = dependant[key];
        const url = (key == 'applicant-file-upload') ? false : key;
        return {
          label: req.translate(`fields.${key}.label`),
          value: radioinputs.includes(key) ? this.getDisplayValue(req, key, value) : value,
          url: url,
          key: key
        }
      })
      dependantDataList.push(dependantData);
    });
    return dependantDataList;
  }

  getEmployerDisplayValue(req, key, value, employerDetail = { 'other-job-setting': '' }) {
    let returnValue = (value !== 'other') ? req.translate(`fields.${key}.options.${value}.label`) : employerDetail['other-job-setting'];
    if (returnValue === 'fields.job-setting.options..label') {
      return '';
    } else {
      return returnValue;
    }
  }

  genrateEmployerDataList(req, allEmployers) {
    const employerDataList = [];
    allEmployers.forEach((employer, index) => {
      const employerData = { employerIndex: index + 1, employerId: employer['employer-id'] };
      const notDisplayChangeLinks = ["applicant-file-upload"];

      employerData.employerFields = employerSections.map(key => {
        const value = employer[key];
        const url = (key == 'employer-name') ? 'add-employer' : key;
        return {
          label: req.translate(`fields.${key}.label`),
          value: (key == 'job-setting' ? this.getEmployerDisplayValue(req, key, value, employer) : value) ||
          (key == 'job-title' && value.length == 0  ? '-' : value) 
          ,
          url: url,
          key: key
        }
      })
      employerDataList.push(employerData)
    });
    return employerDataList;
  }

  getDisplayValue(req, key, value) {
    switch (key) {
      case 'dependant-date-of-birth':
        return moment(value, 'YYYY-MM-DD').format('DD MMMM YYYY');
      case 'date-of-birth':
        return moment(value, 'YYYY-MM-DD').format('DD MMMM YYYY');
      case 'applicant-file-upload':
        return value.map(fileName => `${fileName}`).join('<br>');
      case 'upload-dependant-european-health-insurance-card-ehic':
        return `${value}`;
      default:
        return req.translate(`fields.${key}.options.${value}.label`);
    }
  }
};
'use strict';
const path = require('path');
const date = require('hof-govfrontend-v3').components.date;
const hints = require('../translations/src/en/hints.json');
const nameValidator = [
  'required',
  { type: 'regex', arguments: /[a-zA-Z '-]{0,50}[a-zA-Z][a-zA-Z '-]{0,50}$/ },
  { type: 'minlength', arguments: 1 },
  { type: 'maxlength', arguments: 50 }
]

const IHSValidator = [
  'required',
  { type: 'minlength', arguments: 12 },
  { type: 'regex', arguments: /^[Ii][Hh][Ss][Cc]{0,1}[0-9]{9,9}$/},
  { type: 'maxlength', arguments: 13 }
]

module.exports = {
  'given-name': {
    mixin: 'input-text',
    validate: nameValidator,
    className: ['govuk-input']
  },
  'family-name': {
    mixin: 'input-text',
    validate: nameValidator,
    className: ['govuk-input']
  },
  'date-of-birth': date('date-of-birth', {
    controlType: 'date-input',
    validate: [
      'required',
    ],
    formGroup: {
      className: 'govuk-date-input__item'
    },
    controlsClass: {
      day: 'govuk-input govuk-date-input__input govuk-input--width-2',
      month: 'govuk-input govuk-date-input__input govuk-input--width-2',
      year: 'govuk-input govuk-date-input__input govuk-input--width-4'
    }
  }),
  'national-insurance-number': {
    mixin: 'input-text',
    validate: [
      'required',
      { type: 'regex', arguments: /^(?!BG|GB|NK|KN|TN|NT|ZZ|bg|gb|nk|kn|tn|nt|zz){1,2}\s?((?![DFIQUVdfiquv])([A-Za-z])(?![DFIQUVOdfiquv])([A-Za-z])){1,2}\s?[0-9]{1,2}\s?[0-9]{1,2}\s?[0-9]{1,2}\s?[a-dA-D ]$/ },
      { type: 'maxlength', arguments: 13 }
    ],
    className: ['govuk-input', 'govuk-input--width-10']
  },
  'immigration-health-surcharge-number': {
    mixin: 'input-text',
    validate: IHSValidator,
    className: ['govuk-input', 'govuk-input--width-10'],
    labelClassName: ['govuk-visually-hidden']
  },
  'email': {
    mixin: 'input-text',
    validate: [
      'required',
      'email',
      { type: 'maxlength', arguments: 50 }
    ],
    className: ['govuk-input']
  },
  'phone-number': {
    mixin:'input-phone',
    validate: [
      { type: 'regex', arguments: /^[0-9 ()]*$/ },
      { type: 'maxlength', arguments: 15 },
      { type: 'minlength', arguments: 11 }
    ],
    className: ['govuk-input', 'govuk-input--width-20']
  },
  'dependant-question': {
    mixin: 'radio-group',
    legend: {
      className: 'visuallyhidden'
    },
    includeInSummary: false,
    options: ['yes', 'no'],
    validate: 'required'
  },
  'add-more-dependants': {
    mixin: 'radio-group',
    includeInSummary: false,
    legend: {
      className: 'govuk-visually-hidden'
    },
    options: ['yes', 'no'],
    validate: 'required',
  },
  'claim-start-date': date('claim-start-date', {
    controlType: 'date-input',
    validate: [
      'required',
    ],
    formGroup: {
      className: 'govuk-date-input__item'
    },
    controlsClass: {
      day: 'govuk-input govuk-date-input__input govuk-input--width-2',
      month: 'govuk-input govuk-date-input__input govuk-input--width-2',
      year: 'govuk-input govuk-date-input__input govuk-input--width-4'
    }
  }),
  'subscribe-for-reminder': {
    mixin: 'radio-group',
    legend: {
      className: 'govuk-visually-hidden'
    },
    includeInSummary: false,
    options: ['yes', 'no'],
    validate: 'required',
  },
  'dependant-given-name': {
    mixin: 'input-text',
    validate: nameValidator,
    className: ['govuk-input']
  },
  'dependant-family-name': {
    mixin: 'input-text',
    validate: nameValidator,
    className: ['govuk-input'],
    legend: {
      className: 'govuk-label'
    }
  },
  'dependant-date-of-birth': date('dependant-date-of-birth', {
    controlType: 'date-input',
    disableRender: true,
    validate: [
      'required',
      'date',
      'before'
    ],
    formGroup: {
      className: 'govuk-date-input__item'
    },
    controlsClass: {
      day: 'govuk-input govuk-date-input__input govuk-input--width-2',
      month: 'govuk-input govuk-date-input__input govuk-input--width-2',
      year: 'govuk-input govuk-date-input__input govuk-input--width-4'
    }
  }),
  'dependant-immigration-health-surcharge-number': {
    mixin: 'input-text',
    disableRender: true,
    validate: IHSValidator,
    hintHTML: hints['dependant-immigration-health-surcharge-number'].hint,
    labelClassName: ['govuk-visually-hidden'],
    className: ['govuk-input', 'govuk-input--width-10']
  },
  'employer-name': {
    mixin: 'input-text',
    autocomplete: 'off',
    legend: {
      className: 'govuk-visually-hidden'
    },
    validate: [
      { type: 'maxlength', arguments: 255 },
      { type: 'minlength', arguments: 3 },
      { type: 'regex', arguments: /^[a-zA-Z0-9'_/&:.\[\],()@-\s]+$/i },
    ],
    className: ['govuk-input']
  },
  'primary-role-id': {
    mixin: 'input-text',
    type:'hidden',
    className: ['govuk-visually-hidden'],
    legend: {
      className: 'govuk-visually-hidden'
    },
    labelClassName: ['govuk-visually-hidden']
  },
  'job-title': {
    mixin: 'input-text',
    autocomplete: 'off',
    legend: {
      className: 'govuk-visually-hidden'
    },
    validate: [
      'required',
      { type: 'maxlength', arguments: 70 },
      { type: 'minlength', arguments: 3 },
      { type: 'regex', arguments: /^[a-zA-Z0-9'/.,-\s]+$/ },
    ],
    className: ['govuk-input']
  },
  'job-setting': {
    mixin: 'radio-group',
    legend: {
      className: ['govuk-visually-hidden']
    },
    hintHTML: hints['job-setting'].hint,
    options: ['hospital', 'gppractice', 'carehome', 'community',

      {
        value: 'other',
        toggle: 'other-job-setting',
        child: 'input-text'
      }
    ],
    validate: [
      'required',
      { type: 'minlength', arguments: 3 },
      { type: 'maxlength', arguments: 50 }
    ]
  },
  'other-job-setting': {
    mixin: 'input-text',
    validate: [
      'required',
      { type: 'minlength', arguments: 3 },
      { type: 'maxlength', arguments: 50 },
      { type: 'regex', arguments: /^[a-zA-Z\s]+$/ },
    ],
    className: ['govuk-input'],
    dependent: {
      field: 'job-setting',
      value: 'other'
    }
  },
  'add-more-employer': {
    mixin: 'radio-group',
    includeInSummary: false,
    legend: {
      className: 'govuk-visually-hidden'
    },
    options: ['yes', 'no'],
    validate: 'required',
  },
  'upload-evidence': {
    mixin: 'input-file',
    className: ['govuk-file-upload'],
    labelClassName: ['govuk-visually-hidden'],
  },  
  'extra-info-evidence': {
    mixin: 'input-file',
    className: ['govuk-file-upload'],
    labelClassName: ['govuk-visually-hidden'],
  },
  'uploaded-files-evidence': {
    mixin: 'input-file',
    className: ['govuk-file-upload'],
    labelClassName: ['govuk-visually-hidden']
  },
  'add-more-files-evidence': {
    mixin: 'radio-group',
    includeInSummary: false,
    legend: {
      className: 'govuk-visually-hidden'
    },
    options: ['yes', 'no'],
    validate: 'required',
  },
  'add-more-files-extra-info': {
    mixin: 'radio-group',
    includeInSummary: false,
    legend: {
      className: 'govuk-visually-hidden'
    },
    options: ['yes', 'no'],
    validate: 'required',
  },
  'extra-information': {
    mixin: 'textarea',
    className: ['govuk-textarea'],
    validate: [
      { type: 'maxlength', arguments: 2000 },
      { type: 'regex', arguments: /^[a-zA-Z0-9\n\s,£.\-']*$/ }
    ],
    labelClassName: ['govuk-visually-hidden'],    
    'ignore-defaults': true,
    formatter: ['trim', 'hyphens'],
    attributes: [{
      attribute: 'rows',
      value: 5
    }],
    includeInSummary: true
  }
};
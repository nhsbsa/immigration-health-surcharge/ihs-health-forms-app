const Controller = require('hof-govfrontend-v3').controller;
var fs = require('fs');
const { resolve } = require('path');
const EmployerModel = require('../models/employer');
const { push } = require('../sections/employer');

module.exports = class AddEmployerController extends Controller {

  async process(req, res, next) {
    try {
      let validationCheckStaus = this.checkValidation(req, res, next);
      if (validationCheckStaus != 'pass') {
        return next({
          'job-title': new this.ValidationError('job-title', {
            type: validationCheckStaus
          }, req, res)
        });
      }

      let checkResult = await this.checkEmployer(req)
      this.modifyEmployerDetails(req, req.sessionModel.get('edit-employerId') ? req.sessionModel.get('edit-employerId') : req.sessionModel.get('employer-id'), checkResult);
      req.sessionModel.get('check-employment') ? req.sessionModel.unset('check-employment') : null;
      if (checkResult == 'next') {
        let index = req.session['hof-wizard-health-claim'].steps.indexOf('/job-setting');
        if (index > -1) {
          req.session['hof-wizard-health-claim'].steps.splice(index, 1); // 2nd parameter means remove one item only
        }
        return next();
      } else {
        req.session['hof-wizard-health-claim'].steps.includes('/job-setting') ?
          '' : req.session['hof-wizard-health-claim'].steps.push('/job-setting');
        return res.redirect(checkResult);
      }
    } catch (error) {
      console.error('Error occured while fetching job title, Please try again later!!', error);
    }
  }

  async checkEmployer(req) {
    return new Promise(async (resolve, reject) => {
      try {
        let roleName = req.form.values['job-title'].toLowerCase().replace(/ /g, "_");
        let proleids = await fs.promises.readFile(`${__dirname}/jobroles.json`)
        const jobroles = JSON.parse(proleids);
        if (jobroles.hasOwnProperty(roleName) && jobroles[roleName] == 'FALSE') {
          return resolve(`${req.baseUrl}/check-employment`);
        } else {
          return resolve(`next`);
        }
      } catch (error) {
        reject(error);
      }
    });
  }

  modifyEmployerDetails(req, employerId, resultCheck = false) {
    let allEmployers = req.sessionModel.get('all-employers');
    const employerIndex = allEmployers.findIndex(employer => employer['employer-id'] === employerId);
    if (resultCheck !== 'next')
      allEmployers[employerIndex]['job-setting'] = '';

    allEmployers[employerIndex]['job-title'] = req.form.values['job-title'];
    const empmodel = new EmployerModel(allEmployers[employerIndex]);
    allEmployers[employerIndex] = empmodel.editEmployer(empmodel);
    req.sessionModel.set('all-employers', allEmployers);
    return allEmployers;
  }

  successHandler(req, res, next) {
    return super.successHandler(req, res, next);
  }

  checkValidation(req, res, next) {
    const empTitle = req.form.values['job-title'];
    if (empTitle.length == 0)
      return 'required';

    const nonWhiteSpace = /(.*[a-zA-Z0-9]){3}/gi;
    if (!nonWhiteSpace.test(empTitle))
      return 'minlength';

    const otherSpecial = /[*+]/gi;
    if (otherSpecial.test(empTitle))
      return 'regex';

    return 'pass';
  }
};
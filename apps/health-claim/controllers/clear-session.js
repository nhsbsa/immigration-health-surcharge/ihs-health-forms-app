'use strict';
const Controller = require('hof-govfrontend-v3').controller;
module.exports = class clearDataSession extends Controller {
  getValues(req, res) {
    const sessionCleared = req.query.sessionCleared;
    let nextNav = '';
    nextNav = '/start?sessionCleared=true';

    if (!sessionCleared) {
      req.session.destroy()
      return res.redirect(`${req.baseUrl}${nextNav}`);
    } else {
      const nextNav = (req.baseUrl === '/health-eligibility') ? '/paid-immigration-health-surcharge' : '/name';
      return res.redirect(`${req.baseUrl}${nextNav}`);
    }
  }
};
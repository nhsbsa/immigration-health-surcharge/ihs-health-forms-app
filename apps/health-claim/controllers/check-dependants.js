const Controller = require('hof-govfrontend-v3').controller;
const DependantModel = require('./../models/dependant');
const dependantSections = require('./../sections/dependant')
const moment = require('moment');
const radioinputs = ['dependant-date-of-birth'];
const { moreDependantClearSession, maxDependantCount } = require('./../constants/dependant');


module.exports = class checkDependants extends Controller {  

  getValues(req, res, callback) {    
    super.getValues(req, res, callback);
    const sessionModel = req.sessionModel.toJSON();
    if(!sessionModel.errors && sessionModel['dependant-id']){        
      this.saveDependant(req, sessionModel);
    }
  }
  
  saveDependant(req, sessionModel) {
    const currentDependantId = sessionModel['dependant-id'];
   
    if(!currentDependantId) return;
    let allDependants = sessionModel['all-dependants'] ? sessionModel['all-dependants'] : [];
    const dependantIndex = allDependants.findIndex(dependant => dependant['dependant-id'] === currentDependantId)
    if(dependantIndex === -1) {
      allDependants.push(new DependantModel(sessionModel));
      req.form.values['add-more-dependants']='';  
      req.sessionModel.unset('formValue-add-more-dependants');
      req.sessionModel.unset('dependant-id');      
      req.sessionModel.set('all-dependants', allDependants);
    }else{ // For edit depedant  
      allDependants = this.modifyDepedantDetails(req);
    }
    req.sessionModel.set('all-dependants', allDependants);
  }

  modifyDepedantDetails(req) {
    const dependantId = req.sessionModel.get('edit-dependantId');
    if(dependantId){
      let allDependants = req.sessionModel.get('all-dependants');
      const dependantIndex = allDependants.findIndex(dependant => dependant['dependant-id'] === dependantId);  
      const keys =  Object.keys(allDependants[dependantIndex]);
      keys.forEach((formfield, index) => { 
        allDependants[dependantIndex][formfield] =req.form.values[formfield] ;          
      });   
      const depmodel =  new DependantModel(allDependants[dependantIndex]);
      allDependants[dependantIndex] = depmodel.editDependant(depmodel);
      req.sessionModel.unset('edit-dependantId');
      req.sessionModel.unset('dependant-id');
      return allDependants;
    }   
  }


  locals(req, res, callback) {
    const locals = super.locals(req, res, callback);
    const deletedDependant = req.sessionModel.get('deletedDependant');
    locals.deletedDependant = deletedDependant ? deletedDependant : false;
    if(locals.deletedDependant){
      req.form.values['add-more-dependants']='';  
      req.sessionModel.unset('formValue-add-more-dependants');
    }else{
      req.form.values['add-more-dependants']=req.sessionModel.get('formValue-add-more-dependants');  
    }
    req.sessionModel.unset('deletedDependant');
    const allDependants = req.sessionModel.get('all-dependants');
    if(allDependants && allDependants.length > 0){
      locals.dependantsDataList = this.genrateDependantDataList(req, allDependants);   
    }    
    return locals;
  }

  genrateDependantDataList(req, allDependants) {
    const dependantDataList = [];
    allDependants.forEach((dependant, index) => {
      const dependantData = { dependantIndex: index + 1, dependantId: dependant['dependant-id']};
      const notDisplayChangeLinks = ["applicant-file-upload"];

      dependantData.dependantFields = dependantSections.map(key => {
        const value = dependant[key];
        const url = (notDisplayChangeLinks.includes(key))? false: key ;
        return {
          label: req.translate(`fields.${key}.label`),
          value: radioinputs.includes(key) ? this.getDisplayValue(req, key, value): value,
          url: url,
          key:key
        }
      })
      dependantDataList.push(dependantData)
    });
    return dependantDataList;
  }

  getDisplayValue(req, key, value) { 
    if(key === 'dependant-date-of-birth'){
      return moment(value, 'YYYY-MM-DD').format('DD MMMM YYYY');
    }
    return req.translate(`fields.${key}.options.${value}.label`);
  }

  process(req, res, next) {
    const allDependants = req.sessionModel.get('all-dependants');
    const currentStep = 'add-more-dependants';
    const dependantLength = allDependants ? allDependants.length : 0;
    const moreDependants = req.form.values[currentStep];
    req.sessionModel.set('formValue-add-more-dependants', req.form.values[currentStep]);
    if( !moreDependants){
      const err = new this.ValidationError(currentStep, {
        type: 'required'
      }, req, res);
      const errorObject = {};
      errorObject[currentStep] = err;
      return next(errorObject);
    }
    const uniqueId = moreDependants === 'yes' ? ('id' + (new Date()).getTime()) : void 0;
    if (dependantLength >= maxDependantCount && moreDependants === 'yes') {
      const err = new this.ValidationError(currentStep, {
        type: 'maxDependants'
      }, req, res);
      const errorObject = {};
      errorObject[currentStep] = err;
      return next(errorObject);
    }
    if(moreDependants === 'yes') {
      req.sessionModel.unset(moreDependantClearSession);
      req.sessionModel.set('dependant-id', uniqueId);
      const nextNav = '/dependant-details';
      return res.redirect(`${req.baseUrl}${nextNav}`);       
    }else{
      req.sessionModel.unset('edit-dependantId');
      req.sessionModel.unset('dependant-id');
      if(req.sessionModel.get('check-your-answers')){
        const nextNav = '/check-your-answers';
        req.sessionModel.unset('check-your-answers');      
        return res.redirect(`${req.baseUrl}${nextNav}`); 
      }else{        
       const nextNav = '/claim-start-date';
        return res.redirect(`${req.baseUrl}${nextNav}`);
      }
    }    
    next();
  }
 

};

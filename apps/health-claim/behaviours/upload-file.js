'use strict';
const path = require('path');
const ClaimUtility = require('../utility/claim-utility');
const FileUploadModel = require('../models/file-upload');
const fileUploadModel = new FileUploadModel();
const moment = require('moment');

module.exports = superclass => class UploadFile extends superclass {
  async process(req, res, next) {
    try {
      const locals = super.locals(req, res, next);
      const currentStep = locals.step;
      if (req.sessionModel.get('health-claim-submit-flag'))
        throw { status: false, validationType: '400', currentStep: currentStep };

      const validationObj = this.checkValidation(req, res, next, currentStep);
      if (!validationObj.status)
        throw validationObj

      req.files[currentStep].name = req.files[currentStep].name.replace(/ /g, "_");

      let fileUploadObj = await this.fileupload(req, currentStep, next);

      if ((!fileUploadObj) || (!fileUploadObj.data.status) || fileUploadObj.data.status != '200') {
        console.log('Error occured while uploading File, Please try again later! ');
       if(fileUploadObj.data.status =="429"){
          req.sessionModel.set('maxFileUpload', true);
          return res.redirect(`${req.baseUrl}/attachments`);
        }else{
          throw { status: false, validationType: 'api-error', message: `${req.files[currentStep].name} was not uploaded. Try again`, currentStep: currentStep };
        }
        
      } else {
        this.incrementEvidenceCount(req, locals, currentStep, fileUploadObj['data']['fileid']);
        if (req.url.indexOf('/edit') != -1) {
          if (req.url.indexOf('/upload-evidence/edit') != -1) {
            return res.redirect(`${req.baseUrl}/attachments/edit`);
          } else {
            return res.redirect(`${req.baseUrl}${req.form.options.next}/edit`);
          }
        } else {
          next();
        }
      }
    } catch (error) {
      if (error.validationType == 'required' || error.validationType == 'error') {
        return next({
          [error.currentStep]: new this.ValidationError(error.currentStep, {
            type: (error.validationType == 'error' || typeof error.validationType === 'undefined') ? 'error' : error.validationType
          }, req, res)
        });
      } else if (error.validationType == '400') {
        return next(new this.ValidationError('Invalid request.'));
      } else {
        req.sessionModel.set('evidenceUploadErrorMsg', error.message);
        return res.redirect(`${req.baseUrl}/upload-evidence`);
      }
    }
  }

  incrementEvidenceCount(req, locals, currentStep, fileId) {
    const uploadedFileEvidenceCount = req.sessionModel.get('uploadedFileEvidenceCount');
    req.sessionModel.set('uploadedFileEvidenceCount', uploadedFileEvidenceCount ? uploadedFileEvidenceCount + 1 : 1);
    let uploadedFileList = []

    if (req.sessionModel.get(locals.fileNameKey) == undefined) {
      req.sessionModel.set(locals.fileNameKey, [{ 'srNo': 1, 'fileName': req.files[currentStep].name, 'fileId': fileId }]);
    } else {
      uploadedFileList = req.sessionModel.get(locals.fileNameKey);
      uploadedFileList.push({ 'srNo': req.sessionModel.get('uploadedFileEvidenceCount'), 'fileName': req.files[currentStep].name, 'fileId': fileId });
      req.sessionModel.set(locals.fileNameKey, uploadedFileList);
    }
    req.sessionModel.set(currentStep, req.files[currentStep].name);
  }

  checkFileCount(req, res, next, currentStep) {
    if (req.url === '/upload-evidence' && req.sessionModel.get('uploadedFileEvidenceCount') >= process.env.MAX_FILE_COUNT) {
      req.sessionModel.set('maxFileUpload', true);
      return res.redirect(`${req.baseUrl}${req.form.options.next}`);
    }
    return true;
  }

  checkValidation(req, res, next, currentStep) {
    const file = req.files[currentStep];
    if (file == null) {
      return { status: false, validationType: 'required', currentStep: currentStep, message: `Select a file to upload as evidence` };
    }

    const pattern = /^[a-z0-9\-_\s]+$/i;
    const extension = path.extname(file.name);
    let filename = path.basename(file.name, extension);

    const fileCountFlag = this.checkFileCount(req, res, next, currentStep);
    if (!fileCountFlag) {
      return { status: false, validationType: 'error', currentStep: currentStep, message: `You cannot upload more than 75 files. To upload another file, you'll need to delete an existing file.` };
    }

    if (filename.length > Number(process.env.MAX_FILENAME_LENGTH )) {
      return { status: false, validationType: 'filelength', currentStep: currentStep, message: `${filename}${extension} must be 130 characters or less` };
    }
    
    if (!pattern.test(filename)) {
      return { status: false, validationType: 'regex', currentStep: currentStep, message: `${filename}${extension} must use only letters, numbers, spaces, hyphens and underscore` };
    }

    if (!this.fetchExt(file.name)) {
      return { status: false, validationType: 'extname', currentStep: currentStep, message: `${filename}${extension} must be a jpg, bmp, png or pdf` };
    }

    if (file && file.truncated) {
      return { status: false, validationType: 'filesize', currentStep: currentStep, message: `${filename}${extension} must be less than 2MB in size` };
    } else if (file.size > process.env.FILE_SIZE_LIMIT) {
      return { status: false, validationType: 'filesize', currentStep: currentStep, message: `${filename}${extension} must be less than 2MB in size` };
    }

    return { status: true, validationType: 'filesize', currentStep: currentStep };
  }

  render(req, res, next) {
    const currentStep = res.locals.step;
    if (!Object.keys(req.form.errors).length) {
      req.form.values[currentStep] = null;
    }
    
    if(req.sessionModel['attributes'].hasOwnProperty('errorValues') && req.sessionModel['attributes']['errorValues'].hasOwnProperty('extra-information')){
      delete(req.sessionModel['attributes']['errorValues']['extra-information']);
    }

    if(req.sessionModel['attributes']['errors'] !== null){
      delete(req.sessionModel['attributes']['errors']['extra-information']);
    }

    let evidenceDeletedInfo = req.sessionModel.get('evidenceDeletedInfo') ? req.sessionModel.get('evidenceDeletedInfo') : false;
    res.locals.evidenceDeletedInfo = evidenceDeletedInfo ? `File named ${evidenceDeletedInfo[0]['fileName']} has been deleted` : false;
    if (req.query.deleteAllWantedFile == 'true' && evidenceDeletedInfo == false) {
      res.locals.evidenceDeletedInfo = `All unsuccessful files have been deleted`;
    }
    req.sessionModel.unset('evidenceDeletedInfo');
    res.locals.evidenceUPloadStartDate = moment(res.locals.values['claim-start-date'], "YYYY-MM-DD").format('DD MMMM YYYY');
    res.locals.evidenceUPloadEndDate = moment(req.sessionModel.get('claim-calculated-end-date'), "YYYY-MM-DD").format('DD MMMM YYYY');
    res.locals.errorLogMsg = req.sessionModel.get('evidenceUploadErrorMsg') ? req.sessionModel.get('evidenceUploadErrorMsg') : false;
    res.locals.sessionID = req.sessionID;
    res.locals.maxFileCountValue = process.env.MAX_FILE_COUNT;
    res.locals.MULTI_FILE_URL = process.env.MULTI_FILE_URL;    
    res.locals.MAX_FILENAME_LENGTH = process.env.MAX_FILENAME_LENGTH;    
    res.locals.FILE_SIZE_LIMIT = process.env.FILE_SIZE_LIMIT;
    req.sessionModel.unset('evidenceUploadErrorMsg');
    fileUploadModel.fileListAPI(req.sessionModel.get('claim'), req.sessionID).then(response => {
      let uploadFileList = (response['data']['filesCount'] == 0) ? [] : response['data']['files'];
      let newUploadList = [];
      uploadFileList.forEach((currentFile, index) => {
       if(!currentFile['filename'].includes("extra-info-")){       
        newUploadList.push({
          'srNo': index + 1,
          'fileName': currentFile['filename'],
          'fileId': currentFile['fileId']
        });
      }
      });
      res.locals.uploadFileList = newUploadList;
      res.locals.showUploadFileList = (newUploadList.length == 0) ? false : true;

    }).catch(error => {
      console.error('File Listing:-', error.toString());
      res.locals.fileListingErrorMsg = 'The page has not loaded correctly. You should try to refresh';
    }).finally(() => {
      return super.render(req, res, next);
    });
  }

  async fileupload(req, currentStep, next) {
    const claim = req.sessionModel.get('claim');
    let file = ClaimUtility.fileData(req.files[currentStep]);
    return fileUploadModel.fileuploadAPI(claim, file, next);
  }

  fetchExt(value) {
    return value && [
      '.jpeg',
      '.jpg',
      '.bmp',
      '.png',
      '.pdf'
    ].includes(path.extname(value).toLowerCase());
  }
};

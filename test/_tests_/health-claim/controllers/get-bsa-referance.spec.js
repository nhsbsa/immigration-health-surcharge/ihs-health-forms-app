const BsaRefModel = require('../../../../apps/health-claim/models/bsa-reference');
const ClaimUtility = require('../../../../apps/health-claim/utility/claim-utility');
const mockData = require('./../../../_mocks_/health-claim/controllers/submit-claim');

const BsaReferenceController = require('../../../../apps/health-claim/controllers/get-bsa-referance');
const HOFController = require('hof-govfrontend-v3').controller;
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash');

describe('BsaReferenceController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('BsaReferenceController instance', () => {
    let bsaReferenceController;
    beforeEach(() => {
      bsaReferenceController = new BsaReferenceController({});
    });

    it('extends the hof-form-controller', () => {
      bsaReferenceController.should.be.instanceof(HOFController);
    });

    it('has getValues, locals methods', () => {
      bsaReferenceController.should.have.property('process').that.is.a('function');
      bsaReferenceController.should.have.property('createNewDraftClaim').that.is.a('function');
    });

    describe('process', () => {      
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'process').returns({
          fields: []
        });
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();    
        req.sessionModel.toJSON = sinon.stub();
        req.sessionModel.toJSON.returns(mockData.sessionModel); 
        
      });
      afterEach(() => {
        HOFController.prototype.process.restore();
      });
      
      it('calls process with arguments', () => {
        bsaReferenceController.process(req, res, next);
        const claim = ClaimUtility.updateClaimFromSession(mockData.sessionModel);
        const bsaRefModel = new BsaRefModel();
        const response = bsaRefModel.getBSARef(claim,next);
      });

     });

    describe('createNewDraftClaim', () => {
      beforeEach(() => {
        
        res.locals.values  = sinon.stub();
        req.sessionModel.get = sinon.stub();
        req.sessionModel.get.withArgs('claim-reference-number').returns('BSA123123123');
        req.sessionModel.get.withArgs('claim-calculated-end-date').returns('2000-09-09');
        res.locals.values.withArgs('claim-start-date').returns('2000-09-09');
        
      });

      it('calls createNewDraftClaim', () => {
        bsaReferenceController.createNewDraftClaim(req, res, next);
      });

    });
  });

});
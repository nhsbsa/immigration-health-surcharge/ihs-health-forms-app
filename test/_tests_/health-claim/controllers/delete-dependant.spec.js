const DependantDeleteController = require('../../../../apps/health-claim/controllers/delete-dependant');
const HOFController = require('hof-govfrontend-v3').controller;
const mockData = require('./../../../_mocks_/health-claim/controllers/employer-details');

const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('DependantDeleteController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('DependantDeleteController instance', () => {
    let dependantDeleteController;
    const dependantId = 'abc123';
    beforeEach(() => {
      // an instance of the DependantDeleteController
      dependantDeleteController = new DependantDeleteController({});
    });

    it('extends the hof-form-controller', () => {
      dependantDeleteController.should.be.instanceof(HOFController);
    });

    it('has get, deleteDependant methods', () => {
      dependantDeleteController.should.have.property('get').that.is.a('function');
      dependantDeleteController.should.have.property('deleteDependant').that.is.a('function');
    });

    describe('get', () => {
      beforeEach(() => {
        dependantDeleteController.deleteDependant = sinon.stub();
      });

      it('calls next() skiping condition if there is no dependantId', () => {
        req.query = {};
        dependantDeleteController.get(req, res, next);
        next.should.have.been.called;
      });

      it('calls deleteDependant when dependantId is available', () => {
        req.query = { dependantId };
        dependantDeleteController.get(req, res, next);
        dependantDeleteController.deleteDependant.should.have.been.calledWithExactly(req, res, dependantId);
      });
    });

    describe('deleteDependant', () => {
      beforeEach(() => {
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();
        req.sessionModel.unset = sinon.stub();
        req.session['hof-wizard-health-claim'] = sinon.stub();
        res.redirect = sinon.stub();
        req.baseUrl = '/health-claim';
      });

     it('skips deleteDependant if dependant with Id not found', () => {
        req.sessionModel.get.returns([]);
        dependantDeleteController.get(req, res, next);
      });

      it('Calls redirect to add dependant page, if all dependats are deleted', () => {
        req.sessionModel.get.returns([{ 'dependant-id': dependantId }]);
        dependantDeleteController.deleteDependant(req, res, dependantId);
        res.redirect.should.have.been.calledWithExactly('/health-claim/dependant-question');
      });

      it('Calls redirect to check dependant page, if all dependants are not deleted', () => {
        req.sessionModel.get.returns([{ 'dependant-id': dependantId }, { 'dependant-id': 'cde345' }]);
        dependantDeleteController.deleteDependant(req, res, dependantId);
        res.redirect.should.have.been.calledWithExactly('/health-claim/check-dependants');
      });
    });
  });

});
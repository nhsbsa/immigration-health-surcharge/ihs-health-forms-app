var printEmailAddress = function () {
  var emailInput = document.getElementById("email-address");
  var emailShow = document.getElementById("email-show");
  if (emailInput) {
    emailInput.onkeyup = function (event) {
      var newText = event.target.value;
      emailShow.innerText = newText;
    };
    if (emailInput.value) {
      emailShow.innerText = emailInput.value;
    }
  } else {
    var healthEmailInput = document.getElementById("email");
    if (healthEmailInput) {
      document.getElementById("validEmail").style.display = 'none';

      var healthEmailShow = document.getElementById("health-email-show");
      healthEmailInput.onkeyup = function (event) {
        healthEmailShow.innerText = event.target.value;
        if (event.target.value == '' || !event.target.value)
          document.getElementById("validEmail").style.display = 'none';
        else
          document.getElementById("validEmail").style.display = 'block';
      };
      if (healthEmailInput.value) {
        healthEmailShow.innerText = healthEmailInput.value;
        document.getElementById("validEmail").style.display = 'block';
      }
    }
  }
};
printEmailAddress();
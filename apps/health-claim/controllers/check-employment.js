const Controller = require('hof-govfrontend-v3').controller;
const EmployerModel = require('../models/employer');
const employerSections = require('../sections/employer');
const { moreEmployerClearSession, maxEmployerCount } = require('../constants/employer');
const radioinputs = ['dependant-date-of-birth'];
const FileUploadModel = require('../models/file-upload');
const fileUploadModel = new FileUploadModel();

module.exports = class checkEmployer extends Controller {

  getValues(req, res, callback) {
    req.sessionModel.unset('edit-employerId');   
    req.sessionModel.set('check-employment', true);
    super.getValues(req, res, callback);
    const sessionModel = req.sessionModel.toJSON();
    if (!sessionModel.errors && sessionModel['employer-id']) {
      this.saveEmployer(req, sessionModel);
    }
  }

    
  async render(req, res, next){
    try {
      let newUploadList =  await this.updateFileList(req.sessionModel.get('claim'));
      console.log(newUploadList);
      req.sessionModel.set('uploadedFileEvidence', newUploadList); 
    const locals = super.render(req, res, next);
      return locals;  
    } catch (error) {
      console.log(error);
      console.error('Error occured while Fetching File List !! ');
      const locals = super.render(req, res, next);
      return locals;
    }
  }

   locals(req, res, callback) {
    const locals = super.locals(req, res, callback);
    
    const TitleError = req.sessionModel.get('no-jobtitle') ? true : false;
    if(TitleError){
      locals.TitleError = TitleError ;
    }
    req.sessionModel.unset('no-jobtitle');
    
    const allEmployers = req.sessionModel.get('all-employers');
    locals.employersDataList = this.genrateEmployerDataList(req, allEmployers);
    
    const deletedEmployer = req.sessionModel.get('deletedEmployer');
    locals.deletedEmployer = deletedEmployer ? deletedEmployer : false;
    req.sessionModel.unset('deletedEmployer');
    return locals;
  }

  saveEmployer(req, sessionModel) {
    const currentEmployerId = sessionModel['employer-id'];
    if (!currentEmployerId)
      return false;
    let allEmployers = sessionModel['all-employers'] ? sessionModel['all-employers'] : [];
    const employerIndex = allEmployers.findIndex(employer => employer['employer-id'] === currentEmployerId)
    if (employerIndex === -1) {
      allEmployers.push(new EmployerModel(sessionModel))
    }
    req.sessionModel.set('all-employers', allEmployers);
  }

  getDisplayValue(req, key, value, employerDetail = { 'other-job-setting': '' }) {
     let returnValue =  (value !== 'other') ? req.translate(`fields.${key}.options.${value}.label`) : employerDetail['other-job-setting'];
    if(returnValue === 'fields.job-setting.options..label'){
      return '';
    }else{
      return returnValue;
    } 
  }

  genrateEmployerDataList(req, allEmployers) {
    const employerDataList = [];
    allEmployers.forEach((employer, index) => {
      const employerData = { employerIndex: index + 1, employerId: employer['employer-id'] };
      const notDisplayChangeLinks = ["applicant-file-upload"];
      employerData.employerFields = employerSections.map(key => {
        const value = employer[key];
        const url = (key == 'employer-name') ? 'add-employer' : key;
        return {
          label: req.translate(`fields.${key}.label`),
          value: key == 'job-setting' ? this.getDisplayValue(req, key, value, employer) : value ||
                    (key == 'job-title' && value.length == 0  ? '-' : value) ,
          url: url,
          key: key
        }
      })
      employerDataList.push(employerData)
    });
    return employerDataList;
  }

  async updateFileList(claimObj){
    try {
    let response = await fileUploadModel.fileListAPI(claimObj, claimObj.sessionId);
      let uploadFileList = (response['data']['filesCount'] == 0) ? [] : response['data']['files'];
      let newUploadList = [];
      uploadFileList.forEach((currentFile, index) => {
        if(!currentFile['filename'].includes("extra-info-")){    
          newUploadList.push({
              'srNo': index + 1,
              'fileName': currentFile['filename'],
              'fileId': currentFile['fileId']
          });
        }
      });
      return newUploadList;    
    }  catch (error) {
      console.log(error);
      console.error('Something went wrong while fetching file list');
    }
  }




 async process(req, res, next) {
    try{
        const allEmployers = req.sessionModel.get('all-employers');
        const employerLength = allEmployers ? allEmployers.length : 0;
        const moreEmployers = req.form.values['add-more-employer'];
        if (!moreEmployers) {
          const err = new this.ValidationError('add-more-employer', {
            type: 'required'
          }, req, res);
          const errorObject = {};
          errorObject['add-more-employer'] = err;
          return next(errorObject);
        }
         //check if job title is blank
         if(req.sessionModel.get('all-employers')[0]  && req.sessionModel.get('all-employers')[0]['job-title'].length == 0 ){
          req.sessionModel.set('no-jobtitle', true);
          const nextNav = '/check-employment';
          return res.redirect(`${req.baseUrl}${nextNav}`);
        }
        
        const uniqueId = moreEmployers === 'yes' ? ('id' + (new Date()).getTime()) : void 0;
        if (employerLength >= maxEmployerCount && moreEmployers === 'yes') {
          const err = new this.ValidationError('add-more-employer', {
            type: 'maxEmployer'
          }, req, res);
          const errorObject = {};
          errorObject['add-more-employer'] = err;
          return next(errorObject);
        }
        if (moreEmployers === 'yes') {
          req.sessionModel.unset(moreEmployerClearSession);
          req.sessionModel.unset('edit-employerId');
          req.sessionModel.unset('employer-id');
          const nextNav = '/add-employer';
          return res.redirect(`${req.baseUrl}${nextNav}`);
        } else {
          req.sessionModel.unset('edit-employerId');
          req.sessionModel.unset('employer-id'); 

          if(req.sessionModel.get('check-your-answers')){
            let nextNav ;
            let newUploadList =  await this.updateFileList(req.sessionModel.get('claim'));
            req.sessionModel.set('uploadedFileEvidence', newUploadList);  
            if(newUploadList.length){
              nextNav = '/check-your-answers';
            }else{
              let index = req.session['hof-wizard-health-claim'].steps.indexOf('/upload-evidence');
              if (index > -1) {
                req.session['hof-wizard-health-claim'].steps.splice(index, 1); // 2nd parameter means remove one item only
              }
              return next();
            }
            return res.redirect(`${req.baseUrl}${nextNav}`);
          }else{    
            return next();
          }
        }
      }catch (error) {
        console.error('Some issue occured while rendering check employment '+error);
        return next();
      }
    }
};

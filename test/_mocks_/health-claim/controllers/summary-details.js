const sessionModel = {
  'given-name': 'Personal',
  'family-name': 'Detail',
  'date-of-birth': '2004-09-12',
  'immigration-health-surcharge-number': 'IHS123456789',
  'applicant-share-code': '1AA AAA GGG',
  'telephone-number': '',
  'email-address': 'abc@abc.com',
  'enter-address': 'Mrs Smith 71<br>SOUTHAMPT Cherry Court<br>SOUTHAMPTON<br>UK<br>SO53 5PD<br>',
  'applicant-file-upload': ['dummy.pdf', 'dummy.pdf'],
  'address-line-1': 'Mrs Smith 71',
  'address-line-2': 'SOUTHAMPT Cherry Court',
  'address-town': 'SOUTHAMPTON',
  'address-county': 'UK',
  'address-postcode': 'SO53 5PD'
};

const allDependants = [
  {
    'dependant-name': 'Dependant Details',
    'dependant-given-name': 'Dependant',
    'dependant-family-name': 'Details',
    'add-dependants-with-european-health-insurance-card-ehic': 'yes',
    'dependant-paid-work-uk': 'no',
    'dependant-living-in-uk': 'yes',
    'dependant-date-of-birth': '1998-02-12',
    'dependant-immigration-health-surcharge-number': 'IHS123456789',
    'dependant-share-code': 'A12 345 67G',
    'dependant-id': 'id1641296218741',
    'dependant-same-address-as-applicant': 'no',
    'dependant-enter-address': 'Mrs Smith 71<br>SOUTHAMPT Cherry Court<br>SOUTHAMPTON<br>UK<br>SO53 5PD<br>',
    'upload-dependant-european-health-insurance-card-ehic': 'dummy.pdf',
    'dependant-address-line-1': 'Mrs Smith 71',
    'dependant-address-line-2': 'SOUTHAMPT Cherry Court',
    'dependant-address-town': 'SOUTHAMPTON',
    'dependant-address-county': 'UK',
    'dependant-address-postcode': 'SO53 5PD'
  }
];

const personalDetails = {
  'name': 'Personal Detail',
  'enter-address': 'Mrs Smith 71<br>SOUTHAMPT Cherry Court<br>SOUTHAMPTON<br>UK<br>SO53 5PD<br>',
  'applicant-file-upload': ['dummy.pdf', 'dummy.pdf'],
  ...sessionModel
}

const displayDependants = [
  {
    'dependantIndex': 1,
    'dependantId': 'id1641296218741',
    'dependantFields': [
      {
        "key": "dependant-name",
        'label': 'Translated Label',
        "url": "dependant-name",
        'value': 'Dependant Details'
      },
      {
        "key": "dependant-date-of-birth",
        'label': 'Translated Label',
        "url": "dependant-date-of-birth",
        'value': 'Tranformed Value'
      },
      {
        "key": "dependant-immigration-health-surcharge-number",
        'label': 'Translated Label',
        "url": "dependant-immigration-health-surcharge-number",
        'value': 'IHS123456789'
      },
      {
        "key": "dependant-share-code",
        'label': 'Translated Label',
        "url": "dependant-share-code",
        'value': 'A12 345 67G'
      },
      {
        "key": "dependant-enter-address",
        'label': 'Translated Label',
        "url": "dependant-enter-address",
        'value': 'Mrs Smith 71<br>SOUTHAMPT Cherry Court<br>SOUTHAMPTON<br>UK<br>SO53 5PD<br>'
      },
      {
        "key": "upload-dependant-european-health-insurance-card-ehic",
        'label': 'Translated Label',
        "url": "uploaded-dependant-files-european-health-insurance-card-ehic",
        'value': 'Tranformed Value'
      }
    ]
  }
];

const displayPersonalDetails = [
  { "key": "name","label": "Translated Label", "value": "Personal Detail" , "url": "name" },
  { "key": "date-of-birth","label": "Translated Label", "value": "Tranformed Value" , "url": "date-of-birth" },
  { "key": "immigration-health-surcharge-number","label": "Translated Label", "value": "IHS123456789" , "url": "immigration-health-surcharge-number"},
  { "key": "applicant-share-code","label": "Translated Label", "value": "1AA AAA GGG" , "url": "applicant-share-code" },
  { "key": "enter-address","label": "Translated Label", "value": "Mrs Smith 71<br>SOUTHAMPT Cherry Court<br>SOUTHAMPTON<br>UK<br>SO53 5PD<br>" ,"url": "enter-address" },
  { "key": "email-address","label": "Translated Label", "value": "abc@abc.com" ,"url": "email-address" },
  { "key": "applicant-file-upload","label": "Translated Label", "value": "Tranformed Value" , "url": "uploaded-files-evidence"}];

const dateInput = '2006-04-11';
const dateOutput = '11 April 2006';

const fileName =  [
  {
    srNo: 1,
    fileName: 'dummy.pdf',
    fileId: '1f4632f6-38ed-447e-a866-debb3efe8a9c'
  }
];

const filesList = [ [
  {
    srNo: 1,
    fileName: 'dummy.pdf',
    fileId: '1f4632f6-38ed-447e-a866-debb3efe8a9c'
  }
],  [
  {
    srNo: 2,
    fileName: 'dummy.pdf',
    fileId: '1f4632f6-1234-447e-a866-debb3efe8a9c'
  }
]];
const formattedFileList = 'dummy.pdf<br>dummy.pdf';

module.exports = {
  sessionModel,
  allDependants,
  personalDetails,
  displayDependants,
  displayPersonalDetails,
  dateInput,
  fileName,
  filesList,
  formattedFileList,
  dateOutput
}
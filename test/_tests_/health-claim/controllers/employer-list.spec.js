const EmployerListController = require('../../../../apps/health-claim/controllers/employer-list');
const HOFController = require('hof-govfrontend-v3').controller;
const mockData = require('../../../_mocks_/health-claim/controllers/employer-details');
const {  reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('EmployerListController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('EmployerListController instance', () => {
    let employerListController;
    beforeEach(() => {
      employerListController = new EmployerListController({});
    });

    it('extends the hof-form-controller', () => {
      employerListController.should.be.instanceof(HOFController);
    });

    it('has locals, process methods', () => {
      employerListController.should.have.property('process').that.is.a('function');
      employerListController.should.have.property('locals').that.is.a('function');
    });

    describe('locals', () => {
     beforeEach(() => {
      sinon.stub(HOFController.prototype, 'locals').returns({
        fields: []
      });
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();          
      });

      afterEach(() => {
        HOFController.prototype.locals.restore();
      }); 

      it('calls locals', () => {
        req.form.values['subscribe-for-reminder'] = sinon.stub(); 
        employerListController.locals(req, res, next);
      });

      it('calls locals  when subscribe-for-reminder return value', () => {  
        req.sessionModel.get.withArgs('subscribe-for-reminder').returns('yes');  
        employerListController.locals(req, res, next);
        HOFController.prototype.locals.should.have.been.calledWithExactly(req, res, next);
      });
      it('calls locals  when subscribe-for-reminder does not return value', () => {  
        req.sessionModel.get.withArgs('subscribe-for-reminder').returns('');  
        employerListController.locals(req, res, next);
        HOFController.prototype.locals.should.have.been.calledWithExactly(req, res, next);
      });

    });

    describe('process', () => {
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'process').returns({
          fields: []
        });
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();          
      });
      afterEach(() => {
        HOFController.prototype.process.restore();
      });
      
      it('calls process with arguments', () => {
        employerListController.process(req, res, next);
      });

      it('calls process with check your answer session value', () => {
        req.sessionModel.get.withArgs('check-your-answers').returns(true);
        employerListController.process(req, res, next);
      });

      it('calls process when no value set', () => {        
        req.sessionModel.get.withArgs('all-employers').returns(mockData.allEmployer);
        employerListController.process(req, res, next);
      });

    });
  });

});
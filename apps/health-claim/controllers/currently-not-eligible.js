const Controller = require('hof-govfrontend-v3').controller;
const dateFormat = 'YYYY-MM-DD';
const moment = require('moment');

module.exports = class NotEligibleStartClaimController extends Controller {

    render(req, res, callback) {
        if (!moment(req.sessionModel.get('claimants-start-date'), dateFormat).isValid())
            return res.redirect(`${req.baseUrl}/name`);

        res.locals.claimantsStartDate = moment(req.sessionModel.get('claimants-start-date'), dateFormat).format('D MMMM YYYY');
        res.locals.claimantsStartDate += ' ';
        res.locals.claimantsEndDate = moment(req.sessionModel.get('claimants-end-date'), dateFormat).format('D MMMM YYYY');
        res.locals.claimantsEndDate += '.';
        res.locals.newClaimantsEndDate = moment(req.sessionModel.get('calculated-earliest-date'), dateFormat).format('D MMMM YYYY') + '.';
        const locals = super.render(req, res, callback);
        return locals
    }

};
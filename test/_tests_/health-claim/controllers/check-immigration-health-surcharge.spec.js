const checkIHS = require('../../../../apps/health-claim/controllers/check-immigration-health-surcharge');
const HOFController = require('hof-govfrontend-v3').controller;
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash');


describe('Check checkIHS', () => {
    let req;
    let res;
    const next = sinon.stub();

    beforeEach(() => {
        req = reqres.req();
        res = reqres.res();
    });
    describe('Check Ihs Controller instance', () => {
        let checkIhsObj;
        const validationError = {
            type: 'regex'
          };

        beforeEach(() => {
            checkIhsObj = new checkIHS({});
            checkIhsObj.ValidationError = sinon.stub();
            checkIhsObj.ValidationError.returns(validationError);
            exception = sinon.stub();
        });

        it('extends the hof-form-controller', () => {
            checkIhsObj.should.be.instanceof(HOFController);
        });

        it('has process methods', () => {
            checkIhsObj.should.have.property('process').that.is.a('function');
        });
        
        it('calls process with arguments', () => {
            checkIhsObj.process(req, res, next);
        });
           
        it('calls process with custom Exception', () => {
            exception.customeException = sinon.stub();
            checkIhsObj.process(req, res, next);
        });

        it('calls process and gets claimantsData', () => {
            exception.customeException = sinon.stub();
            checkIhsObj.process(req, res, next);
        });

        
    });
});

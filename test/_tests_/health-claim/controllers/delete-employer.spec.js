const DeleteEmployerController = require('../../../../apps/health-claim/controllers/delete-employer');
const HOFController = require('hof-govfrontend-v3').controller;
const mockData = require('./../../../_mocks_/health-claim/controllers/employer-details');
const {  reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('DeleteEmployerController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('DeleteEmployerController instance', () => {
    let deleteEmployerController;
    const employerId = '1';
    beforeEach(() => {
      // an instance of the DeleteEmployerController
      deleteEmployerController = new DeleteEmployerController({});
    });

    it('extends the hof-form-controller', () => {
      deleteEmployerController.should.be.instanceof(HOFController);
    });

    it('has get, deleteEmployer methods', () => {
      deleteEmployerController.should.have.property('get').that.is.a('function');
      deleteEmployerController.should.have.property('deleteEmployer').that.is.a('function');
    });

    describe('get', () => {
      beforeEach(() => {
        deleteEmployerController.deleteEmployer = sinon.stub();
      });

      it('calls next() skiping consdition if there is no employerId', () => {
        req.query = {};
        deleteEmployerController.get(req, res, next);
      });

      it('calls deleteEmployer when employerId is available', () => {
        req.query = { employerId };
        deleteEmployerController.get(req, res, next);
        deleteEmployerController.deleteEmployer.should.have.been.calledWithExactly(req, res, employerId);
      });
    });

    describe('deleteEmployer', () => {
      beforeEach(() => {
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();
        req.sessionModel.unset = sinon.stub();        
        res.redirect = sinon.stub();
        req.session = sinon.stub();
        req.session['hof-wizard-health-claim'] = sinon.stub();
        req.baseUrl = '/health-claim';
      });

      it('skips deleteEmployer if employer with Id not found', () => {
        req.sessionModel.get.returns([]);
        deleteEmployerController.deleteEmployer(req, res, employerId);
        res.redirect.should.have.been.called;
      });

      it('Calls redirect to add employer page, if all employers are deleted', () => {
        req.sessionModel.get.returns([{ 'employer-id': employerId }]);
        deleteEmployerController.deleteEmployer(req, res, employerId);
        res.redirect.should.have.been.calledWithExactly('/health-claim/add-employer');
      });

      it('Calls redirect to check employer page, if all dependants are not deleted', () => {
        req.sessionModel.get.returns([{ 'employer-id': employerId }, { 'employer-id': '1' }]);
        deleteEmployerController.deleteEmployer(req, res, employerId);
        res.redirect.should.have.been.calledWithExactly('/health-claim/check-employment');
      });

      it('Redirects based on the error and number of employers in the session ', () => {
        req.sessionModel.get.returns([{ 'employer-id': employerId }, { 'employer-id': '1' }]);
        _.set(req, 'hof-wizard-health-claim',   'errors'    );
        deleteEmployerController.deleteEmployer(req, res, employerId);
        res.redirect.should.have.been.calledWithExactly('/health-claim/check-employment');
      });
/*
      it('calls next() with validation error', () => {        
        const addMoreEmployersError = { 'add-more-employer': { type: 'maxEmployer' } };

         req.sessionModel.get.returns(addMoreEmployersError);
        _.set(req, 'hof-wizard-health-claim', {
          "add-more-employer": 'yes'
        });
        
        deleteEmployerController.deleteEmployer(req, res, employerId);
      //  res.redirect.should.have.been.calledWithExactly('/add-employer');
      });*/
    });
  });

});
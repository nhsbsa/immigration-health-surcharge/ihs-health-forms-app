const DeleteEvidanceCommentsController = require('../../../../apps/health-claim/controllers/delete-evidence-extra-info');
const HOFController = require('hof-govfrontend-v3').controller;
const mockData = require('../../../_mocks_/health-claim/controllers/evidance-details');

const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('DeleteEvidanceCommentsController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('DeleteEvidanceCommentsController instance', () => {
    let deleteEvidanceCommentsController;
    const dependantId = 'abc123';
    beforeEach(() => {
      // an instance of the DeleteEvidanceCommentsController
      deleteEvidanceCommentsController = new DeleteEvidanceCommentsController({});
      req.sessionModel.get = sinon.stub();
      req.sessionModel.set = sinon.stub();         

    });

    it('extends the hof-form-controller', () => {
      deleteEvidanceCommentsController.should.be.instanceof(HOFController);
    });

    it('has get methods', () => {
      deleteEvidanceCommentsController.should.have.property('get').that.is.a('function');
    });

    describe('get', () => {
      beforeEach(() => {
        exception = sinon.stub();
        claimantsData = sinon.stub();
      });

      it('calls get function', () => {
        req.sessionModel.get.withArgs('health-claim-submit-flag').returns(true);
        req.query = { fileId: '3455677888889887' };
        req.sessionModel.get.withArgs('uploadedFileExtraInfo').returns(mockData.uploadedFileExtraInfo);   
        deleteEvidanceCommentsController.get(req, res, next);
      });   

      it('calls get with custom Exception', () => {
        exception.customeException = sinon.stub();
        deleteEvidanceCommentsController.get(req, res, next);
      });
    });   
  });
});
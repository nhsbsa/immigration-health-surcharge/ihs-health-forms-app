$('#employer-error-link').on('click', (event) => {  
  $("#employer-error-link").removeAttr("href");
  $("#employer-error-link").css("color", "#d4351c");
  $('#add-employer-error')[0].scrollIntoView();
  $('#add-employer')[0].focus({
    preventScroll: true
  }); 
});


$('#job-title-error-link').on('click', (event) => {  
  $("#job-title-error-link").removeAttr("href");
  $("#job-title-error-link").css("color", "#d4351c");
  $('#job-title-error')[0].scrollIntoView();
  $('#job-title-1-change')[0].focus({
    preventScroll: true
  }); 
});
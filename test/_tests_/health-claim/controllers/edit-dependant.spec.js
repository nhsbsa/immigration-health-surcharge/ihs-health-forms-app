const EditDependantController = require('../../../../apps/health-claim/controllers/edit-dependant');
const HOFController = require('hof-govfrontend-v3').controller;
const mockData = require('../../../_mocks_/health-claim/controllers/employer-details');
const { ValidationError } = require('hof-govfrontend-v3/controller');
const { chai, reqres, sinon, expect, assert } = require('../../../setup');
//const {  reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('EditDependantController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('EditDependantController instance', () => {
    let editDependantController;
    const dependantId = '1';
    const pagename = 'abc';
    beforeEach(() => {
      // an instance of the EditDependantController
      editDependantController = new EditDependantController({});
    });

    it('extends the hof-form-controller', () => {
      editDependantController.should.be.instanceof(HOFController);
    });

    it('has get, editDependant methods', () => {
      editDependantController.should.have.property('get').that.is.a('function');
      editDependantController.should.have.property('editDependant').that.is.a('function');
    });

    describe('get', () => {
      beforeEach(() => {
        editDependantController.editDependant = sinon.stub();
      });

      it('calls next() skiping consdition if there is no dependantId', () => {
        req.query = {};
        const resp =  editDependantController.get(req, res, next);
        expect(resp).to.be.undefined;
      });

      it('calls editDependant when dependantId is available', () => {
        req.query = { dependantId };
        const resp = editDependantController.get(req, res, next);
        editDependantController.editDependant.should.have.been.calledWithExactly(req, res, dependantId);
      });
    });

    describe('editDependant', () => {
      beforeEach(() => {
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();
        req.sessionModel.unset = sinon.stub();        
        res.redirect = sinon.stub();
        req.session = sinon.stub();
        req.session['hof-wizard-health-claim'] = sinon.stub();
        req.baseUrl = '/health-claim';
      });

      it('skips editDependant if employer with Id not found', () => {
        req.sessionModel.get.returns([]);
        editDependantController.editDependant(req, res, dependantId);
        res.redirect.should.have.been.called;
      });

      it('Calls redirect to edit Dependant page, if all employers are deleted', () => {
        req.sessionModel.get.returns([{ 'dependant-id': dependantId }]);
        editDependantController.editDependant(req, res, dependantId,'dependant-details');
        res.redirect.should.have.been.calledWithExactly('/health-claim/dependant-details?dependantId=1');
      });

      it('Calls redirect to check employer page, if all dependants are not deleted', () => {
        req.sessionModel.get.returns([{ 'dependant-id': dependantId }, { 'dependant-id': '1' }]);
        editDependantController.editDependant(req, res, dependantId,'dependant-details');
        res.redirect.should.have.been.calledWithExactly('/health-claim/dependant-details?dependantId=1');
      });
    });
  });

});
const Controller = require('hof-govfrontend-v3').controller;
const { moreEmployerClearSession } = require('../constants/employer');

module.exports = class DeleteEmployerController extends Controller {

  get(req, res, next) {
    const employerId = req.query.employerId;
    if (employerId) {
      this.deleteEmployer(req, res, employerId)
    }
  }

  deleteEmployer(req, res, employerId) {
    const allEmployers = req.sessionModel.get('all-employers')
    const employerIndex = allEmployers.findIndex(employer => employer['employer-id'] === employerId)
    if (employerIndex !== -1) {
      const deletedDependant = (employerIndex + 1) + ' ' + allEmployers[employerIndex]['employer-name'] + ' ';
      allEmployers.splice(employerIndex, 1);
      req.sessionModel.set('all-employers', allEmployers);
      req.sessionModel.set('deletedEmployer', deletedDependant);
      req.sessionModel.unset(moreEmployerClearSession);
      req.sessionModel.unset('employer-id');
    }
    
    if (req.session['hof-wizard-health-claim'].errors != null && req.session['hof-wizard-health-claim'].errors.hasOwnProperty('add-more-employer'))
      delete req.session['hof-wizard-health-claim'].errors['add-more-employer'];

    const nextNav = allEmployers.length === 0 ? '/add-employer' : '/check-employment';
    return res.redirect(`${req.baseUrl}${nextNav}`);
  }
};
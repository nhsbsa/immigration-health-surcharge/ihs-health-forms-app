'use strict';
const { viewAndProveImmigrationStatus, startPageUrl, completionPageFeedbackURL, moreInfoData, contactUKLink } = require('./../../configRoutes');
const continueButtonId = 'continue'
module.exports = {
  name: 'health-claim',
  baseUrl: '/health/claim',
  confirmStep: '/check-your-answers',
  steps: {
      '/name': {
      fields: ['given-name', 'family-name'],
      next: '/date-of-birth',
      locals: {
        step: 'name',
        labelClassName: 'govuk-input',
        id: continueButtonId
      },
      backLink: false
    },
    '/date-of-birth': {
      fields: ['date-of-birth'],
      next: '/national-insurance-number',
      controller: require('./controllers/validate-date-of-birth'),
      locals: {
        step: 'date-of-birth',
        labelClassName: 'govuk-input',
        id: continueButtonId
      }
    },
    '/national-insurance-number': {
      fields: ['national-insurance-number'],
      next: '/immigration-health-surcharge-number',
      controller: require('./controllers/check-nino'),
      locals: {
        step: 'national-insurance-number',
        labelClassName: 'govuk-input govuk-input--width-10',
        id: continueButtonId
      }
    },
    '/immigration-health-surcharge-number': {
      fields: ['immigration-health-surcharge-number'],
      next: '/email',
      controller: require('./controllers/check-immigration-health-surcharge'),
      locals: {
        step: 'immigration-health-surcharge-number',
        labelClassName: 'govuk-input govuk-input--width-10',
        contactUKLink: contactUKLink,
        id: continueButtonId
      }
    },
    '/email': {
      fields: ['email'],
      next: '/phone-number',
      locals: {
        step: 'email',
        id: continueButtonId
      }
    },
    '/phone-number': {
      fields: ['phone-number'],
      next: '/dependant-question',      
      controller: require('./controllers/get-bsa-referance'),
      locals: {
        step: 'phone-number',
        id: continueButtonId
      }
    },
    '/dependant-question': {
      fields: ['dependant-question'],
      controller: require('./controllers/add-dependant'),
      next: '/dependant-details',
      locals: {
        step: 'dependant-question',
        id: continueButtonId
      },
      forks: [{
        target: '/claim-start-date',
        condition: {
          field: 'dependant-question',
          value: 'no'
        }
      }]
    },
    '/dependant-details': {
      fields: ['dependant-given-name', 'dependant-family-name', 'dependant-date-of-birth', 'dependant-immigration-health-surcharge-number'],
      next: '/check-dependants',
      locals: {
        step: 'dependant-details',
        labelClassName: 'govuk-input govuk-input--width-10',
        contactUKLink: contactUKLink,
        id: continueButtonId
      }
    },
    '/check-dependants': {
      fields: ['add-more-dependants'],
      controller: require('./controllers/check-dependants'),
      next: '/claim-start-date',
      locals: {
        step: 'check-dependants',
        labelClassName: 'govuk-input govuk-input--width-10',
        id: continueButtonId
      },
      backLink: false,
      forks: [{
        target: '/claim-start-date',
        condition: {
          field: 'check-dependants',
          value: 'no'
        }
      }]
    },
    '/dependant-name': {
      fields: ['dependant-given-name', 'dependant-family-name'],
      next: '/dependant-date-of-birth',
      controller: require('./controllers/fetch-dependant'),
      locals: {
        step: 'dependant-name',
        id: continueButtonId
      }
    },
    '/dependant-date-of-birth': {
      fields: ['dependant-date-of-birth'],
      controller: require('./controllers/fetch-dependant'),
      next: '/dependant-immigration-health-surcharge-number',
      locals: {
        step: 'dependant-date-of-birth',
        id: continueButtonId
      }
    },
    '/delete-dependant': {
      controller: require('./controllers/delete-dependant'),
      backLink: false
    },
    '/delete-employer': {
      controller: require('./controllers/delete-employer'),
      backLink: false
    },
    '/update-list-evidence': {
      controller: require('./controllers/update-list-evidence'),
      locals: {
        fileNameKey: 'uploadedFileEvidence',
      },
      backLink: false
    },
    '/delete-evidence': {
      controller: require('./controllers/delete-evidence'),
      backLink: false,
      locals: {
        fileNameKey: 'uploadedFileEvidence'
      }
    },
    '/delete-evidence-extra-info': {
      controller: require('./controllers/delete-evidence-extra-info'),
      backLink: false,
      locals: {
        fileNameKey: 'uploadedFileEvidenceExtraInformation'
      }
    },
    '/edit-dependant': {
      controller: require('./controllers/edit-dependant'),
      backLink: false
    },
    '/edit-employer': {
      controller: require('./controllers/edit-employer'),
      backLink: false
    },
    '/claim-start-date': {
      fields: ['claim-start-date'],
      next: '/subscribe-for-reminder',
      controller: require('./controllers/claim-start-date'),
      locals: {
        step: 'claim-start-date',
        labelClassName: 'govuk-input',
        id: continueButtonId
      }
    },
    '/currently-not-eligible': {
      template: 'currently-not-eligible',
      controller: require('./controllers/currently-not-eligible'),
      locals: {
        backLink: '',
        feedbackUrl: '', // So that the banner will not display
        moreInfoData: moreInfoData
      }
    },
    '/subscribe-for-reminder': {
      fields: ['subscribe-for-reminder'],
      controller: require('./controllers/employer-list'),
      next: '/add-employer',
      locals: {
        step: 'subscribe-for-reminder',
        labelClassName: 'govuk-input govuk-input--width-10',
        id: continueButtonId
      }
    },
    '/add-employer': {
      fields: ['employer-name', 'primary-role-id'],
      controller: require('./controllers/add-employer'),
      next: '/job-title',
      locals: {
        step: 'add-employer',
        id: continueButtonId
      },
    },
    '/job-title': {
      fields: ['job-title'],
      controller: require('./controllers/job-title'),
      next: '/job-setting',
      locals: {
        step: 'job-title',
        id: continueButtonId
      }
    },
    '/job-setting': {
      fields: ['job-setting', 'other-job-setting'],
      next: '/check-employment',
      controller: require('./controllers/job-setting'),
      className: 'govuk-radios ',
      locals: {
        step: 'job-setting',
        id: continueButtonId
      }
    },
    '/check-employment': {
      fields: ['add-more-employer'],
      controller: require('./controllers/check-employment'),
      next: '/upload-evidence',
      locals: {
        step: 'check-employment',
        id: continueButtonId
      },
      backLink: false,
    },
    '/upload-evidence': {
      fields: ['upload-evidence'],
      next: '/attachments',
      behaviours: require('./behaviours/upload-file'),
      locals: {
        step: 'upload-evidence',
        fileNameKey: 'uploadedFileEvidence',
        labelClassName: 'govuk-input govuk-input--width-10',
        id: continueButtonId
      }
    },
    '/attachments': {
      fields: ['add-more-files-evidence'],
      controller: require('./controllers/get-file-values'),
      next: '/extra-information',
      locals: {
        step: 'attachments',
        labelClassName: 'govuk-input govuk-input--width-10',
        id: continueButtonId
      },
      backLink: false
    },
    '/extra-information': {
      fields: ['extra-information'],
      behaviours: require('./behaviours/upload-file-extra-info'),
      next: '/extra-evidence-uploaded',
    //  next: '/check-your-answers',
      locals: {
        step: 'extra-information',
        backLink: 'attachments',
        fileNameKey: 'uploadedFileExtraInfo',
        id: continueButtonId,
        labelClassName: 'govuk-visually-hidden'
      },
    },
    '/extra-evidence-uploaded': {
      fields: ['add-more-files-extra-info'],
      controller: require('./controllers/extra-evidence-uploaded'),
      next: '/check-your-answers',
      locals: {
        step: 'extra-evidence-uploaded',
        labelClassName: 'govuk-input govuk-input--width-10',
        id: continueButtonId
      },
      backLink: false
    },
    '/check-your-answers': {
      controller: require('./controllers/check-your-answers'),
      next: '/declaration',
      locals: {
        step: 'check-your-answers',
        id: continueButtonId
      }
    },
    '/declaration': {
      template: 'declaration',
      next: '/done',
      controller: require('./controllers/submit-claim')
    },
    '/done': {
      template: 'application-complete',
      controller: require('./controllers/get-claim-reference-number'),
      locals: {
        feedbackUrl: '', // So that the banner will not display
        completionPageFeedbackURL
      },
      backLink: false
    },
    '/set-test-settings': {
      controller: require('./controllers/set-test-settings')
    }
  }

};

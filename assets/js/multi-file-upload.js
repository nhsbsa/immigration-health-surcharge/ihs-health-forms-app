try {
    let multiFileLib = require('./multi-file-upload-override');
    $('.moj-multi-file-upload').show();
    window.history.replaceState({}, document.title, document.location.pathname);
    const documentTitle = document.title;
    const pageUrlName = '/' + window.location.pathname.split('/')[window.location.pathname.split('/').length - 1];

    ($(document).find('.moj-multi-file-upload__row').length !== 0) ? $('#files-added-component').show().attr('aria-hidden', false) : '';
    //TODO: 2030 REMOVE THE FLIXKET AND PROPER FOCUS
    if ($(document).find('#file-listing-error-summery').length !== 0) {
        $(document).find('#file-listing-error-summery').show();
        document.title = 'Error: ' + documentTitle;
    }

    multiFileLib.initAll();
    let multFileObj = new multiFileLib.MultiFileUpload({
        container: $('.moj-multi-file-upload'),
        uploadUrl: `${$('.moj-multi-file-upload').attr('data-upload-file-url')}multifile/evidence`,
        fileSizeLimit: `${$('.moj-multi-file-upload').attr('file-size-limit')}`,
        deleteUrl: `${$('.moj-multi-file-upload').attr('data-upload-file-url')}multifile/evidence`,
        documentTitle: documentTitle,
        pageUrlName: pageUrlName,
        sessionId: $('#hof-session-id').val(),
        uploadFileEntryHook: (pointer, files) => {
            $('#files-added-component').show().attr('aria-hidden', false);
            ($(document).find('.moj-multi-file-upload__list').length == 0) ? $(document).find('.moj-multi-file__uploaded-files').html(`<div class="govuk-summary-list moj-multi-file-upload__list"></div>`) : '';
        },
        uploadFileExitHook: (pointer, files, callbackObj) => {
            pointer.updateFileQueue();
        },
        uploadFileErrorHook: (pointer, file, xhrResponse, validationObj, item = false) => {
            pointer.updateFileQueue();

            if (validationObj.type != 'delete-fail')
                pointer.clearAllDeleteFileErrors();

            let statusCode = xhrResponse.status || xhrResponse.statusCode;
            if (statusCode == 429 && $(document).find('#error-msg-li-429').length === 0) {
                pointer.addErrorSummery(429, pointer.validationMessage.filelimit, statusCode, true);
            }
            if (!window.navigator.onLine && $(document).find('#error-msg-li-100').length === 0)
                pointer.addErrorSummery(100, pointer.validationMessage.connectionInterrupted, 100, true);

            pointer.markAsErrorFile(file, validationObj, item, statusCode)
            $('#multi-file-notification-banner-info').html(``);
            pointer.status.html(validationObj.message);
            pointer.setFocusOnErrorSummery();
        },
        fileDeleteHook: (pointer, file, xhrResponse) => {
            console.debug('hooks fileDeleteHook', pointer);
            pointer.clearAllDeleteFileErrors();
            $(file.element.currentTarget).parents('.moj-multi-file-upload__row').remove();
            $(`#error-msg-li-${$(file.element.currentTarget).attr('data-fileId')}`).remove();
            pointer.FileQueuecomplete();

            $('#multi-file-notification-banner-info').html(pointer.getNotificationBannerHtml(file));
            if ($(document).find('.govuk-summary-list__row').length === 0) {
                $('#files-added-component').hide().attr('aria-hidden', true);
                $(document).find('.moj-multi-file__uploaded-files').html(``);
            }
            pointer.setFocusOnNotification();
        },
    });

    $('#continue-button').on('click', (event) => {
        event.preventDefault();
        $('#multi-file-notification-banner-info').html(``);
        if ($(document).find('#file-listing-error-summery').length !== 0 || $('#error-msg-li-430').length !== 0) {
            multFileObj.__proto__.setFocusOnErrorSummery();
            return false;
        }

        if ($(document).find('.moj-multi-file-upload__row').length === 0) {

            $('.form-group').addClass('govuk-form-group--error');
            $('#bref-error-summery').html(`<span class="govuk-error-message"><span class="govuk-visually-hidden">upload evidence</span>${multFileObj.__proto__.validationMessage.required}</span>`);
            $('#multi-file-error-summary').html(`
            <div class="govuk-error-summary" aria-labelledby="error-summary-title" data-module="govuk-error-summary" role="alert" tabindex="-1">
                <h2 class="govuk-error-summary__title" id="error-summary-title">There is a problem</h2>
                  <div class="govuk-error-summary__body">
                  <ul class="govuk-list govuk-error-summary__list">
                  <li id="error-msg-li-no-file"><a href="#documents">${multFileObj.__proto__.validationMessage.required}</a></li>
                  </ul>
              </div>
            </div>
            `);
            document.title = 'Error: ' + documentTitle;
            $('.govuk-error-summary').trigger('focus');
            return false;
        }

        if ($(document).find('.moj-multi-file-upload__progress').length !== 0) {
            multFileObj.__proto__.markWaitingOfProcess();
            return false;
        }

        if ($(document).find('.govuk-error-summary__list').length == 0) {
            window.location.replace(`${window.location.origin}${window.location.pathname.replace('upload-evidence', 'update-list-evidence')}`);
        } else {
            $('.govuk-error-summary').trigger('focus');
        }
    });


} catch (error) {
    console.error('multifile upload error,', error);
}
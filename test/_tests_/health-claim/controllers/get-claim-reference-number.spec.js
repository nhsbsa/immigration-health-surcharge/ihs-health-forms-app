const ClaimReferenceNumberController = require('../../../../apps/health-claim/controllers/get-claim-reference-number');
const HOFController = require('hof-govfrontend-v3').controller;
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('ClaimReferenceNumberController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('ClaimReferenceNumberController instance', () => {
    let claimReferenceNumberController;
    beforeEach(() => {
      claimReferenceNumberController = new ClaimReferenceNumberController({});
    });

    it('extends the hof-form-controller', () => {
      claimReferenceNumberController.should.be.instanceof(HOFController);
    });

    it('has getValues, locals methods', () => {
      claimReferenceNumberController.should.have.property('getValues').that.is.a('function');
      claimReferenceNumberController.should.have.property('locals').that.is.a('function');
    });

    describe('getValues', () => {    
      
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'getValues').returns({
          fields: []
        });
        _.set(claimReferenceNumberController, 'options.locals.field', {})
      });
      afterEach(() => {
        HOFController.prototype.getValues.restore();
      });

      it('calls super.getValues', () => {
        claimReferenceNumberController.getValues(req, res, next);
        HOFController.prototype.getValues.should.have.been.calledOnce
          .and.calledWithExactly(req, res, next);
      });
      it('calls getValues', () => {
        claimReferenceNumberController.getValues(req, res, next);
      }); 

    });

    describe('locals', () => {
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'locals').returns({
          fields: []
        });
        res.locals.values  = sinon.stub();
        req.sessionModel.get = sinon.stub();
        req.sessionModel.get.withArgs('claim-reference-number').returns('BSA123123123');
        req.sessionModel.get.withArgs('claim-calculated-end-date').returns('2000-09-09');
        res.locals.values.withArgs('claim-start-date').returns('2000-09-09');
        
      });

      afterEach(() => {
        HOFController.prototype.locals.restore();
      });


      it('calls locals', () => {
        claimReferenceNumberController.locals(req, res, next);
        HOFController.prototype.locals.should.have.been.calledOnce
          .and.calledWithExactly(req, res, next);
      });

    });
  });

});
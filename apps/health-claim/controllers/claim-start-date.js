const Controller = require('hof-govfrontend-v3').controller;
const moment = require('moment');
const BsaRefModel = require('../models/bsa-reference');
const dateFormat = 'YYYY-MM-DD';

module.exports = class getbackLink extends Controller {
  async render(req, res, next) {
    try {
      const isedit = (res.locals.action.includes("/edit")) ? '/edit' : '';
      let allDependants = req.sessionModel.get('all-dependants');
      var backlink;
      if (!(allDependants) || allDependants.length === 0) {
        backlink = 'dependant-question';
      } else {
        backlink = 'check-dependants';
      }
      res.locals.backLink = "/health/claim/" + backlink + isedit;
      res.locals.options.locals.backLink = "/health/claim/" + backlink + isedit;
      let validationDate = req.sessionModel.get('validation-date');
      let currentDate = (typeof validationDate === 'undefined') ? moment(new Date()).format(dateFormat) : validationDate;
      const bsaRefModel = new BsaRefModel();
      let claimantsData = await bsaRefModel.getClaimantsSearch({
        "immigrationHealthSurchargeNumber": req.sessionModel.get('immigration-health-surcharge-number'),
        "dateOfBirth": req.sessionModel.get('date-of-birth'),
        "givenName": req.sessionModel.get('given-name'),
        "familyName": req.sessionModel.get('family-name'),
        "latest": true
      });
      if (typeof claimantsData.data['claimants'] !== 'undefined' && claimantsData.data['claimants'].length != 0) {
        let previousEndDate = moment((claimantsData.data['claimants'][0]['endDate']).split('T')[0], dateFormat);
        let previousStartDate = moment((claimantsData.data['claimants'][0]['startDate']).split('T')[0], dateFormat);
        if (previousEndDate.isAfter(moment(currentDate, dateFormat))) {
          req.sessionModel.set('claimants-start-date', (claimantsData.data['claimants'][0]['startDate']).split('T')[0]);
          req.sessionModel.set('claimants-end-date', (claimantsData.data['claimants'][0]['endDate']).split('T')[0]);
          return res.redirect(`${req.baseUrl}/currently-not-eligible`);
        }
               
        res.locals.earliestStartDate = moment(previousEndDate, dateFormat).add(1, 'day').format('DD MMMM YYYY') + '.';
        req.sessionModel.set('earliest-start-date', previousEndDate);
        res.locals.previousStartDate = moment(previousStartDate, "YYYY-MM-DD").format('DD MMMM YYYY');
        res.locals.previousEndDate = moment(previousEndDate, "YYYY-MM-DD").format('DD MMMM YYYY');
      }else{
        req.sessionModel.unset('earliest-start-date');
        req.sessionModel.unset('claimants-start-date');
        req.sessionModel.unset('claimants-end-date');
      }
      const locals = super.render(req, res, next);
      return locals;
    } catch (error) {
      req.sessionModel.unset('earliest-start-date');
      req.sessionModel.unset('claimants-start-date');
      req.sessionModel.unset('claimants-end-date');
      console.error('Error occured while calculating earliest Claim start date, Please try again later!! ');
      const locals = super.render(req, res, next);
      return locals;
    }
  }

  async process(req, res, next) {
    try {
      let validationDate = req.sessionModel.get('validation-date');
      let currentDate = (typeof validationDate === 'undefined') ? moment(new Date()).format(dateFormat) : validationDate;
      let validate = this.checkValidation(req.form.values['claim-start-date'], req, res);
      let before = this.beforeCheck(req.form.values['claim-start-date'], currentDate, [6, 'months'], req, res);
      let after = this.afterCheck(req.form.values['claim-start-date'], req, res);
      let overlap = this.overlapCheck(req.form.values['claim-start-date'], req, res);
      if (!validate.status) {
        return next(validate.errorObject);
      } else if (!before.status) {
        return next(before.errorObject);
      } else if (!after.status) {
        return next(after.errorObject);
      } else if (!overlap.status) {
        return next(overlap.errorObject);
      } else {
        const bsaRefModel = new BsaRefModel();
        let endDate = await bsaRefModel.getEndDate(req.form.values['claim-start-date']);
        req.sessionModel.set('claim-calculated-end-date', endDate.data['calculatedDate']);
        return next();
      }
    } catch (error) {
      next(new Error(error.body));
    }
  }

  flashError(req, res, validationType) {
    return {
      'claim-start-date': new this.ValidationError('claim-start-date', {
        type: validationType
      }, req, res)
    };
  }

  checkValidation(formValue, req, res) {
    if (formValue === '')
      return { status: false, errorObject: this.flashError(req, res, 'required') };

    let pattern = /\d{4}\-\d{2}\-\d{2}/;
    if (!pattern.test(formValue) && moment(formValue, dateFormat).isValid())
      return { status: false, errorObject: this.flashError(req, res, 'date') };

    let dateYear = parseInt(moment(formValue, dateFormat).format('YYYY'));
    let dateMonth = parseInt(moment(formValue, dateFormat).format('M'));
    let dateDay = parseInt(moment(formValue, dateFormat).format('D'));

    if (!(/^\d{4}$/).test(dateYear))
      return { status: false, errorObject: this.flashError(req, res, 'date') };

    if (!(/^\d{1,2}$/).test(dateMonth) && parseInt(dateMonth, 10) > 0 && parseInt(dateMonth, 10) < 13)
      return { status: false, errorObject: this.flashError(req, res, 'date') };

    if (!(/^\d{1,2}$/).test(dateDay) && parseInt(dateDay, 10) > 0 && parseInt(dateDay, 10) < 32)
      return { status: false, errorObject: this.flashError(req, res, 'date') };

    return { status: true, errorObject: {} };
  }

  beforeCheck(formValue, validationDate, argumentsList, req, res) {

    let valueDate = moment(formValue, dateFormat).add(parseInt(argumentsList[0]), argumentsList[1]);

    if (moment(validationDate, dateFormat).isBefore(valueDate))
      return { status: false, errorObject: this.flashError(req, res, 'before') }
    else
      return { status: true, errorObject: {} };
  }

  afterCheck(valueDate, req, res) {

    if (!moment(valueDate, dateFormat).isSameOrAfter('2020-03-31'))
      return { status: false, errorObject: this.flashError(req, res, 'after') }
    else
      return { status: true, errorObject: {} };
  }

  overlapCheck(valueDate, req, res) {
    if (!req.sessionModel.get('earliest-start-date')){
      return { status: true, errorObject: {} };
    }
    var earliestStartDate = moment(req.sessionModel.get('earliest-start-date'));
    var now = moment(valueDate, dateFormat);
    
    if (now.isAfter(earliestStartDate))
      return { status: true, errorObject: {} };
    else
      return { status: false, errorObject: this.flashError(req, res, 'overlap') }
  }
};
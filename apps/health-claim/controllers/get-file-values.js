const Controller = require('hof-govfrontend-v3').controller;
module.exports = class FileDetailsController extends Controller {

  getValues(req, res, callback) {
    const field = this.options.locals.field;
    super.getValues(req, res, callback);
  }

  locals(req, res, callback) {
    const locals = super.locals(req, res, callback);
    locals.uploadedFileEvidence = req.sessionModel.get('uploadedFileEvidence');
    locals.maxFileUpload = req.sessionModel.get('maxFileUpload');
    locals.evidenceDeletedInfo = req.sessionModel.get('evidenceDeletedInfo') ? req.sessionModel.get('evidenceDeletedInfo') : false;
    locals.evidenceDeleteErrorMessage =   req.sessionModel.get('evidenceDeleteErrorMessage') ? req.sessionModel.get('evidenceDeleteErrorMessage') : false;
    req.form.values['add-more-files-evidence']=req.sessionModel.get('addMoreFilesEvidence') ? req.sessionModel.get('addMoreFilesEvidence') : ''; 
    req.sessionModel.unset('evidenceDeleteErrorMessage');
    req.sessionModel.unset('evidenceDeletedInfo');
    req.sessionModel.unset('maxFileUpload');
    return locals;
  }

  process(req, res, next) {
    req.sessionModel.unset('addMoreFilesEvidence');
    if (req.form.values['add-more-files-evidence'] === 'yes' && req.sessionModel.get('uploadedFileEvidenceCount') >= process.env.MAX_FILE_COUNT) {
      req.sessionModel.set('maxFileUpload', true);
      return this.flashError(req, res, next, 'uploaded-files-evidence', "filecount");
    } else if (req.form.values['add-more-files-evidence'] === 'no') {
      req.sessionModel.set('addMoreFilesEvidence', 'no');
      if(req.sessionModel.get('check-your-answers')){
        const nextNav = '/check-your-answers';    
        return res.redirect(`${req.baseUrl}${nextNav}`); 
      }else{   
        return next();    
      }
    } else if (req.form.values['add-more-files-evidence'] === 'yes') {
      if (req.url.indexOf('/edit') != -1) {       
         res.redirect(`${req.baseUrl}/upload-evidence/edit`);         
      } else {
        return res.redirect(`${req.baseUrl}/upload-evidence`);
      }
    } else{
      return this.flashError(req, res, next, 'add-more-files-evidence', "required");
    }
  }

  flashError(req, res, next, currentStep, validationType) {
    const err = new this.ValidationError(currentStep, {
      type: validationType
    }, req, res);
    const errorObject = {};
    errorObject[currentStep] = err;
    return next(errorObject);
  }
};
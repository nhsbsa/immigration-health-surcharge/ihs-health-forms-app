const Controller = require('hof-govfrontend-v3').controller;

module.exports = class AdditionalEvidanceUploadedController extends Controller {

  getValues(req, res, callback) {
    const field = this.options.locals.field;
    super.getValues(req, res, callback);
  }

  locals(req, res, callback) {
    const locals = super.locals(req, res, callback);
    locals.uploadedFileEvidence = req.sessionModel.get('uploadedFileExtraInfo');
    locals.extraInfoMaxFileUpload = req.sessionModel.get('extraInfoMaxFileUpload');
    locals.evidenceDeletedInfo = req.sessionModel.get('evidenceDeletedInfo') ? req.sessionModel.get('evidenceDeletedInfo') : false;
    locals.evidenceDeleteErrorMessage =   req.sessionModel.get('evidenceDeleteErrorMessage') ? req.sessionModel.get('evidenceDeleteErrorMessage') : false;
    req.form.values['add-more-files-extra-info']=req.sessionModel.get('addMoreFilesExtraInfo') ? req.sessionModel.get('addMoreFilesExtraInfo') : ''; 
    
    if(req.sessionModel['attributes'].hasOwnProperty('errorValues') && req.sessionModel['attributes']['errorValues'].hasOwnProperty('extra-information')){
      delete(req.sessionModel['attributes']['errorValues']['extra-information']);
    } 
    req.sessionModel.unset('TempExtraInfo');
    req.sessionModel.unset('evidenceDeleteErrorMessage');
    req.sessionModel.unset('evidenceDeletedInfo');
    req.sessionModel.unset('extraInfoMaxFileUpload');
    return locals;
  }

  process(req, res, next) {
    req.sessionModel.unset('addMoreFilesExtraInfo');
    if (req.form.values['add-more-files-extra-info'] === 'yes' && req.sessionModel.get('uploadedFileCountExtraInfo') >= process.env.COMMENT_MAX_FILE_COUNT) {
      req.sessionModel.set('extraInfoMaxFileUpload', true);
      return this.flashError(req, res, next, 'uploaded-files-evidence', "filecount");
    } else if (req.form.values['add-more-files-extra-info'] === 'no') {
      req.sessionModel.set('addMoreFilesExtraInfo', 'no');
      return next();   
    } else if (req.form.values['add-more-files-extra-info'] === 'yes') {
      if (req.url.indexOf('/edit') != -1) {       
         res.redirect(`${req.baseUrl}/extra-information/edit`);         
      } else {
        return res.redirect(`${req.baseUrl}/extra-information`);
      }
    }else{
      return this.flashError(req, res, next, 'add-more-files-extra-info', "required");
    }  
  }

  flashError(req, res, next, currentStep, validationType) {
    const err = new this.ValidationError(currentStep, {
      type: validationType
    }, req, res);
    const errorObject = {};
    errorObject[currentStep] = err;
    return next(errorObject);
  }
};
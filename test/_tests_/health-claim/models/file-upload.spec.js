'use strict';
const Model = require('hof-govfrontend-v3').model;
const axios = require('axios').default;
const FileUploadModel = require('../../../../apps/health-claim/models/file-upload');

const { chai, reqres, sinon, expect, assert } = require('../../../setup');

const fileUploadMock = {
  fileuploadAPI() {
    return {
      "status": "200",
      "message": "Record created/updated successfully",
      "success": true
    };
  },
};

const fileDeleteMock = {
  fileDeleteAPI() {
    return {      
        "message": "File is deleted successfully",
        "status": 200
    };
  },
};

const fileListMock = {
  fileListAPI() {
    return {            
        "filesCount": 0,
        "files": [],
        "errors": []
    
    };
  },
};

describe('File Upload Model', () => {
  let sandbox;
  let req;
  const next = sinon.stub();

  beforeEach(function () {
    req = reqres.req();
    req.sessionModel.get = sinon.stub();    
    sandbox = sinon.createSandbox();
  });

  afterEach(() => sandbox.restore());

  it('returns a promise', () => {
    const claim = sinon.stub();
    const file = sinon.stub();
    const fileUploadModel = new FileUploadModel();
    const response = fileUploadModel.fileuploadAPI(claim,file,next);
    expect(response).to.be.an.instanceOf(Promise);
  });

  it('should throw an error if can\'t upload claim Evidence', () => {
    const fileUploadModel = new FileUploadModel();
    const stub = sinon.stub(fileUploadModel, 'fileuploadAPI').rejects();
    return fileUploadModel.fileuploadAPI(req, next).should.be.rejectedWith('')
      .then(() => {
        stub.restore();
      });
  });

   describe('fileuploadAPI', () => {
    it('returns a promise', () => {
      const fileUploadModel = new FileUploadModel();
      const response = fileUploadModel.fileuploadAPI();
      expect(response).to.be.an.instanceOf(Promise);
    });

    it('throw error if file upload fails', function () {
      const fileUploadModel = new FileUploadModel();
      const error = new Error("some fake error");
      const call = sandbox.stub(fileUploadModel, "fileuploadAPI").throws(error);
      expect(call).to.throw(Error);
    });

    it("makes a call to file upload api", function () {
      const fileUploadModel = new FileUploadModel();
      sinon.stub(fileUploadModel, "fileuploadAPI").callsFake(function fakeFn() {
        return {
          "status": "200",
          "message": "Record created/updated successfully",
          "success": true
        };
      });
      assert.deepStrictEqual(fileUploadModel.fileuploadAPI(), fileUploadMock.fileuploadAPI());
    });
  });

  describe('fileDeleteAPI', () => {
    it('returns a promise', () => {
      const fileUploadModel = new FileUploadModel();
      const response = fileUploadModel.fileDeleteAPI();
      expect(response).to.be.an.instanceOf(Promise);
    });

    it('throw error if file upload fails', function () {
      const fileUploadModel = new FileUploadModel();
      const error = new Error("some fake error");
      const call = sandbox.stub(fileUploadModel, "fileDeleteAPI").throws(error);
      expect(call).to.throw(Error);
    });

    it('throw error if file upload fails', function () {
      let fileId ='123';
      const claim = sinon.stub();
      const fileUploadModel = new FileUploadModel();
      fileUploadModel.fileDeleteAPI(claim, fileId);
    });


    it("makes a call to file upload api", function () {
      const fileUploadModel = new FileUploadModel();
      sinon.stub(fileUploadModel, "fileDeleteAPI").callsFake(function fakeFn() {
        return {
          "status": 200,
          "message": "File is deleted successfully"
        };
      });
      assert.deepStrictEqual(fileUploadModel.fileDeleteAPI(), fileDeleteMock.fileDeleteAPI());
    });
  });

  describe('fileListAPI', () => {
    it('returns a promise', () => {
      const fileUploadModel = new FileUploadModel();
      const response = fileUploadModel.fileListAPI();
      expect(response).to.be.an.instanceOf(Promise);
    });

    it('throw error if file upload fails', function () {
      const fileUploadModel = new FileUploadModel();
      const error = new Error("some fake error");
      const call = sandbox.stub(fileUploadModel, "fileListAPI").throws(error);
      expect(call).to.throw(Error);
    });

    it('should throw an error if can\'t list ', () => {
      const fileUploadModel = new FileUploadModel();
      const stub = sinon.stub(fileUploadModel, 'fileListAPI').rejects();
      return fileUploadModel.fileListAPI(req, next).should.be.rejectedWith('')
        .then(() => {
          stub.restore();
        });
    });

    it('throw error if file list fails', function () {
      const fileUploadModel = new FileUploadModel();
      const error = new Error("some fake error");
      const call = sandbox.stub(fileUploadModel, "fileListAPI").throws(error);
      expect(call).to.throw(Error);
    });

    it("makes a call to file list  api", function () {
      const fileUploadModel = new FileUploadModel();
      sinon.stub(fileUploadModel, "fileListAPI").callsFake(function fakeFn() {
        return {
          "filesCount": 0,
          "files": [],
          "errors": []
        };
      });
      assert.deepStrictEqual(fileUploadModel.fileListAPI(), fileListMock.fileListAPI());
    });
  });

});
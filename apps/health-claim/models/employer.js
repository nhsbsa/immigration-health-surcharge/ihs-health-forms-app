module.exports = class EmployerModel {
  constructor(model) {
    this['employer-name'] = model['employer-name'];
    this['organisation-id'] = model['organisation-id'];
    this['job-title'] = model['job-title'];
    this['job-role-id'] = model['job-role-id'];
    this['job-setting'] = model['job-setting'];
    this['other-job-setting'] = model['other-job-setting'];
    this['employer-id'] = model['employer-id'];
  }

  editEmployer(model) {
    const editDep = model;
    editDep['employer-name'] = model['employer-name'];
    editDep['organisation-id'] = model['organisation-id'];
    editDep['job-title'] = model['job-title'];
    editDep['job-role-id'] = model['job-role-id'];
    editDep['job-setting'] = model['job-setting'];
    editDep['other-job-setting'] = model['other-job-setting'];
    editDep['employer-id'] = model['employer-id'];
    return editDep;
  }
}
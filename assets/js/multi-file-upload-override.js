let methodOverriderObj = require('@ministryofjustice/frontend');
const path = require('path');
var validationMsgs = require('./validation-messages.json');

//TODO: find a way to override this delete click listener
methodOverriderObj.MultiFileUpload.prototype.setupLabel = function () {
    //TODO: remove depricated proxy n replace with blind 
    this.params.container.on('click', '.hof-moj-multi-file-upload__delete', $.proxy(this, 'onFileDeleteClick'));
    this.label = $('<label for="' + this.fileInput[0].id + '" class="govuk-button govuk-button--secondary">' + this.params.dropzoneButtonText + '</label>');
    this.dropzone.append('<p class="govuk-body">' + this.params.dropzoneHintText + '</p>');
    this.dropzone.append(this.label);
};

methodOverriderObj.MultiFileUpload.prototype.uploadFiles = function (files) {
    if ($(document).find('#file-listing-error-summery').length !== 0) {
        this.setFocusOnErrorSummery();
        return false;
    }

    if ($(document).find('.moj-multi-file-upload__progress').length !== 0) {
        this.markWaitingOfProcess();
        this.setFocusOnErrorSummery();
        return false;
    }
    this.currentFileQueue = [...files];
    let tempFileList = [...files];
    let checkForLimitObj = this.checkForFileLimit(tempFileList.length);
    if (!checkForLimitObj.status) {
        if (!checkForLimitObj.errorListed)
            this.addErrorSummery(430, checkForLimitObj.validationMsg, 430, true);
        $(`#error-msg-li-no-file`).remove();
        this.setFocusOnErrorSummery();
        $('.form-group').addClass('govuk-form-group--error');
        $('#bref-error-summery').html(this.getFileLimitBrifErrorMsgHtml(checkForLimitObj.validationMsg));
        $('#multi-file-notification-banner-info').html(``);
        return false;
    }

    for (var i = 0; i < tempFileList.length; i++) {
        this.params.uploadFileEntryHook(this, tempFileList[i]);
        let validationObj = fileValidation(tempFileList[i], i, tempFileList.length);
        if (validationObj.status) {
            this.tempFileStorage = tempFileList[i];
            let setTimeOutObj = setTimeout((file) => {
                this.uploadFile(file);
            }, 500, tempFileList[i])
        } else {
            this.params.uploadFileErrorHook(this, tempFileList[i], {}, validationObj);
        }
    }
};

//TODO: MINIMIZE THIS FILE VALIDATION FUNCTION
fileValidation = function (fileDetails, fileCount, fileLength) {
    let status = {
        status: true, message: '', type: '', saveType: 'local'
    };
    if (!(fileDetails['name'] && ['.jpeg', '.jpg', '.bmp', '.png', '.pdf'].includes(path.extname(fileDetails['name']).toLowerCase()))) {
        //return validationMsgs.Errors["upload-evidence"][0]['extname'];
        return status = {
            status: false, message: `${fileDetails['name']} must be a jpg, bmp, png or pdf`, type: 'extname', fileId: Math.round(Math.random() * 10000), name: fileDetails['name'], saveType: 'local'
        }
    }
    if (fileDetails['name'].substr(0, fileDetails['name'].lastIndexOf('.')).length >   Number(`${$('.moj-multi-file-upload').attr('filename-length')}`) ){
        return status = {
            status: false, message: `${fileDetails['name']} must be 130 characters or less`, type: 'extname', fileId: Math.round(Math.random() * 10000), name: fileDetails['name'], saveType: 'local'
        }
    }

    if (!(/^[a-z0-9\-_\s]+$/i).test(path.parse(fileDetails['name']).name)) {
        return status = {
            status: false, message: `${fileDetails['name']} must use only letters, numbers, spaces, hyphens and underscore`, type: 'regex', fileId: Math.round(Math.random() * 10000), name: fileDetails['name'], saveType: 'local'
        }
    }
    
    if (fileDetails['size'] >  Number(`${$('.moj-multi-file-upload').attr('file-size-limit')}`)) {
        return status = {
            status: false, message: `${fileDetails['name']} must be less than 2MB in size`, type: 'filesize', fileId: Math.round(Math.random() * 10000), name: fileDetails['name'], saveType: 'local'
        }
    }

    return status;
};

methodOverriderObj.MultiFileUpload.prototype.uploadFile = function (file) {

    const reader = new FileReader();
    reader.readAsDataURL(file);
    
    reader.onload = (evt) => {
        var item = $(this.getFileRowHtml(file));
        this.feedbackContainer.find('.moj-multi-file-upload__list').append(item);
        $.ajax({
            url: this.params.uploadUrl,
            headers: {
                "sessionid":  this.params.sessionId,
                "Content-Type": "application/json"
            },
            type: 'POST',
            data: JSON.stringify({
                "fileName": file.name.replace(/ /g, "_"),
                "contentType": file.type,
                "data": evt.target.result.substring(evt.target.result.indexOf(',') + 1).trim()
            }),
            processData: false,
            contentType: false,
            success: $.proxy(function (response) {
                if (response.statusCode != 200) {
                    //TODO: IMPROVE ERROR MARKS ERROR FILES CODE
                    this.params.uploadFileErrorHook(this, file, response, {
                        status: false,
                        message: `${file.name} was not uploaded. Try again`,
                        type: 'upload-fail',
                        fileId: Math.round(Math.random() * 10000),
                        name: file.name,
                        saveType: 'local'
                    }, item);
                } else {
                    item.find('.moj-multi-file-upload__message').html(this.getSuccessHtml({
                        'messageHtml': `${file.name} successfully uploaded`
                    }));
                    this.status.html(`${file.name} successfully uploaded`);
                    item.find('.moj-multi-file-upload__actions').append(this.getDeleteButtonHtml(response, file));
                    this.params.uploadFileExitHook(this, file, response);
                }
            }, this),
            error: $.proxy(function (jqXHR, textStatus, errorThrown) {
                this.params.uploadFileErrorHook(this, file, jqXHR, {
                    status: false,
                    message:  `${(jqXHR.status == 429)?'': `${file['name']} "was not uploaded. Try again"`}`,
                    type: 'upload-fail',
                    fileId: Math.round(Math.random() * 10000),
                    name: file.name,
                    saveType: 'local'
                }, item);
            }, this),
            xhr: function () {
                var xhr = new XMLHttpRequest();
                xhr.upload.addEventListener('progress', function (e) {
                    if (e.lengthComputable) {
                        var percentComplete = e.loaded / e.total;
                        percentComplete = parseInt(percentComplete * 100, 10);
                        item.find('.moj-multi-file-upload__progress').text(' ' + percentComplete + '%');
                    }
                }, false);
                return xhr;
            }
        });
    }
};

methodOverriderObj.MultiFileUpload.prototype.onFileDeleteClick = function (e) {
    e.preventDefault(); // if user refreshes page and then deletes
    if ($(document).find('.moj-multi-file-upload__progress').length !== 0) {
        this.markWaitingOfProcess()
        this.setFocusOnErrorSummery();
        return false;
    }
    let item = $(e.currentTarget).parents()[1];
    let fileDetails = {
        name: $(e.currentTarget).attr('data-fileName'),
        fileId: $(e.currentTarget).attr('data-fileId'),
        savetype: ($(e.currentTarget).attr('data-savetype')) ? 'local' : 'server',
        element: e
    };
    this.currentFileQueue = [fileDetails];
    if ($(e.currentTarget).attr('data-savetype') == 'local') {
        this.params.fileDeleteHook(this, fileDetails, {});
        return true;
    }
    $.ajax({
        url: this.params.deleteUrl,
        method: 'DELETE',
        headers: {
            "sessionid": this.params.sessionId,
            "fileid": $(e.currentTarget).attr('data-fileId'),
            "Content-Type": "application/json"
        },
        success: $.proxy(function (response) {
            if (response.statusCode == 200) {
                $(e.currentTarget).parents('.moj-multi-file-upload__row').remove();
                if (this.feedbackContainer.find('.moj-multi-file-upload__row').length === 0) {
                    this.feedbackContainer.addClass('moj-hidden');
                }
                this.params.fileDeleteHook(this, fileDetails, response);
            } else {
                this.params.uploadFileErrorHook(this, file, response, {
                    status: false,
                    message: `${file['name']} was not deleted. Try again`,
                    type: 'delete-fail',
                    fileId: $(e.currentTarget).attr('data-fileId'),
                    saveType: 'server'
                }, item);
            }
        }, this),
        error: $.proxy(function (jqXHR, textStatus, errorThrown) {
            this.params.uploadFileErrorHook(this, fileDetails, jqXHR, {
                status: false,
                message: `${fileDetails['name']} was not deleted. Try again`,
                type: 'delete-fail',
                fileId: $(e.currentTarget).attr('data-fileId'),
                saveType: 'server'
            }, item);
        }, this)
    });
};

methodOverriderObj.MultiFileUpload.prototype.updateFileQueue = function () {
    this.currentFileQueue.pop();
    if (this.currentFileQueue.length == 0)
        this.FileQueuecomplete();
};

methodOverriderObj.MultiFileUpload.prototype.FileQueuecomplete = function () {
    console.debug('file queue complete');
    $(`#error-msg-li-no-file`).remove();
    $(`#error-msg-li-888`).remove();
    $(`#error-msg-li-430`).remove();

    if ($("[data-response-code=429]").length <= 1 && $(`#error-msg-li-429`).length == 1)
        $(`#error-msg-li-429`).remove();

    if ($('.moj-multi-file-upload__error').length === 0) {
        $('.form-group').removeClass('govuk-form-group--error');
        $('#bref-error-summery').html(``);
        if ($('#error-msg-li-100').length == 0)
            $('#multi-file-error-summary').html(``);
        // this.feedbackContainer.addClass('moj-hidden');
    } else {
        let brefErrorSummeryHTML = ($('.moj-multi-file-upload__list').find(`[data-errortype='delete-fail']`).length == 0) ? this.getErrorMessageHtml() : this.getFileLimitBrifErrorMsgHtml(this.validationMessage.deleteFail);
        $('#bref-error-summery').html(brefErrorSummeryHTML);
    }

    if ($('.govuk-error-summary__list li').length == 0)
        document.title = this.params.documentTitle;
};

methodOverriderObj.MultiFileUpload.prototype.getNotificationBannerHtml = function (fileDetails) {
    return `
    <div class="govuk-notification-banner govuk-notification-banner--success" role="alert"
        aria-labelledby="govuk-notification-banner-title" data-module="govuk-notification-banner" tabindex="-1">
        <div class="govuk-notification-banner__header">
            <h2 class="govuk-notification-banner__title" id="govuk-notification-banner-title">
                Success
            </h2>
        </div>
        <div class="govuk-notification-banner__content">
            <p class="govuk-body break-word">File named ${fileDetails.name} has been deleted</p>
        </div>
    </div>
    `;
}

methodOverriderObj.MultiFileUpload.prototype.getErrorSummeryHtml = function () {
    return `
    <div class="govuk-error-summary" aria-labelledby="error-summary-title" data-module="govuk-error-summary" role="alert" tabindex="-1">
        <h2 class="govuk-error-summary__title" id="error-summary-title">There is a problem</h2>
            <div class="govuk-error-summary__body">
                <ul class="govuk-list govuk-error-summary__list">
                </ul>
            </div>
    </div>
    `;
}

methodOverriderObj.MultiFileUpload.prototype.getErrorSummeryRowHtml = function (errorsDetails) {
    return `<li id="error-msg-li-${errorsDetails.fileId}" data-response-code="${errorsDetails.statusCode}">
                <a class="break-word" href="#${(errorsDetails.fileId == 100) ? 'refresh-page-contain' : 'file-delete-id-' + errorsDetails.fileId}">${errorsDetails.message}</a>
                ${this.getRefereshLinkHtml(errorsDetails)}
            </li>`;
}

methodOverriderObj.MultiFileUpload.prototype.getFileLimitHtml = function (errorsDetails) {
    return `<li id="error-msg-li-430" data-response-code="430">
                <a href="#refresh-page-sub-contain">${errorsDetails.message}</a><br>
                <a href=".${this.params.pageUrlName}" id="refresh-page-contain" class="govuk-link refresh-page-link">Clear error message</a>
            </li>`;
}

methodOverriderObj.MultiFileUpload.prototype.getRefereshLinkHtml = function (errorsDetails) {
    let refereshHtmlString = ``;
    if (errorsDetails.fileId === 100 || errorsDetails.message.search('was not deleted.') !== -1) {
        let linkDisplayUrl = (errorsDetails.message.search('was not deleted.') == -1) ? 'Refresh this page' : 'Clear error message';
        refereshHtmlString = `<br><a href=".${this.params.pageUrlName}?fileId=${errorsDetails.fileId}" ${(errorsDetails.fileId === 100) ? 'id="refresh-page-contain"' : ''} class="govuk-link refresh-page-link">${linkDisplayUrl}</a>`;
    }
    return refereshHtmlString;
}

methodOverriderObj.MultiFileUpload.prototype.getErrorMessageHtml = function () {
    this.addClickEventDeleteAllLink();
    return `<span class="govuk-error-message">
              <span class="govuk-visually-hidden">upload evidence</span>
              You must delete all unsuccessful files and try again to continue <br>
              <a class="govuk-link govuk-!-font-weight-regular" href="#" id="file-delete-id-429">Delete all unsuccessful files</a>
            </span>`;
}

methodOverriderObj.MultiFileUpload.prototype.getWaitFileMessageHtml = function () {
    return `<span class="govuk-error-message" id="file-delete-id-888">
              <span class="govuk-visually-hidden">upload evidence</span>
              ${this.validationMessage.waitingMessage}
            </span>`;
}

methodOverriderObj.MultiFileUpload.prototype.getFileLimitBrifErrorMsgHtml = function (validationMessage,idName="refresh-page-sub-contain") {
    return `<span class="govuk-error-message">
              <span class="govuk-visually-hidden">upload evidence</span>
              ${validationMessage}<br>
              <a class="govuk-link govuk-!-font-weight-regular" href=".${this.params.pageUrlName}" id="${idName}">Clear error message</a>
            </span>`;
}

methodOverriderObj.MultiFileUpload.prototype.getSuccessHtml = function (success) {
    return `<span class="moj-multi-file-upload__success" > 
                <svg class="moj-banner__icon" fill="currentColor" role="presentation" focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25" height="25" width="25"><path d="M25,6.2L8.7,23.2L0,14.1l4-4.2l4.7,4.9L21,2L25,6.2z"/></svg>
                <div class="moj-file-message_span">${success.messageHtml}</div>
            </span> `;
};

methodOverriderObj.MultiFileUpload.prototype.getErrorFileRowHtml = function (file, validationObj) {
    var html = `
        <div class="govuk-summary-list__row moj-multi-file-upload__row" id="file-row-id-${validationObj.fileId}">
         <div class="govuk-summary-list__value moj-multi-file-upload__message"><span class="moj-multi-file-upload__error">
                 <svg class="moj-banner__icon" fill="currentColor" role="presentation" focusable="false"
                     xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25" height="25" width="25">
                     <path d="M13.6,15.4h-2.3v-4.5h2.3V15.4z M13.6,19.8h-2.3v-2.2h2.3V19.8z M0,23.2h25L12.5,2L0,23.2z">
                     </path>
                 </svg>
                 <div class="moj-file-message_span">${validationObj.message}</div></span>
             </div>
         <div class="govuk-summary-list__actions moj-multi-file-upload__actions">
         ${this.getDeleteButtonHtml({ fileId: validationObj.fileId }, { originalname: validationObj.name }, 'local')}
         </div>
    </div>`;
    return html;
};

methodOverriderObj.MultiFileUpload.prototype.getDeleteErrorFileRowHtml = function (file, validationObj) {
    var html= `
         <div class="govuk-summary-list__value moj-multi-file-upload__message" data-errorType="${validationObj.type}">
            <span class="moj-multi-file-upload__error">
                 <svg class="moj-banner__icon" fill="currentColor" role="presentation" focusable="false"
                     xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25" height="25" width="25">
                     <path d="M13.6,15.4h-2.3v-4.5h2.3V15.4z M13.6,19.8h-2.3v-2.2h2.3V19.8z M0,23.2h25L12.5,2L0,23.2z">
                     </path>
                 </svg>
                 <div class="moj-file-message_span">${validationObj.message}</div>
            </span>
             </div>
         <div class="govuk-summary-list__actions moj-multi-file-upload__actions">
         ${this.getDeleteButtonHtml({ fileId: validationObj.fileId }, { originalname: file.name }, validationObj.saveType)}
         </div>`;
    return html;
};

methodOverriderObj.MultiFileUpload.prototype.getDeleteButtonHtml = function (response, file, saveType = 'server') {
    return `<a class="govuk-link hof-moj-multi-file-upload__delete" href="#" role="button" data-fileName="${file.originalname || file.name}" data-fileId="${response.fileId}" data-saveType="${saveType}" id="file-delete-id-${response.fileId}">
        Delete <span class="govuk-visually-hidden"> ${file.originalname || file.name}</span></a> `;
};

methodOverriderObj.MultiFileUpload.prototype.checkForFileLimit = function (stackedFiles = 0) {
    let currentSuccessFiles = $(document).find('.moj-multi-file-upload__success').length;
    let totalFilesInQueue = currentSuccessFiles + stackedFiles;
    if (totalFilesInQueue > $('#max-file-count-value').val()) {
        return {
            status: false,
            errorListed: ($('#error-msg-li-430').length > 0) ? true : false,
            validationMsg: (currentSuccessFiles > 0) ? this.validationMessage.fileSelectionWithExistingFileLimit : this.validationMessage.fileSelectionLimit
        };
    } else {
        return { status: true };
    }
}

methodOverriderObj.MultiFileUpload.prototype.markAsErrorFile = function (file, validationObj, item, statusCode) {
    if (item){
        if(statusCode != 429)
            $(item).html(this.getDeleteErrorFileRowHtml(file, validationObj));
        else
            $(item).html('');
    }else{
        this.feedbackContainer.find('.moj-multi-file-upload__list').append($(this.getErrorFileRowHtml(file, validationObj)));
    }
    $('.form-group').addClass('govuk-form-group--error');
    let fileId = validationObj['fileId'];
    if (validationObj.type !== 'delete-fail') {
        if(statusCode != 429)
            $('#bref-error-summery').html(this.getErrorMessageHtml());
        else
            $('#bref-error-summery').html(this.getFileLimitBrifErrorMsgHtml(this.validationMessage.fileSelectionWithExistingFileLimit,'file-delete-id-429'));
    } else {
        $('#bref-error-summery').html(this.getFileLimitBrifErrorMsgHtml(this.validationMessage.deleteFail));
    }

    if (window.navigator.onLine)
        this.addErrorSummery(validationObj['fileId'], validationObj['message'], statusCode);
}

methodOverriderObj.MultiFileUpload.prototype.markWaitingOfProcess = function () {
    $('#multi-file-notification-banner-info').html(``);
    ($(`#error-msg-li-888`).length == 0) ? this.addErrorSummery(888, this.validationMessage['waitingMessage'], 488, true) : '';
    $('.govuk-error-summary').trigger('focus');
    $('.form-group').addClass('govuk-form-group--error');
    $('#bref-error-summery').html(this.getWaitFileMessageHtml());
}

methodOverriderObj.MultiFileUpload.prototype.clearAllDeleteFileErrors = function () {
    $('#bref-error-summery').html(``);
    $('.moj-multi-file-upload__list').find(`[data-errortype='delete-fail']`).each((index, element) => {
        $(element).removeAttr('data-errortype');
        let fileId = $(element).parent().find('a').attr('data-fileid');
        $(`#error-msg-li-${$(element).parent().find('a').attr('data-fileid')}`).remove();
        $(element).html(this.getSuccessHtml({
            messageHtml: `${$(element).parent().find('a').attr('data-filename')} successfully uploaded`
        }));
    });
}

methodOverriderObj.MultiFileUpload.prototype.addErrorSummery = function (fileId, message, statusCode, prepend = false) {
    if ($('#error-msg-li-' + fileId).length > 0) {
        return false;
    }
    let innerHtml = this.getErrorSummeryRowHtml({ fileId: fileId, message: message, statusCode: statusCode });
    innerHtml = (statusCode == 430) ? this.getFileLimitHtml({ fileId: fileId, message: message, statusCode: statusCode }) : innerHtml;

    if ($(document).find('.govuk-error-summary').length === 0) {
        document.title = `Error: ` + this.currentDocumenttitle;
        $('#multi-file-error-summary').append(this.getErrorSummeryHtml());
        $('.govuk-error-summary__list').append(innerHtml);
    } else {
        $('.govuk-error-summary__list').prepend(innerHtml);
    }
}

methodOverriderObj.MultiFileUpload.prototype.addClickEventDeleteAllLink = function () {
    setTimeout(() => {
        $('#file-delete-id-429').on('click', (event) => {
            event.preventDefault();
            if ($(document).find('.moj-multi-file-upload__progress').length !== 0) {
                this.markWaitingOfProcess()
                return false;
            } else {
                window.location.replace(`${window.location.origin}${window.location.pathname}?deleteAllWantedFile=true`);
            }
        });
    }, 200);
}

methodOverriderObj.MultiFileUpload.prototype.setFocusOnErrorSummery = function () {
    setTimeout(() => {
        $('.govuk-error-summary').trigger('focus');
    }, 100);
}

methodOverriderObj.MultiFileUpload.prototype.setFocusOnNotification = function () {
    setTimeout(() => {
        $('.govuk-notification-banner--success').trigger('focus');
    }, 100);
}
methodOverriderObj.MultiFileUpload.prototype.currentDocumenttitle = document.title;
methodOverriderObj.MultiFileUpload.prototype.validationMessage = validationMsgs;
module.exports = methodOverriderObj;
'use strict';

const moreDependantClearSession = [
  'dependant-given-name',
  'dependant-family-name',
  'dependant-date-of-birth',
  'dependant-immigration-health-surcharge-number',
  ];

const notEligibleClearSession = [
  'dependant-given-name',
  'dependant-family-name',
  'dependant-date-of-birth',
  'dependant-immigration-health-surcharge-number',
  'dependant-id',
 ];

const maxDependantCount = 10;


module.exports = {
  moreDependantClearSession,
  notEligibleClearSession,
  maxDependantCount
};
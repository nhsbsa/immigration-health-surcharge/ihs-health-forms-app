'use strict';
const Model = require('hof-govfrontend-v3').model;
const axios = require('axios').default;
const GetEmployer = require('../../../../apps/health-claim/models/get-employer');

const { chai, reqres, sinon, expect, assert } = require('../../../setup');

describe('Submit Claim Model', () => {
  let sandbox;
  let employerList;
  const next = sinon.stub();

  beforeEach(function () {    
    sandbox = sinon.createSandbox();
  });

  afterEach(() => sandbox.restore());

  it('returns a promise', () => {
    const param = '[AIREDALE NHS]';
    const employerList = sinon.stub();
    const getEmployer = new GetEmployer();
    const response = getEmployer.getEmployer(next,param);
    expect(response).to.be.an.instanceOf(Promise);
  });

  it('calls getEmployer with arguments', () => {
    const param = sinon.stub();
    const employerList = sinon.stub();
    const getEmployer = new GetEmployer();
    const response = getEmployer.getEmployer(next,param);
  });


});
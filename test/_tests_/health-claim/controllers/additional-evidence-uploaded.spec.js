
const AdditionalEvidanceUpload = require('../../../../apps/health-claim/controllers/extra-evidence-uploaded');

const HOFController = require('hof-govfrontend-v3').controller;
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('AdditionalEvidanceUploadedController', () => {
  let req;
  let res;
  const next = sinon.stub();
  const callback = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('Additional Evidance Uploaded instance', () => {
    let additionalEvidanceUpload;
    it('extends the hof-form-controller', () => {
      additionalEvidanceUpload.should.be.instanceof(HOFController);
    });

    beforeEach(() => {
      additionalEvidanceUpload = new AdditionalEvidanceUpload({});
      it('has a getValues method', () => {
        additionalEvidanceUpload.should.have.property('getValues').that.is.a('function');
      });
    });

    it('has getValues, locals, process, flashError methods', () => {
      additionalEvidanceUpload.should.have.property('process').that.is.a('function');
      additionalEvidanceUpload.should.have.property('getValues').that.is.a('function');
      additionalEvidanceUpload.should.have.property('locals').that.is.a('function');
      additionalEvidanceUpload.should.have.property('flashError').that.is.a('function');
    });

    describe('getValues', () => {

      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'getValues').returns({
          fields: []
        });
        _.set(additionalEvidanceUpload, 'options.locals.field', {})
      });
      afterEach(() => {
        HOFController.prototype.getValues.restore();
      });

      it('calls super.getValues', () => {
        additionalEvidanceUpload.getValues(req, res, callback);
        HOFController.prototype.getValues.should.have.been.calledOnce
          .and.calledWithExactly(req, res, callback);
      });
      it('calls getValues', () => {
        additionalEvidanceUpload.getValues(req, res, callback);
      });

    });

    describe('locals', () => {
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'locals').returns({
          fields: []
        });
        res.locals.values = sinon.stub();
        req.sessionModel.get = sinon.stub();
        req.sessionModel.get.withArgs('claim-reference-number').returns('BSA123123123');
        req.sessionModel.get.withArgs('claim-calculated-end-date').returns('2000-09-09');
        res.locals.values.withArgs('claim-start-date').returns('2000-09-09');

      });

      afterEach(() => {
        HOFController.prototype.locals.restore();
      });


      it('calls locals', () => {
        additionalEvidanceUpload.locals(req, res, next);
        HOFController.prototype.locals.should.have.been.calledOnce
          .and.calledWithExactly(req, res, next);
      });

     // if(req.sessionModel['attributes'].hasOwnProperty('errorValues') 
    //   req.sessionModel['attributes']['errorValues'].hasOwnProperty('extra-information')){
   


      it('calls locals with error values', () => {
        req.sessionModel.get.withArgs('attributes').returns('errorValues');
        req.sessionModel.get.withArgs(['attributes']['errorValues']).returns('extra-information');
        additionalEvidanceUpload.locals(req, res, next);
      });

    });

    describe('process', () => {
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'process').returns({
          fields: []
        });
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();          
      });
      afterEach(() => {
        HOFController.prototype.process.restore();
      });

      it('calls process with arguments', () => {
        additionalEvidanceUpload.process(req, res, next);
      });

      it('calls process with add-more-files-extra-info value yes', () => {
        req.form.values['add-more-files-extra-info'] = 'yes'; 
        additionalEvidanceUpload.process(req, res, next);
      });

      it('calls process with add-more-files-extra-info value no', () => {
        req.form.values['add-more-files-extra-info'] = 'no'; 
        additionalEvidanceUpload.process(req, res, next);
      });

      it('calls process with add-more-files-extra-info value yes', () => {
        req.form.values['add-more-files-extra-info'] = 'yes'; 
        req.url  = 'extra-evidence-uploaded/edit';
        additionalEvidanceUpload.process(req, res, next);
      });

      it('calls process with add-more-files-extra-info value yes', () => {
        req.form.values['add-more-files-extra-info'] = 'yes'; 
        req.sessionModel.get.withArgs('uploadedFileCountExtraInfo').returns(1);
        additionalEvidanceUpload.process(req, res, next);
      });

      it('calls process with add-more-files-extra-info value yes', () => {
        req.form.values['add-more-files-extra-info'] = 'yes'; 
        req.sessionModel.get.withArgs('uploadedFileCountExtraInfo').returns(4);
        process.env.COMMENT_MAX_FILE_COUNT = '3';
        additionalEvidanceUpload.process(req, res, next);
      });      
    });



  });
});
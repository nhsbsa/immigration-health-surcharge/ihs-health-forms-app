const Controller = require('hof-govfrontend-v3').controller;
module.exports = class DependantDeleteController extends Controller {

  get(req, res, next) {
    const dependantId = req.query.dependantId;
    if (dependantId) {
      this.deleteDependant(req, res, dependantId)
    }
    next();
  }

  deleteDependant(req, res, dependantId) {
    const allDependants = req.sessionModel.get('all-dependants')
    const dependantIndex = allDependants.findIndex(dependant => dependant['dependant-id'] === dependantId)
    if (dependantIndex !== -1) {
      const deletedDependant = (dependantIndex + 1) + ' ' + allDependants[dependantIndex]['dependant-name'];
      allDependants.splice(dependantIndex, 1);
      req.sessionModel.set('all-dependants', allDependants);
      req.sessionModel.set('deletedDependant', deletedDependant);
      req.sessionModel.unset('dependant-id');     
    }
    const nextNav = allDependants.length === 0 ? '/dependant-question' : '/check-dependants';
    return res.redirect(`${req.baseUrl}${nextNav}`);
  }

};
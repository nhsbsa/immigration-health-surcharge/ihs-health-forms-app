const Controller = require('hof-govfrontend-v3').controller;
const DependantModel = require('./../models/dependant');
const dependantSections = require('./../sections/dependant')
const moment = require('moment');
const { moreDependantClearSession, maxDependantCount } = require('./../constants/dependant');

const radioinputs = ['dependant-date-of-birth'];
module.exports = class DependantDetailsController extends Controller {

  getValues(req, res, callback) {    
    super.getValues(req, res, callback);
    const sessionModel = req.sessionModel.toJSON();
    this.saveDependant(req, sessionModel);
  }
  
  saveDependant(req, sessionModel) {
    const currentDependantId = sessionModel['dependant-id'];   
    if(!currentDependantId) return;
    let allDependants = sessionModel['all-dependants'] ? sessionModel['all-dependants'] : [];
    const dependantIndex = allDependants.findIndex(dependant => dependant['dependant-id'] === currentDependantId)
    if(dependantIndex === -1) {
      allDependants.push(new DependantModel(sessionModel))
    }else{
       allDependants = this.modifyDepedantDetails(req);
    }
    req.sessionModel.set('all-dependants', allDependants);
  }

  modifyDepedantDetails(req) {
    const dependantId = req.sessionModel.get('edit-dependantId');
    if(dependantId){
      let allDependants = req.sessionModel.get('all-dependants');
      const dependantIndex = allDependants.findIndex(dependant => dependant['dependant-id'] === dependantId);  
      const keys =  Object.keys(allDependants[dependantIndex]);
      keys.forEach((formfield, index) => { 
        allDependants[dependantIndex][formfield] =req.form.values[formfield] ;          
      });   
      const depmodel =  new DependantModel(allDependants[dependantIndex]);
      allDependants[dependantIndex] = depmodel.editDependant(depmodel);
      req.sessionModel.unset('edit-dependantId');
      return allDependants;
    }   
  }

  
};
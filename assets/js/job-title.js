var autocomplete = require('./autocomplete-new');

autocomplete({
  input: document.getElementById("job-title"),
  fetch: function (text, update) {
    text = text.toLowerCase();
    var request = new XMLHttpRequest();
    var jobrolesList = require('./jobroles.json');
      var suggestions = [];
      jobrolesList.Roles.forEach(filterFunction);
      function filterFunction(value, index, array) {
        if (value.Role.toLowerCase().indexOf(text.toLowerCase()) !== -1) {
          suggestions.push({"label": value.Role, "value":value.TitleReq});
        }
      }
      update(suggestions);
  },
  minLength:3,
  onSelect: function (item) {
    document.getElementById("job-title").value = item.label;
  }
});

const Controller = require('hof-govfrontend-v3').controller;
module.exports = class EditEmployerController extends Controller {

  get(req, res, next) {
    const employerId = req.query.employerId;
    const url = req.query.pagename;
    if (employerId) {
      this.editEmployer(req, res, employerId,url);
    }else{
      next();
    }
  }

  editEmployer(req, res, employerId, url) {
    const allEmployers = req.sessionModel.get('all-employers');
    req.sessionModel.set('edit-employerId', employerId);
    const employerIndex = allEmployers.findIndex(employer => employer['employer-id'] === employerId);
    if (employerIndex !== -1) {
      for (let key in allEmployers[employerIndex]) {
        allEmployers[employerIndex].hasOwnProperty(key) ? req.sessionModel.set(key, allEmployers[employerIndex][key]) : false;
      }
    }
    req.session['hof-wizard-health-claim'].steps.includes('/job-title') ?
      '' : req.session['hof-wizard-health-claim'].steps.push('/job-title');
    req.session['hof-wizard-health-claim'].steps.includes('/job-setting') ?
            '' : req.session['hof-wizard-health-claim'].steps.push('/job-setting');
    const nextNav = '/' + url + '?employerId=' + employerId;
    return res.redirect(`${req.baseUrl}${nextNav}`);
  }
};